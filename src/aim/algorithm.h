/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_AIM_ALGO_H
#define HNEF_AIM_ALGO_H

#include "aiminimaxt.h"	/* hnef_aiminimax */

/*@-protoparamname@*/
/*@unused@*/
extern
enum HNEF_FR
aiminimax_command (
/*@in@*/
/*@notnull@*/
	const struct hnef_game	* const HNEF_RSTR g,
/*@in@*/
/*@notnull@*/
	struct hnef_aiminimax	* const aim,
/*@out@*/
/*@notnull@*/
	unsigned short		* pos,
/*@out@*/
/*@notnull@*/
	unsigned short		* dest,
/*@null@*/
	void			* data,
/*@null@*/
	enum HNEF_FR (* func_interrupt)
				(void *, HNEF_BOOL *, HNEF_BOOL *),
/*@null@*/
	enum HNEF_FR (* func_progress) (void *, int, int)
	)
/*@modifies * aim, * pos, * dest, * data@*/
;
/*@=protoparamname@*/

#endif

