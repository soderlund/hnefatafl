/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */
#include <stdlib.h>	/* malloc, free */

#include "aiminimax.h"

/*
 * Default listm capacity for moves when allocating.
 *
 * This shouldn't grow too many times: 80 -> 120 -> 180.
 *
 * It may be excessive on 7x7 boards, and far from enough on 19x19, but
 * it should be enough with only a few growths on 9x9 to 13x13.
 */
/*@unchecked@*/
static
const size_t HNEF_AIM_OPT_LIST_CAP_DEF	= (size_t)80;

/*
 * Value of an escape square in aiminimax->board_escval.
 *
 * NOTE:	You can tune this value.
 */
/*@unchecked@*/
static
const int HNEF_AIM_ESCDIST_BASE	= 20;

/*
 * Modification in value for every step from an escape square in
 * aiminimax->board_escval. This should be negative to make the computer
 * player prefer moving toward the escape square; and it must be
 * negative to prevent infinite recursion when assigning values.
 *
 * NOTE:	You can tune this value.
 */
/*@unchecked@*/
static
const int HNEF_AIM_ESCDIST_MOD	= -2;

HNEF_BOOL
hnef_aiminimax_depth_max_valid (
	const unsigned short	depth_max
	)
{
	return	depth_max >= HNEF_AIM_DEPTHMAX_MIN
	&&	depth_max <= HNEF_AIM_DEPTHMAX_MAX;
}

/*@null@*/
/*@only@*/
/*@partial@*/
static
struct hnef_listm * *
hnef_alloc_opt_buf_moves (
	const unsigned short	opt_buf_len
	)
/*@modifies nothing@*/
{
	unsigned short	i;
	struct hnef_listm	* * opt_buf_moves =
			malloc(sizeof(* opt_buf_moves) * opt_buf_len);
	if	(NULL == opt_buf_moves)
	{
		return	NULL;
	}

	for	(i = 0; i < opt_buf_len; ++i)
	{
		opt_buf_moves[i]	= hnef_alloc_listm
					(HNEF_AIM_OPT_LIST_CAP_DEF);
		if	(NULL == opt_buf_moves[i])
		{
			unsigned short	j;
			for	(j = 0; j < i; j++)
			{
				hnef_free_listm(opt_buf_moves[j]);
			}
			free	(opt_buf_moves);
			return	NULL;
		}
	}
	return	opt_buf_moves;
}

/*@null@*/
/*@only@*/
/*@partial@*/
static
struct hnef_board * *
hnef_alloc_opt_buf_board (
	const unsigned short	opt_buf_len,
	const unsigned short	blen
	)
/*@modifies nothing@*/
{
	unsigned short	i;
	struct hnef_board	* * opt_buf_board =
			malloc(sizeof(* opt_buf_board) * opt_buf_len);
	if	(NULL == opt_buf_board)
	{
		return	NULL;
	}

	for	(i = 0; i < opt_buf_len; ++i)
	{
		opt_buf_board[i]	= hnef_alloc_board(blen);
		if	(NULL == opt_buf_board[i])
		{
			unsigned short	j;
			for	(j = 0; j < i; j++)
			{
				hnef_free_board(opt_buf_board[j]);
			}
			free	(opt_buf_board);
			return	NULL;
		}
	}
	return	opt_buf_board;
}

/*
 * This function assigns value to pos_x / pos_y in b_escval, and then
 * recurses with value - HNEF_AIM_ESCDIST_MOD on every adjacent square,
 * stopping when it encounters a higher value or when value is reduced
 * to 0.
 *
 * NOTE:	This function uses recursion and assumes that the call
 *		stack won't be depleted. Since the maximum board size is
 *		19, this shouldn't ever be close to happening regardless
 *		of ruleset.
 */
static
void
hnef_escval_walk (
/*@in@*/
/*@notnull@*/
	const struct hnef_ruleset	* const HNEF_RSTR rules,
/*@in@*/
/*@notnull@*/
	int				* b_escval,
	const unsigned short		pos_x,
	const unsigned short		pos_y,
	const int			value
	)
/*@modifies * b_escval@*/
{
	unsigned short	pos;

	assert	(NULL != rules);
	assert	(NULL != b_escval);

	if	(pos_x >= rules->bwidth
	||	pos_y >= rules->bheight
	||	value <= 0)
	{
		return;
	}

	pos	= (unsigned short)(pos_y * rules->bheight + pos_x);

	if	(value > b_escval[pos])
	{
		b_escval[pos]	= value;
	}
	else /* if (value <= b_escval[pos]) */
	{
		return;
	}

	hnef_escval_walk(rules, b_escval, (unsigned short)(pos_x + 1),
				(unsigned short)pos_y,
		value + HNEF_AIM_ESCDIST_MOD);
	hnef_escval_walk(rules, b_escval, (unsigned short)(pos_x - 1),
				(unsigned short)pos_y,
		value + HNEF_AIM_ESCDIST_MOD);
	hnef_escval_walk(rules, b_escval, (unsigned short)pos_x,
				(unsigned short)(pos_y + 1),
		value + HNEF_AIM_ESCDIST_MOD);
	hnef_escval_walk(rules, b_escval, (unsigned short)pos_x,
				(unsigned short)(pos_y - 1),
		value + HNEF_AIM_ESCDIST_MOD);
}

static
enum HNEF_FR
hnef_aiminimax_board_escval_init (
/*@in@*/
/*@notnull@*/
	const struct hnef_game	* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	int			* const HNEF_RSTR board_escval
	)
/*@modifies * board_escval@*/
{
	unsigned short		pos;
	struct hnef_ruleset	* HNEF_RSTR rules	= NULL;

	assert	(NULL != game);
	assert	(NULL != board_escval);
	assert	(NULL != game->rules->squares);

	rules	= game->rules;

	if	(HNEF_AIM_ESCDIST_MOD >= 0)
	{
		/*
		 * This will cause infinite recursion.
		 */
		return	HNEF_FR_FAIL_ILL_STATE;
	}

	for	(pos = 0; pos < rules->opt_blen; ++pos)
	{
		const HNEF_BIT_U8 sbit	= rules->squares[pos];
		if	(((unsigned int)sbit
			& (unsigned int)rules->opt_ts_escape)
			== (unsigned int)sbit)
		{
			const unsigned short pos_x = (unsigned short)
				(pos % rules->bwidth);
			const unsigned short pos_y = (unsigned short)
				(pos / rules->bwidth);
			hnef_escval_walk(rules, board_escval,
				pos_x, pos_y, HNEF_AIM_ESCDIST_BASE);
		}
	}
	return	HNEF_FR_SUCCESS;
}

/*
 * Allocates all variables and initializes everything.
 *
 * Returning (as fr) HNEF_FR_FAIL_ILL_ARG means that p_index or
 * depth_max are invalid, but otherwise there are no fatal errors.
 *
 * Returning any other HNEF_FR_FAIL_* is program-fatal.
 *
 * HNEF_FR_SUCCESS means all parameters were valid and everything
 * worked.
 *
 * old_tab can be given to transfer an old hash table, so that it
 * doesn't have to be allocated again. If it's given (non-NULL), then it
 * will be preserved (not released!) if the AI fails to allocate.
 *
 * NOTE:	An old hash table can only be transferred for the same
 *		ruleset! Else the board length and other things will
 *		have changed, and then the hashmap has to be allocated
 *		again.
 *
 * NOTE:	An old hash table must have the same `mem_tab` and
 *		`mem_col`.
 *
 * This function requires that the game is valid according to
 * `hnef_game_valid()` and fully initialized.
 *
 * `mem_xyz must be valid according to `hnef_zhash_mem_xyz_valid()`.
 */
struct hnef_aiminimax *
hnef_alloc_aiminimax (
	const struct hnef_game	* const game,
	const unsigned short	p_index,
	const unsigned short	depth_max,
	enum HNEF_FR		* const HNEF_RSTR fr
#ifdef	HNEFATAFL_AIM_ZHASH
	,
	struct hnef_zhashtable	* const old_tab,
	const size_t		mem_tab,
	const size_t		mem_col
#endif	/* HNEFATAFL_AIM_ZHASH */
	)
{
	struct hnef_aiminimax	* aim	= NULL;
	unsigned short		i;

	assert	(NULL != game);
	assert	(NULL != fr);
#ifdef	HNEFATAFL_AIM_ZHASH
	assert	(hnef_zhash_mem_tab_valid(mem_tab));
	assert	(hnef_zhash_mem_col_valid(mem_col));
#endif	/* HNEFATAFL_AIM_ZHASH */

	/*
	 * HNEF_FR_FAIL_ILL_ARG are non-fatal.
	 */
	if	(!hnef_player_index_valid(p_index))
	{
		* fr	= HNEF_FR_FAIL_ILL_ARG;
		return	NULL;
	}
	if	(!hnef_aiminimax_depth_max_valid(depth_max))
	{
		* fr	= HNEF_FR_FAIL_ILL_ARG;
		return	NULL;
	}

	aim	= malloc(sizeof(* aim));
	if	(NULL == aim)
	{
		* fr	= HNEF_FR_FAIL_ALLOC;
		return	NULL;
	}

	aim->escape_ruleset	= HNEF_FALSE;
	for	(i = 0; i < game->playerc; ++i)
	{
		if	(HNEF_BIT_U8_EMPTY
			!= game->players[i]->opt_owned_esc)
		{
			/*
			 * Player has some piece that can escape.
			 */
			aim->escape_ruleset	= HNEF_TRUE;
			break;
		}
	}

	/*
	 * Set to NULL here for free_aiminimax.
	 */
	aim->board_escval	= NULL;
	aim->opt_moves		= NULL;
	aim->opt_buf_moves	= NULL;
	aim->opt_buf_board	= NULL;
	aim->opt_movehist	= NULL;

#ifdef	HNEFATAFL_AIM_ZHASH
	aim->tp_tab		= NULL;
#endif	/* HNEFATAFL_AIM_ZHASH */
	aim->p_index		= p_index;
	aim->depth_max		= depth_max;
	aim->opt_buf_len	= (unsigned short)(aim->depth_max + 1);

	aim->opt_moves	= hnef_alloc_listm(HNEF_AIM_OPT_LIST_CAP_DEF);
	if	(NULL == aim->opt_moves)
	{
		hnef_free_aiminimax	(aim);
		* fr			= HNEF_FR_FAIL_ALLOC;
		return			NULL;
	}

	aim->opt_movehist = hnef_alloc_listmh(HNEF_LISTMH_CAP_DEF);
	if	(NULL == aim->opt_movehist)
	{
		hnef_free_aiminimax	(aim);
		* fr			= HNEF_FR_FAIL_ALLOC;
		return			NULL;
	}

	aim->opt_buf_moves	= hnef_alloc_opt_buf_moves
				(aim->opt_buf_len);
	if	(NULL == aim->opt_buf_moves)
	{
		hnef_free_aiminimax	(aim);
		* fr			= HNEF_FR_FAIL_ALLOC;
		return			NULL;
	}

	aim->opt_buf_board	= hnef_alloc_opt_buf_board
				(aim->opt_buf_len,
				game->rules->opt_blen);
	if	(NULL == aim->opt_buf_board)
	{
		hnef_free_aiminimax	(aim);
		* fr			= HNEF_FR_FAIL_ALLOC;
		return			NULL;
	}

	/*
	 * NOTE:	Relying on `calloc` to 0-fill. This is fine
	 *		since we're not `NULL`-filling.
	 */
	aim->board_escval	= calloc((size_t)game->rules->opt_blen,
					sizeof(* aim->board_escval));
	if	(NULL == aim->board_escval)
	{
		hnef_free_aiminimax	(aim);
		* fr			= HNEF_FR_FAIL_ALLOC;
		return			NULL;
	}
	* fr	= hnef_aiminimax_board_escval_init
		(game, aim->board_escval);
	if	(HNEF_FR_SUCCESS != * fr)
	{
		hnef_free_aiminimax	(aim);
		return			NULL;
	}

#ifdef	HNEFATAFL_AIM_ZHASH
	if	(NULL == old_tab)
	{
/*
 * splint complains about a possible memory leak here. The problem is
 * this:
 *
 * 1.	When we create a completely new AI, we give it a NULL parameter
 *	as old_tab, indicating that it should be allocated.
 * 2.	When we create an AI to replace one that already exists (to
 *	change search depth), then it's unnecessary to allocate the
 *	whole table again, since it could be >100 MB. Then we just
 *	transfer the pointers.
 * 3.	Therefore aim->tp_tab has to be dependent, because ui.c will
 *	handle transferring it. If it's not NULL in free_aiminimax, then
 *	it will be freed -- else ui.c will free it.
 *
 * So there is no problem. I have tested it.
 */
/*@i1@*/\
		aim->tp_tab	= hnef_alloc_zhashtable
				(game, mem_tab, mem_col);
		if	(NULL == aim->tp_tab)
		{
			hnef_free_aiminimax	(aim);
			* fr			= HNEF_FR_FAIL_ALLOC;
			return			NULL;
		}
	}
	else
	{
		/*
		 * Old table given as parameter. Simply set the pointer.
		 *
		 * NOTE:	`z_j` is set to `opt_blen` when the
		 *		table is initialized. If they are not
		 *		equal now, then an incompatible hashmap
		 *		(allocated for a different ruleset) was
		 *		passed to the function, which is a
		 *		programming error.
		 */
		assert	(mem_tab == old_tab->mem_tab
		&&	mem_col == old_tab->mem_col);
		assert	(old_tab->z_j == game->rules->opt_blen);
		aim->tp_tab	= old_tab;
	}
#endif	/* HNEFATAFL_AIM_ZHASH */

	* fr	= HNEF_FR_SUCCESS;
	return aim;
}

void
hnef_free_aiminimax (
	struct hnef_aiminimax	* const HNEF_RSTR aim
	)
{
	assert	(NULL != aim);

	if	(NULL != aim->board_escval)
	{
		free	(aim->board_escval);
	}

#ifdef	HNEFATAFL_AIM_ZHASH
	if	(NULL != aim->tp_tab)
	{
		/*
		 * Note that we may set tp_tab to NULL when we create a
		 * new minimax player, in order to not have to
		 * re-allocate the hashtable (which is very large).
		 * Therefore we actually depend on this variable
		 * possibly being NULL, but only in this function.
		 */
		hnef_free_zhashtable	(aim->tp_tab);
	}
#endif	/* HNEFATAFL_AIM_ZHASH */

	if	(NULL != aim->opt_buf_board)
	{
		unsigned short i;
		for	(i = 0;
			i < (unsigned short)(aim->depth_max + 1); ++i)
		{
			hnef_free_board	(aim->opt_buf_board[i]);
		}
		free	(aim->opt_buf_board);
	}

	if	(NULL != aim->opt_buf_moves)
	{
		unsigned short i;
		for	(i = 0;
			i < (unsigned short)(aim->depth_max + 1); ++i)
		{
			hnef_free_listm	(aim->opt_buf_moves[i]);
		}
		free	(aim->opt_buf_moves);
	}

	if	(NULL != aim->opt_moves)
	{
		hnef_free_listm	(aim->opt_moves);
	}

	if	(NULL != aim->opt_movehist)
	{
		hnef_free_listmh(aim->opt_movehist);
	}

	free(aim);
}

