/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_UI_FILEFINDT_H
#define HNEF_UI_FILEFINDT_H

struct hnef_filefind
{

	/*
	 * `abs_path` is the absolute "-d" invocation parameter path (a
	 * directory), corresponding to the `HNEF_UI_ENVVAR_PATH`
	 * environment variable.
	 *
	 * `abs_runcom` is the absolute "-c" invocation parameter path
	 * (a file), corresponding to the `HNEF_UI_ENVVAR_RC`
	 * environment variable.
	 *
	 * `fname_lang` is the "-l" invocation parameter filename,
	 * corresponding to the `HNEF_UI_ENVVAR_LANG` environment
	 * variable.
	 *
	 * `fname_rules` is the "-r" invocation parameter filename,
	 * corresponding to the `HNEF_UI_ENVVAR_RULES` environment
	 * variable.
	 *
	 * Note that these settings take precedence over the environment
	 * variables.
	 *
	 * These are not `NULM`-terminated. These are invocation
	 * parameters, i.e. `argv` in `main()`. They don't have to be
	 * freed (in fact they can't be) and are read-only storage. If
	 * not set, they are `NULL`.
	 */
/*@null@*/
/*@temp@*/
	const char	* abs_path,
			* abs_runcom,
			* fname_lang,
			* fname_rules;

	/*
	 * This has length 0 if unset, or length > 0 if set.
	 *
	 * This is set if the user gives the `d <path>` command in an RC
	 * file, CLI or XLib. It overrides `-d` invocation parameter and
	 * `HNEF_UI_ENVVAR_PATH` (if set).
	 */
/*@notnull@*/
/*@owned@*/
	char		* abs_path_cmd;

	/*
	 * `true` to not read any RC file on program startup.
	 */
	HNEF_BOOL	runcom_suppress;

	/*
	 * `NULM`-terminated line for calls to `hnef_fopen_dirguess()`.
	 * This exists only so that we don't have to allocate a new line
	 * every time `hnef_fopen_dirguess()` is called (which would be
	 * wasteful).
	 */
/*@in@*/
/*@notnull@*/
/*@owned@*/
	char		* opt_dirguess;

};

#endif

