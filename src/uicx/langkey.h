/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_UI_LANGKEY_H
#define HNEF_UI_LANGKEY_H

#include "gleipnir_lang.h"	/* gleip_lang */

#include "hnefatafl.h"

#include "funcxt.h"	/* HNEF_FRX */

/*@in@*/
/*@null@*/
extern
const char *
hnef_langkey_get_fr (
/*@in@*/
/*@notnull@*/
/*@returned@*/
	const struct gleip_lang	* const GLEIP_RSTR,
	const enum HNEF_FR
	)
/*@globals errno@*/
/*@modifies errno@*/
;

/*@in@*/
/*@null@*/
extern
const char *
hnef_langkey_get_rread (
/*@in@*/
/*@notnull@*/
/*@returned@*/
	const struct gleip_lang	* const GLEIP_RSTR,
	const enum HNEF_RREAD
	)
/*@globals errno@*/
/*@modifies errno@*/
;

/*@in@*/
/*@null@*/
extern
const char *
hnef_langkey_get_rvalid (
/*@in@*/
/*@notnull@*/
/*@returned@*/
	const struct gleip_lang	* const GLEIP_RSTR,
	const enum HNEF_RVALID
	)
/*@globals errno@*/
/*@modifies errno@*/
;

/*@in@*/
/*@null@*/
/*@unused@*/	/* Unused without X */
extern
const char *
hnef_langkey_get_frx (
/*@in@*/
/*@notnull@*/
/*@returned@*/
	const struct gleip_lang	* const GLEIP_RSTR,
	const enum HNEF_FRX
	)
/*@globals errno@*/
/*@modifies errno@*/
;

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_langkey_init (
/*@in@*/
/*@notnull@*/
	struct gleip_lang	* const GLEIP_RSTR lang
	)
/*@globals errno@*/
/*@modifies errno, * lang@*/
;
/*@=protoparamname@*/

#endif

