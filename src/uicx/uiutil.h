/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_UIUTIL_H
#define HNEF_UIUTIL_H

#include "gleipnir_lines.h"	/* gleip_lines */

#include "hnefatafl.h"

#include "controlt.h"	/* HNEF_CONTROL_TYPE */
#include "funcxt.h"	/* HNEF_FRX */
#include "uit.h"	/* hnef_ui */

#ifdef	HNEFATAFL_UI_AIM

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_computer_move (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@in@*/
/*@null@*/
	void		* data,
/*@null@*/
	enum HNEF_FR	(* func_interrupt)
				(void *, HNEF_BOOL *, HNEF_BOOL *),
/*@null@*/
	enum HNEF_FR	(* func_progress)
				(void *, int, int),
/*@out@*/
/*@notnull@*/
	HNEF_BOOL	* const HNEF_RSTR success
	)
/*@modifies * data, * ui, * success@*/
;
/*@-protoparamname@*/

#endif	/* HNEFATAFL_UI_AIM */

/*@-protoparamname@*/
extern
enum HNEF_CONTROL_TYPE
hnef_control_type_get (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui
	)
/*@modifies * ui->game@*/
;
/*@=protoparamname@*/

#ifdef	HNEFATAFL_UI_AIM

/*@-protoparamname@*/
/*@unused@*/	/* Unused without X */
extern
HNEF_BOOL
hnef_control_computer (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui
	)
/*@modifies * ui@*/
;
/*@=protoparamname@*/

#endif	/* HNEFATAFL_UI_AIM */

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_ui_parseline_game_load_filename (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const char	* const HNEF_RSTR filename,
/*@out@*/
/*@notnull@*/
	HNEF_BOOL	* const HNEF_RSTR success,
/*@null@*/
	enum HNEF_FR	(* func_print_langkey)
				(struct hnef_ui	* const,
				enum HNEF_FRX	* const,
				const char	* const),
/*@null@*/
	enum HNEF_FR	(* func_print_fr)
				(struct hnef_ui	* const,
				enum HNEF_FRX	* const,
				const enum HNEF_FR),
/*@in@*/
/*@null@*/
	enum HNEF_FRX	* const HNEF_RSTR frx
	)
/*@globals errno, fileSystem, internalState@*/
/*@modifies errno, fileSystem, internalState, * ui, * success, * frx@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_ui_parseline_game_load (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line,
/*@out@*/
/*@notnull@*/
	HNEF_BOOL			* const HNEF_RSTR success,
/*@null@*/
	enum HNEF_FR			(* func_print_langkey)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const char	* const),
/*@null@*/
	enum HNEF_FR			(* func_print_fr)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const enum HNEF_FR),
/*@in@*/
/*@null@*/
	enum HNEF_FRX			* const HNEF_RSTR frx
	)
/*@globals errno, fileSystem, internalState@*/
/*@modifies errno, fileSystem, internalState, * ui, * success, * frx@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_ui_parseline_game_save (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line,
/*@null@*/
	enum HNEF_FR			(* func_print_langkey)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const char	* const),
/*@null@*/
	enum HNEF_FR			(* func_print_fr)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const enum HNEF_FR),
/*@in@*/
/*@null@*/
	enum HNEF_FRX			* const HNEF_RSTR frx
	)
/*@globals errno, fileSystem@*/
/*@modifies errno, fileSystem, * ui, * frx@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_ui_parseline_game_undo (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line,
/*@out@*/
/*@notnull@*/
	HNEF_BOOL			* const HNEF_RSTR success,
/*@null@*/
	enum HNEF_FR			(* func_print_langkey)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const char	* const),
/*@in@*/
/*@null@*/
	enum HNEF_FRX			* const HNEF_RSTR frx
	)
/*@globals errno@*/
/*@modifies errno, * ui, * success, * frx@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_ui_parseline_game_new (
/*@in@*/
/*@notnull@*/
	struct hnef_ui		* const HNEF_RSTR ui,
/*@null@*/
	enum HNEF_FR		(* func_print_langkey)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const char	* const),
/*@in@*/
/*@null@*/
	enum HNEF_FRX		* const HNEF_RSTR frx
	)
/*@modifies * ui, * frx@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_ui_parseline_lang_filename (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const char	* const HNEF_RSTR filename,
	const HNEF_BOOL	try_absolute,
/*@null@*/
	enum HNEF_FR	(* func_print_langkey)
				(struct hnef_ui	* const,
				enum HNEF_FRX	* const,
				const char	* const),
/*@null@*/
	enum HNEF_FR	(* func_print_fr)
				(struct hnef_ui	* const,
				enum HNEF_FRX	* const,
				const enum HNEF_FR),
/*@null@*/
	enum HNEF_FR	(* func_print_str)
				(struct hnef_ui	* const,
				enum HNEF_FRX	* const,
				const char	* const),
/*@in@*/
/*@null@*/
	enum HNEF_FRX		* const HNEF_RSTR frx
	)
/*@globals errno, fileSystem@*/
/*@modifies errno, fileSystem, * ui, * frx@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_ui_parseline_lang (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line,
	const HNEF_BOOL			try_absolute,
/*@null@*/
	enum HNEF_FR			(* func_print_langkey)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const char	* const),
/*@null@*/
	enum HNEF_FR			(* func_print_fr)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const enum HNEF_FR),
/*@null@*/
	enum HNEF_FR			(* func_print_str)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const char	* const),
/*@in@*/
/*@null@*/
	enum HNEF_FRX			* const HNEF_RSTR frx
	)
/*@globals errno, fileSystem@*/
/*@modifies errno, fileSystem, * ui, * frx@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_ui_parseline_rules_filename (
/*@in@*/
/*@notnull@*/
	struct hnef_ui		* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const char		* const HNEF_RSTR filename,
	const HNEF_BOOL		try_absolute,
/*@null@*/
	enum HNEF_FR		(* func_print_langkey)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const char	* const),
/*@null@*/
	enum HNEF_FR		(* func_print_fr)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const enum HNEF_FR),
/*@null@*/
	enum HNEF_FR		(* func_print_rread)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const enum HNEF_RREAD),
/*@null@*/
	enum HNEF_FR		(* func_print_rvalid)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const enum HNEF_RVALID),
/*@null@*/
	enum HNEF_FR		(* func_print_lines)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
				const struct gleip_lines * const),
/*@in@*/
/*@null@*/
	enum HNEF_FRX		* const HNEF_RSTR frx
	)
/*@globals errno, fileSystem, internalState@*/
/*@modifies errno, fileSystem, internalState, * ui, * frx@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_ui_parseline_rules (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line,
	const HNEF_BOOL			try_absolute,
/*@null@*/
	enum HNEF_FR			(* func_print_langkey)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const char	* const),
/*@null@*/
	enum HNEF_FR			(* func_print_fr)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const enum HNEF_FR),
/*@null@*/
	enum HNEF_FR			(* func_print_rread)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const enum HNEF_RREAD),
/*@null@*/
	enum HNEF_FR			(* func_print_rvalid)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const enum HNEF_RVALID),
/*@null@*/
	enum HNEF_FR			(* func_print_lines)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
				const struct gleip_lines * const),
/*@in@*/
/*@null@*/
	enum HNEF_FRX		* const HNEF_RSTR frx
	)
/*@globals errno, fileSystem, internalState@*/
/*@modifies errno, fileSystem, internalState, * ui, * frx@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_ui_parseline_player (
/*@in@*/
/*@notnull@*/
	struct hnef_ui			* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR line,
/*@null@*/
	enum HNEF_FR			(* func_print_langkey)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const,
					const char	* const),
/*@null@*/
	enum HNEF_FR			(* func_print_player)
					(struct hnef_ui	* const,
					const unsigned short,
					enum HNEF_FRX	* const),
/*@null@*/
	enum HNEF_FR			(* func_print_players)
					(struct hnef_ui	* const,
					enum HNEF_FRX	* const),
/*@in@*/
/*@null@*/
	enum HNEF_FRX			* const HNEF_RSTR frx
	)
/*@globals errno, internalState@*/

/*@modifies errno, * ui, * frx@*/
#ifdef	HNEFATAFL_UI_ZHASH
/*@modifies internalState@*/
#endif	/* HNEFATAFL_UI_ZHASH */
;
/*@=protoparamname@*/

#endif

