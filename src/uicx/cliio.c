/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */
#include <errno.h>	/* errno */
#include <math.h>	/* log10 */
#include <string.h>	/* strlen */

#include "gleipnir_line.h"	/* gleip_line_* */
#include "gleipnir_lineread.h"	/* gleip_lineread_* */

#include "hnefatafl.h"

#ifdef	HNEFATAFL_UI_AIM
#include "hnefatafl_aim.h"
#endif	/* HNEFATAFL_UI_AIM */

#include "cliio.h"
#include "cliiot.h"	/* HNEF_CMD_* */
#include "config.h"	/* HNEF_UI_ENVVAR_* */
#include "langkey.h"	/* hnef_langkey_* */

/*
 * The length (in characters) of a tabulator. This is used to align
 * printed text in columns. 8 is the standard.
 *
 * > Tabs are 8 characters, and thus indentations are also 8 characters.
 * > There are heretic movements that try to make indentations 4 (or
 * > even 2!) characters deep, and that is akin to trying to define the
 * > value of PI to be 3.
 * >
 * > -- Linux kernel coding style
 */
/*@unchecked@*/
static
const size_t	HNEF_UI_TABLEN	= (size_t)8;

/*
 * Returns the number of digits in `number`.
 *
 * `number` must be positive or 0. Negative numbers are not allowed.
 *
 * Returns a negative number for failure (which should never happen as
 * long as the parameter is valid).
 */
int
hnef_ui_digitc (
	const long	number
	)
{
	int	errno_old	= errno,
		digits;

	if	(0 == number)
	{
		return	1;
	}

	assert	(number > 0);

	errno	= 0;
	digits	= (int)(log10((double)number) + 0.0000001) + 1;
	if	(0 != errno)
	{
		/*
		 * Since `errno` is only set when the number is 0 or
		 * negative, this failure will never happen, since
		 * `number` > 0.
		 */
		errno	= errno_old;
		return	-1;
	}
	errno	= errno_old;

	assert	(digits > 0);
	return	digits;
}

enum HNEF_FR
hnef_print_langkey (
	const struct gleip_lang	* const GLEIP_RSTR lang,
	const char		* const HNEF_RSTR key,
	FILE			* const HNEF_RSTR out
	)
{
	const char	* HNEF_RSTR value	= NULL;

	assert	(NULL != lang);
	assert	(NULL != key);
	assert	(NULL != out);

	value	= gleip_lang_getany(lang, key);
	assert	(NULL != value);

	return	fprintf(out, "%s\n", value) < 0
		? HNEF_FR_FAIL_IO_WRITE
		: HNEF_FR_SUCCESS;
}

/*
 * Prints a `HNEF_FR` failure, and errno if errno is set.
 *
 * This function sets errno to 0.
 *
 * Unlike the other printing functions, `lang` is allowed to be `NULL`.
 * If so, it will print a hard-coded terse error code message which
 * probably doesn't make much sense.
 */
enum HNEF_FR
hnef_print_fail_fr (
	const struct gleip_lang	* const GLEIP_RSTR lang,
	const enum HNEF_FR	fr,
	FILE			* const HNEF_RSTR out
	)
{
	HNEF_BOOL	fail			= HNEF_FALSE;
	const char	* HNEF_RSTR value	= NULL,
			* HNEF_RSTR errnomsg	= NULL;

	assert	(HNEF_FR_SUCCESS != fr);
	assert	(NULL != out);

	if	(NULL == lang)
	{
		value	= NULL;
	}
	else
	{
		value	= hnef_langkey_get_fr(lang, fr);
	}

	if	(0 != errno)
	{
		errnomsg	= strerror(errno);
	}
	else
	{
		errnomsg	= NULL;
	}

	if	(NULL == value
	||	hnef_line_empty(value))
	{
		if	(NULL == errnomsg)
		{
			fail	= EOF == fprintf(out, "%s%d\n",
				HNEF_UI_ERROR_FR, (int)fr)	|| fail;
		}
		else
		{
			fail	= EOF == fprintf(out, "%s%d (%s)\n",
				HNEF_UI_ERROR_FR, (int)fr, errnomsg)
								|| fail;
		}
	}
	else
	{
		if	(NULL == errnomsg)
		{
			fail	= EOF == fprintf(out, "%s\n",
					value)			|| fail;
		}
		else
		{
			fail	= EOF == fprintf(out, "%s (%s)\n",
					value, errnomsg)	|| fail;
		}
	}

	/*
	 * The user has been informed so now we reset `errno`.
	 */
	errno	= 0;

	return	fail
		? HNEF_FR_FAIL_IO_WRITE
		: HNEF_FR_SUCCESS;
}

enum HNEF_FR
hnef_print_fail_rread (
	const struct gleip_lang	* const GLEIP_RSTR lang,
	const enum HNEF_RREAD	rread,
	FILE			* const HNEF_RSTR out
	)
{
	const char	* HNEF_RSTR value	= NULL;

	assert	(NULL != lang);
	assert	(HNEF_RREAD_SUCCESS != rread);
	assert	(NULL != out);

	value	= hnef_langkey_get_rread(lang, rread);

	if	(NULL == value
	||	hnef_line_empty(value))
	{
		return	EOF == fprintf(out, "%s%d\n",
				HNEF_UI_ERROR_RREAD, (int)rread)
			? HNEF_FR_FAIL_IO_WRITE
			: HNEF_FR_SUCCESS;
	}
	else
	{
		return	EOF == fprintf(out,
				"%s\n",
				value)
			? HNEF_FR_FAIL_IO_WRITE
			: HNEF_FR_SUCCESS;
	}
}

enum HNEF_FR
hnef_print_fail_rvalid (
	const struct gleip_lang	* const GLEIP_RSTR lang,
	const enum HNEF_RVALID	rvalid,
	FILE			* const HNEF_RSTR out
	)
{
	const char	* HNEF_RSTR value	= NULL;

	assert	(NULL != lang);
	assert	(HNEF_RVALID_SUCCESS != rvalid);
	assert	(NULL != out);

	value	= hnef_langkey_get_rvalid(lang, rvalid);

	if	(NULL == value
	||	hnef_line_empty(value))
	{
		return	EOF == fprintf(out, "%s%d\n",
				HNEF_UI_ERROR_RVALID, (int)rvalid)
			? HNEF_FR_FAIL_IO_WRITE
			: HNEF_FR_SUCCESS;
	}
	else
	{
		return	EOF == fprintf(out,
				"%s\n",
				value)
			? HNEF_FR_FAIL_IO_WRITE
			: HNEF_FR_SUCCESS;
	}
}

#ifdef	HNEFATAFL_UI_XLIB

enum HNEF_FR
hnef_print_fail_frx (
	const struct gleip_lang	* const GLEIP_RSTR lang,
	const enum HNEF_FRX	frx,
	FILE			* const HNEF_RSTR out
	)
{
	const char	* HNEF_RSTR value	= NULL;

	assert	(NULL != lang);
	assert	(HNEF_FRX_SUCCESS != frx);
	assert	(NULL != out);

	value	= hnef_langkey_get_frx(lang, frx);

	if	(NULL == value
	||	hnef_line_empty(value))
	{
		return	EOF == fprintf(out, "%s%d\n",
				HNEF_UI_ERROR_FRX, (int)frx)
			? HNEF_FR_FAIL_IO_WRITE
			: HNEF_FR_SUCCESS;
	}
	else
	{
		return	EOF == fprintf(out,
				"%s\n",
				value)
			? HNEF_FR_FAIL_IO_WRITE
			: HNEF_FR_SUCCESS;
	}
}

#endif	/* HNEFATAFL_UI_XLIB */

enum HNEF_FR
hnef_beep (void)
{
	HNEF_BOOL	fail	= HNEF_FALSE;

	fail	= EOF == fputc	('\a', stdout)	|| fail;
	fail	= EOF == fflush	(stdout)	|| fail;
	return	fail
		? HNEF_FR_FAIL_IO_WRITE
		: HNEF_FR_SUCCESS;
}

/*
 * Returns tab indentation of a string with length `len`, so that it
 * matches the indentation of a string with the maximum length `maxlen`.
 *
 * If `maxlen` is `SIZET_MAX`, then it will be treated as equal to `len`
 * and then this function will always return 1.
 */
size_t
hnef_textlen_tabs (
	const size_t	len,
	size_t		maxlen
	)
/*@modifies nothing@*/
{
	int	tabs;

	assert	(HNEF_UI_TABLEN > 0);

	if	(SIZET_MAX == maxlen)
	{
		maxlen	= len;
	}

	tabs	= (int)(maxlen	/ HNEF_UI_TABLEN)
		- (int)(len	/ HNEF_UI_TABLEN);
	if	(tabs < 0)
	{
		tabs	= 0;
	}

	return	(size_t)++tabs;
}

/*
 * `maxlen` is the length of the longest `varname` that is passed to
 * this function. It's used for alignment purposes when you call this
 * function several times. If `maxlen` is `SIZET_MAX`, it will be the
 * `strlen(varname)`.
 */
HNEF_BOOL
hnef_print_envvar_single (
	const char	* const HNEF_RSTR varname,
	size_t		maxlen
	)
{
	HNEF_BOOL	fail			= HNEF_FALSE;
	const char	* HNEF_RSTR varval	= NULL;
	size_t		len,
			tabs;

	assert	(NULL != varname);

	len	= strlen(varname);
	if	(SIZET_MAX == maxlen)
	{
		maxlen	= len;
	}

	tabs	= hnef_textlen_tabs	(len, maxlen);
	varval	= getenv		(varname);

	fail		= EOF == fputs(varname, stdout)		|| fail;
	while	(tabs-- > 0)
	{
		fail	= EOF == fputc('\t', stdout)		|| fail;
	}

	fail		= EOF == fputs("= ", stdout)		|| fail;
	if	(NULL == varval)
	{
		fail	= EOF == puts(HNEF_UI_VAR_UNSET)	|| fail;
	}
	else
	{
		fail	= printf("\"%s\"\n", varval) < 0	|| fail;
	}
	return	!fail;
}

enum HNEF_FR
hnef_print_envvar (void)
{
	HNEF_BOOL	fail	= HNEF_FALSE;
	const size_t	maxlen	= strlen(HNEF_UI_ENVVAR_RULES);

#ifdef	HNEFATAFL_UI_AIM
	fail	= !hnef_print_envvar_single(HNEF_UI_ENVVAR_SYS_DISPLAY,
		maxlen)	|| fail;
#endif	/* HNEFATAFL_UI_AIM */
	fail	= !hnef_print_envvar_single(HNEF_UI_ENVVAR_SYS_HOME,
		maxlen)	|| fail;
	fail	= !hnef_print_envvar_single(HNEF_UI_ENVVAR_SYS_LANG,
		maxlen)	|| fail;
	fail	= !hnef_print_envvar_single(HNEF_UI_ENVVAR_SYS_LC_ALL,
		maxlen)	|| fail;
	fail	= !hnef_print_envvar_single(HNEF_UI_ENVVAR_SYS_LC_CTYPE,
		maxlen)	|| fail;
#ifdef	HNEFATAFL_UI_XLIB
	fail	= !hnef_print_envvar_single(HNEF_UI_ENVVAR_FONT,
		maxlen)	|| fail;
#endif	/* HNEFATAFL_UI_XLIB */
	fail	= !hnef_print_envvar_single(HNEF_UI_ENVVAR_LANG,
		maxlen)	|| fail;
	fail	= !hnef_print_envvar_single(HNEF_UI_ENVVAR_PATH,
		maxlen)	|| fail;
	fail	= !hnef_print_envvar_single(HNEF_UI_ENVVAR_RC,
		maxlen)	|| fail;
	fail	= !hnef_print_envvar_single(HNEF_UI_ENVVAR_RULES,
		maxlen)	|| fail;

	return	fail
		? HNEF_FR_FAIL_IO_WRITE
		: HNEF_FR_SUCCESS;
}

enum HNEF_FR
hnef_print_copy (void)
{
	return	hnef_core_license_print(stdout)
		? HNEF_FR_FAIL_IO_WRITE
		: HNEF_FR_SUCCESS;
}

enum HNEF_FR
hnef_print_version (void)
{
	HNEF_BOOL	fail	= HNEF_FALSE;

	fail	= printf("%s\t\t%ld\n",
			"gleipnir_line", GLEIP_LINE_VERSION) < 0
		|| fail;
	fail	= printf("%s\t\t%ld\n",
			"gleipnir_lines", GLEIP_LINES_VERSION) < 0
		|| fail;
	fail	= printf("%s\t%ld\n",
			"gleipnir_lineread", GLEIP_LINEREAD_VERSION) < 0
		|| fail;
	fail	= printf("%s\t\t%ld\n",
			"gleipnir_lang", GLEIP_LANG_VERSION) < 0
		|| fail;

	fail	= printf("%s\t\t\t%ld\n",
			hnef_core_id(), HNEFATAFL_CORE_VERSION) < 0
		|| fail;
#ifdef	HNEFATAFL_UI_AIM
	fail	= printf("%s\t\t\t%ld\n",
			hnef_aim_id(), HNEFATAFL_AIM_VERSION) < 0
		|| fail;
#endif	/* HNEFATAFL_UI_AIM */
	fail	= printf("%s\t\t\t%ld\n",
			hnef_ui_id(), HNEFATAFL_UI_VERSION) < 0
		|| fail;

	return	fail
		? HNEF_FR_FAIL_IO_WRITE
		: HNEF_FR_SUCCESS;
}

/*
 * 2-9 is the complete Swedish national anthem, including the additions
 * by Louise Ahléns.
 *
 * These are only shown in X. They are also encoded in UTF-8 and
 * hard-coded, so you can print them to see if UTF-8 rendering works.
 * "ǫ" is only rendered if the XFontSet can handle it (in many cases it
 * can't). "åäöð" should work.
 */
/*@in@*/
/*@notnull@*/
/*@observer@*/
static
const char *
hnef_nine_brief_num (
	int	num
	)
/*@globals internalState@*/
/*@modifies internalState@*/
{
	if	(num < 1
	||	num > 9)
	{
		num	= 1 + (int)(9.0 * (rand() / (RAND_MAX + 1.0)));
	}
	assert	(num >= 1
	&&	num <= 9);

	if	(1 == num)
	{
		return	"Heill Óðinn Alfǫðr!";
	}
	else if	(2 == num)
	{
		return	"Du gamla, Du fria, Du fjällhöga nord, "
			"Du tysta, Du glädjerika sköna!";
	}
	else if	(3 == num)
	{
		return	"Jag hälsar Dig, vänaste land uppå jord, "
			"Din sol, Din himmel, Dina ängder gröna.";
	}
	else if	(4 == num)
	{
		return	"Du tronar på minnen från fornstora dar, "
			"då ärat Ditt namn flög över jorden.";
	}
	else if	(5 == num)
	{
		return	"Jag vet att Du är och Du blir vad Du var. "
			"Ja, jag vill leva jag vill dö i Norden.";
	}
	else if	(6 == num)
	{
		return	"Jag städs vill dig tjäna, mitt älskade land, "
			"Dig trohet till döden vill jag svära.";
	}
	else if	(7 == num)
	{
		return "Din rätt skall jag värna med håg och med hand, "
			"Din fana, högt den bragderika bära.";
	}
	else if	(8 == num)
	{
		return	"Med Gud skall jag kämpa för hem och för härd, "
			"för Sverige, den kära fosterjorden.";
	}
	else /* if (9 == num) */
	{
		return	"Jag byter Dig ej mot allt i en värld, "
			"Nej, jag vill leva jag vill dö i Norden!";
	}
}

/*
 * These are quotes about Hnefatafl in Old West Norse from the Edda,
 * Icelandic Sagas and other historical sources. The comments are the
 * translations (in Swedish, Danish and English).
 */
static
void
hnef_nine_print_num (
	int	num
	)
/*@globals errno, fileSystem, internalState, stdout@*/
/*@modifies errno, fileSystem, internalState, stdout@*/
{
	if	(num < 1
	||	num > 9)
	{
		num	= 1 + (int)(9.0 * (rand() / (RAND_MAX + 1.0)));
	}
	assert	(num >= 1
	&&	num <= 9);

	if	(1 == num)
	{
		(void)puts("Þar munu eftir");
		(void)puts("undrsamligar");
		(void)puts("gullnar tǫflur");
		(void)puts("í grasi finnask,");
		(void)puts("þærs í árdaga");
		(void)puts("áttar hǫfðu.");
		/*
		 * Där skola åter
		 * de underbara
		 * guldbrädspelsbrickorna
		 * i gräset hittas,
		 * som i tidens morgon
		 * dem tillhört hade.
		 */
	}
	else if	(2 == num)
	{
		(void)puts("Tefldu í túni,");
		(void)puts("teitir váru,");
		(void)puts("var þeim vettergis");
		(void)puts("vant ór gulli,");
		(void)puts("uns þrjár kvámu");
		(void)puts("þursa meyjar");
		(void)puts("ámáttkar mjǫk");
		(void)puts("ór Jǫtunheimum.");
		/*
		 * På gården med brädspel
		 * de glada lekte,
		 * armod på guld
		 * fanns ingalunda,
		 * tills tursamöar
		 * trenne kommo,
		 * mycket mäktiga
		 * mör, från jättevärlden.
		 */
	}
	else if	(3 == num)
	{
		(void)puts("Tafl emk ǫrr at efla,");
		(void)puts("íþróttir kank níu,");
		(void)puts("týnik trauðla rúnum,");
		(void)puts("tíð er bók ok smíðir,");
		(void)puts("skríða kank á skíðum,");
		(void)puts("skýtk ok rœk, svát nýtir;");
		(void)puts("hvártveggja kank hyggja:");
		(void)puts("harpslótt ok bragþóttu.");
		/*
		 * I can play at tafl,
		 * nine skills I know
		 * rarely forget I the runes,
		 * I know of books and smithing,
		 * I know how to slide on skis,
		 * shoot and row, well enough;
		 * each of two arts I know,
		 * harp-playing and speaking poetry.
		 */
	}
	else if	(4 == num)
	{
		(void)puts("Burr var inn ellsti,");
		(void)puts("en Barn annat,");
		(void)puts("Jóð ok Aðal,");
		(void)puts("Arfi, Mǫgr,");
		(void)puts("Niðr ok Niðjungr,");
		(void)puts("— námu leika, —");
		(void)puts("Sonr ok Sveinn,");
		(void)puts("— sund ok tafl, —");
		(void)puts("Kundr hét enn,");
		(void)puts("Konr var inn yngsti.");
		/*
		 * Bur var den äldste,
		 * och Barn den andre,
		 * Jod och Adal,
		 * Arve, Mog;
		 * lekar Nid
		 * och Nidjung lärde
		 * samt Son och Sven
		 * simning och brädspel;
		 * Kund hette en,
		 * Kon var den yngste.
		 */
	}
	else if	(5 == num)
	{
		(void)puts("Mjǫk eru reifðir");
		(void)puts("rógbirtingar,");
		(void)puts("þeirs í Haralds túni");
		(void)puts("húnum verpa;");
		(void)puts("féi eru þeir gœddir");
		(void)puts("ok fǫgrum mækum,");
		(void)puts("málmi húnlenzkum");
		(void)puts("ok mani austrœnu.");
		/*
		 * I höj grad begaves
		 * de krigere,
		 * som i Haralds gård
		 * leger med brikker;
		 * med gods de begaves
		 * og med blanke sværd,
		 * med hunnisk malm
		 * og østlandske trælkvinder.
		 */
	}
	else if	(6 == num)
	{
		(void)puts("Gestumblindi mælti:\n");
		(void)puts("\"Hverjar eru þær snótir,");
		(void)puts("er um sinn dróttin");
		(void)puts("vápnlausar vega,");
		(void)puts("inar jǫrpu hlífa");
		(void)puts("um alla daga,");
		(void)puts("en inar fegri frýja?");
		(void)puts("Heiðrekr konungr,");
		(void)puts("hygg þú at gátu.\"\n");
		(void)puts("\"Góð er gáta þín,");
		(void)puts("Gestumblindi,");
		(void)puts("getit er þeirar:\n");
		(void)fputs("Þat er hneftafl. "
				"Tǫflur drepast vápnlausar ", stdout);
		(void)puts("um hnefann, ok fylgja honum inar rauðu.\"");
		/*
		 * Da sagde Gest:
		 *
		 * Hvilke ere de Møer,
		 * som vaabenløse
		 * kæmpe om sin Herre?
		 * mod de hvides
		 * vilde Angreb
		 * de sorte ham stedse skjerme.
		 * Raad den Gaade
		 * rigtig, Kong Heidrek.
		 *
		 * Heidrek svarede:
		 *
		 * God er din Gaade,
		 * Gest hin blinde!
		 * gjettet er den:
		 * i damspil de sorte
		 * sin Drot forsvare
		 * mod Anfald af de hvide.
		 */
	}
	else if	(7 == num)
	{
		(void)puts("Gestumblindi mælti:\n");
		(void)puts("\"Hvat er þat dýra,");
		(void)puts("er drepr fé manna");
		(void)puts("ok er járni kringt útan,");
		(void)puts("horn hefir átta,");
		(void)puts("en hǫfuð ekki");
		(void)puts("ok rennr, sem renna má?");
		(void)puts("Heiðrekr konungr,");
		(void)puts("hygg þú at gátu.\"\n");
		(void)puts("\"Gód er gáta þín,");
		(void)puts("Gestumblindi,");
		(void)puts("getit er þeirar:\n");
		(void)puts("Þat er húnn í hneftafli; hann heitir som "
			"bjǫrn; hann rennr, þegar honum er kastat.\"");
		/*
		 * Da sagde Gest:
		 *
		 * Hvilket er det Dyr
		 * som dræber Folks Hjorde,
		 * helt af Jern forhudet?
		 * Otte Horn det har,
		 * men Hoved ikke,
		 * og tæller et talrigt Følge
		 * Raad den Gaade
		 * rigtig, Kong Heidrek.
		 *
		 * Heidrek svarade:
		 *
		 * God er din Gaade,
		 * Gest hin blinde!
		 * gjettet er den:
		 * det er Ræven,
		 * som rovbegjerlig
		 * i Damspil Faarene dræber.
		 */
	}
	else if	(8 == num)
	{
		(void)fputs("Jafnan skemmtu þau Helga sér at tafli ok "
			"Gunnlaugr. Lagði hvárt þeira góðan þokka til "
			"annars bráðliga, sem raunir bar á síðan. "
			"Þau váru mjǫk jafnaldrar.\n\n", stdout);
		(void)fputs("Helga var svá fǫgr, at þat er sǫgn fróðra "
			"manna, at hon hafi fegrst kona verit á "
			"Íslandi. Hár hennar var svá mikit, at þat "
			"mátti hylja hana alla, ok svá fagrt sem gull "
			"barit, ok ", stdout);
		(void)puts("engi kostr þótti þá þvílíkr sem Helga in "
			"fagra í ǫllum Borgarfirði ok víðara annars "
			"staðar.");
		/*
		 * Gunnlög och Helga muntrade sig tidt och ofta med att
		 * spela tafvel. Mycket snart fattade de godt tycke till
		 * hvarandra; hvilket tiden sedan skulle visa. De voro
		 * nästan jemnåriga. Helga var så fager, att sagokunnige
		 * män sagt, att hon varit Islands skönaste qvinna.
		 * Hennes hår var så ymnigt, att hon dermed kunde hölja
		 * sig helt och hållet, och det var vänt som smidt gull.
		 * Och så godt gifte som Helga den fagra fans ej i hela
		 * Borgarfjärden och vida om kring.
		 */
	}
	else /* if (9 == num) */
	{
		(void)fputs("En konungs synir sǫfnuðu liði ok fengu "
			"lítit lið ok sendu orð Friðþjófi ok báðu hann "
			"liðs, en hann sat at tafli, er sendimenn kómu "
			"til hans.\n\n", stdout);
		(void)fputs("Þeir mæltu: \"Konungar várar sendu þér "
			"kvedju ok vildu þiggja liðsinni þitt til "
			"orrostu með sér.\"\n\n", stdout);
		(void)puts("Hann svarade engu ok mælti við Bjǫrn: "
			"\"Leita ráðs fyrir at tefla, því at tvíkostr "
			"er þér á tvá vega,\" sagði hann.");
		/*
		 * Björn och Fritiof sutto båda
		 * vid ett schackbord, skönt att skåda.
		 * Silver var varannan ruta,
		 * och varannan var av guld.
		 *
		 * Då steg Hilding in. "Sitt neder!
		 * Upp i högbänk jag dig leder,
		 * töm ditt horn, och låt mig sluta
		 * spelet, fosterfader huld!"
		 *
		 * Hilding kvad: "Från Beles söner
		 * kommer jag till dig med böner.
		 * Tidningarna äro onde,
		 * och till dig står landets hopp."
		 *
		 * Fritiof kvar: "Tag dig till vara,
		 * Björn, ty nu är kung i fara.
		 * Frälsas kan han med en bonde:
		 * den är gjord att offras upp."
		 */
	}
}

void
hnef_nine_print (
	const char	* const HNEF_RSTR str
	)
{
	int	num;
	if	(NULL == str)
	{
		num		= -1;
	}
	else
	{
		if	(!hnef_texttoint(str, & num))
		{
			num	= -1;
		}
	}
	hnef_nine_print_num(num);
}

const char *
hnef_nine_brief (
	const char	* const HNEF_RSTR str
	)
{
	int	num;
	if	(NULL == str)
	{
		num		= -1;
	}
	else
	{
		if	(!hnef_texttoint(str, & num))
		{
			num	= -1;
		}
	}
	return	hnef_nine_brief_num(num);
}


const char *
hnef_nine_hint (void)
{
	/*
	 * Same as the `HNEF_CMD_EE_NINE`.
	 */
	return	"9";
}

