/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_UI_CONTROLT_H
#define HNEF_UI_CONTROLT_H

#ifdef	HNEFATAFL_UI_AIM
#include "hnefatafl.h"
#include "hnefatafl_aim.h"
#endif	/* HNEFATAFL_UI_AIM */

#include "config.h"	/* HNEFATAFL_UI_ZHASH */

#ifdef	HNEFATAFL_UI_AIM
/*
 * Default / invalid value for AIM parameter `depth`.
 */
/*@unchecked@*/
extern
const unsigned short	HNEF_UI_AIMARG_DEPTH_INVALID;

#ifdef	HNEFATAFL_UI_ZHASH
/*
 * Default / invalid value for AIM parameters `mem_tab` and `mem_col`.
 */
/*@unchecked@*/
extern
const size_t		HNEF_UI_AIMARG_MEM_INVALID;
#endif	/* HNEFATAFL_UI_ZHASH */
#endif	/* HNEFATAFL_UI_AIM */

enum HNEF_CONTROL_TYPE
{

	HNEF_CONTROL_TYPE_HUMAN

#ifdef	HNEFATAFL_UI_AIM
	,
	HNEF_CONTROL_TYPE_AIM
#endif	/* HNEFATAFL_UI_AIM */

};

/*
 * A player controller.
 */
struct hnef_control
{

	/*
	 * Initially human.
	 */
	enum HNEF_CONTROL_TYPE	type;

#ifdef	HNEFATAFL_UI_AIM
	/*
	 * If `type` is `HNEF_CONTROL_TYPE_AIM`, then this is set to a
	 * minimax struct. However, if the game is invalid, it's set to
	 * `NULL` until a valid game is created, at which point it is
	 * allocated lazily.
	 *
	 * This is and remains `NULL` if `type` is
	 * `HNEF_CONTROL_TYPE_HUMAN`.
	 */
/*@null@*/
/*@owned@*/
	struct hnef_aiminimax	* aiminimax;

	unsigned short		aiminimax_depth;

#ifdef	HNEFATAFL_UI_ZHASH

	size_t			aiminimax_mem_tab,
				aiminimax_mem_col;

#endif	/* HNEFATAFL_UI_ZHASH */
#endif	/* HNEFATAFL_UI_AIM */

};

#endif

