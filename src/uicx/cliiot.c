/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include "cliiot.h"

const char	* const	HNEF_UI_PROMPT		= "> ",
		* const HNEF_UI_VAR_UNSET	= "NULL",
		* const HNEF_UI_ERROR_FR	= "FEL / ERROR FR=",
		* const HNEF_UI_ERROR_RREAD	= "FEL / ERROR RREAD=",
		* const HNEF_UI_ERROR_RVALID	= "FEL / ERROR RVALID=",
		* const HNEF_UI_ERROR_FRX	= "FEL / ERROR FRX=";

