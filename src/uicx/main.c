/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */
#include <errno.h>	/* errno */
#include <locale.h>	/* setlocale */
#include <stdlib.h>	/* EXIT_* */
#include <string.h>	/* strlen */
#include <time.h>	/* time */

#include "gleipnir_lang.h"	/* gleip_lang_* */

#include "cli.h"	/* run_cli */
#include "cliio.h"	/* hnef_print_* */
#include "cliiot.h"	/* HNEF_CMD_* */
#include "langkey.h"	/* hnef_langkey_* */
#include "ui.h"		/* hnef_ui_* */

/*
 * Prints brief usage instructions.
 */
static
enum HNEF_FR
hnef_print_invoc_help (
/*@in@*/
/*@notnull@*/
	FILE	* const HNEF_RSTR out
	)
/*@globals fileSystem@*/
/*@modifies fileSystem, out@*/
{
	/*
	 * Printing invocation help from language file strings may not
	 * be appropriate if the language file hasn't been loaded (the
	 * user may want help on how to load it). So this printout is
	 * very terse and uses hard-coded strings (Swedish, of course).
	 */
	const char	* const HNEF_RSTR path		= "sökväg",
			* const HNEF_RSTR filename	= "filnamn",
			* const HNEF_RSTR variable	= "variabel",
			* const HNEF_RSTR theme		= "tema";

	assert	(NULL != out);

	/*
	 * NOTE:	Help for these is not printed:
	 *		-	`HNEF_CMD_EE_NINE`
	 *		-	`HNEF_CMD_EE_HINT`
	 */

	/*
	 * It would be convenient to have `%n$s` here, but that's a
	 * POSIX extensions that's not even in C99. This program should
	 * be C89-compatible.
	 */
	return	fprintf(out,
			"hnefatafl [-ChiLsv] [-c <%s>] [-d <%s>] "
			"[-e <%s>] [-l <%s>] [-n <%s>] "
			"[-r <%s>] [-x <%s>]\n",
			path, path,
			path, filename, variable,
			filename, theme
			) < 0
		? HNEF_FR_FAIL_IO_WRITE
		: HNEF_FR_SUCCESS;
}

/*
 * Returns 0 if the program should continue, or !0 if the program should
 * terminate. < 0 is a failure and > 0 is success.
 */
static
int
hnef_invoc (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
	const int	argc,
/*@in@*/
/*@notnull@*/
	const char	* const * const argv,
/*@out@*/
/*@notnull@*/
	HNEF_BOOL	* const HNEF_RSTR locale_suppress,
/*@out@*/
/*@notnull@*/
	const char	* * const game_load_fname,
/*@out@*/
/*@notnull@*/
	HNEF_BOOL	* const HNEF_RSTR run_x_then_quit,
/*@out@*/
/*@notnull@*/
	const char	* * const xui_theme
	)
/*@globals errno, fileSystem, internalState, stdout@*/
/*@modifies errno, fileSystem, internalState, stdout@*/
/*@modifies * ui, * locale_suppress, * game_load_fname@*/
/*@modifies * run_x_then_quit, * xui_theme@*/
{
	const int		success	= 1,
				fail	= -1,
				cont	= 0;
	struct hnef_filefind	* HNEF_RSTR ff	= NULL;
	int			i;

	assert	(NULL != ui);
	assert	(NULL != argv);

	* locale_suppress	= HNEF_FALSE;
	* game_load_fname	= NULL;
	* run_x_then_quit	= HNEF_FALSE;
	* xui_theme		= NULL;
	ff			= ui->filefind;

	for	(i = 1; i < argc; ++i)
	{
		const char	* const arg	= argv[i];
		const size_t	arglen		= strlen(arg);
		size_t		j;

		if	(arglen < (size_t)2
		||	'-' != arg[0])
		{
			/*
			 * Invocation argument doesn't begin with "-".
			 */
			return	fail;
		}

		for	(j = (size_t)1; j < arglen; ++j)
		{
			const char	ch	= arg[j];
			const char	* * ptr	= NULL;
			switch	(ch)
			{
				case	HNEF_CMD_LANG:
					ptr = & ff->fname_lang;
/*@fallthrough@*/
				case	HNEF_CMD_PATH:
					if	(NULL == ptr)
					{
						ptr = & ff->abs_path;
					}
/*@fallthrough@*/
				case	HNEF_CMD_RC:
					if	(NULL == ptr)
					{
						ptr = & ff->abs_runcom;
					}
/*@fallthrough@*/
				case	HNEF_CMD_RULES:
					if	(NULL == ptr)
					{
						ptr = & ff->fname_rules;
					}
					if	(++i >= argc)
					{
						/*
						 * Missing argument.
						 */
						return	fail;
					}
					assert	(NULL != ptr);
					* ptr	= argv[i];
/*@switchbreak@*/
					break;
				case	HNEF_CMD_LOCALE_SUPPRESS:
					* locale_suppress = HNEF_TRUE;
/*@switchbreak@*/
					break;
				case	HNEF_CMD_RC_SUPPRESS:
					ui->filefind->runcom_suppress
						= HNEF_TRUE;
/*@switchbreak@*/
					break;
				case	HNEF_CMD_COPY:
					(void)hnef_print_copy();
					return	success;
				case	HNEF_CMD_EE_NINE:
					hnef_nine_print(++i < argc
						? argv[i] : NULL);
					return	success;
				case	HNEF_CMD_EE_HINT:
					(void)puts(hnef_nine_hint());
					return	success;
				case	HNEF_CMD_ENVVAR:
					if	(++i < argc)
					{
						(void)
						hnef_print_envvar_single
						(argv[i], SIZET_MAX);
					}
					else
					{
						(void)
						hnef_print_envvar();
					}
					return	success;
				case	HNEF_CMD_GAME_LOAD:
					if	(++i < argc)
					{
						* game_load_fname =
							argv[i];
/*@switchbreak@*/
						break;
					}
					else
					{
						return	fail;
					}
				case	HNEF_CMD_HELP:
					(void)hnef_print_invoc_help
						(stdout);
					return	success;
				case	HNEF_CMD_LANG_PRINT:
					(void)gleip_lang_print(ui->lang,
						stdout, '\n');
					return	success;
				case	HNEF_CMD_VERSION:
					(void)hnef_print_version();
					return	success;
				case	HNEF_CMD_XLIB:
					* run_x_then_quit = HNEF_TRUE;
					if	(i + 1 < argc
/*@i1@*/\
					&&	!gleip_line_empty
							(argv[i + 1])
					&&	'-' != argv[i + 1][0])
					{
						* xui_theme = argv[++i];
					}
/*@switchbreak@*/
					break;
				default:
					/*
					 * Unknown switch.
					 */
					return	fail;
			}
		}
	}
	return	cont;
}

int
main (
	const int	argc,
	const char	* const * const argv
	)
/*@globals errno, fileSystem, internalState, stdin, stderr, stdout@*/
/*@modifies errno, fileSystem, internalState, stdin, stderr, stdout@*/
{
	HNEF_BOOL	locale_suppress		= HNEF_FALSE,
			run_x_then_quit		= HNEF_FALSE;
	const char	* xui_theme		= NULL,
			* game_load_fname	= NULL;
	int		invoc_ret;
	struct hnef_ui	* HNEF_RSTR ui		= NULL;
	enum HNEF_FR	fr			= HNEF_FR_SUCCESS;

	/*
	 * Needed for minimax player and invoc.
	 */
	srand	((unsigned int)time(NULL));

	ui	= hnef_alloc_ui(& fr);
	if	(NULL == ui)
	{
		if	(!hnef_fr_good(fr))
		{
			(void)hnef_print_fail_fr(NULL, fr, stderr);
		}
		else
		{
			(void)hnef_print_fail_fr(NULL,
				HNEF_FR_FAIL_ALLOC, stderr);
		}
		return	EXIT_FAILURE;
	}

	invoc_ret	= hnef_invoc(ui, argc, argv,
			& locale_suppress,
			& game_load_fname,
			& run_x_then_quit,
			& xui_theme);
	if	(invoc_ret < 0)
	{
		(void)hnef_print_invoc_help(stderr);
		hnef_free_ui(ui);
		return	EXIT_FAILURE;
	}
/*@i1@*/\
	else if	(invoc_ret > 0)
	{
		hnef_free_ui(ui);
		return	EXIT_SUCCESS;
	}

	/*
	 * 1.	Try `LC_ALL`	= `""` (`$LC_ALL`).
	 * 2.	Try `LC_CTYPE`	= `""` (`$LC_CTYPE`).
	 * 3.	Try `LC_ALL`	= `getenv("LANG")`.
	 * 4.	Try `LC_CTYPE`	= `getenv("LANG")`.
	 *
	 * `LC_ALL` is everything and `LC_CTYPE` is string handling
	 * (except for some `is*` functions), so we try `LC_ALL` first.
	 *
	 * Maybe `$LC_*` are not set but `$LANG` is, hence steps 3-4.
	 * This really is ugly but sometimes works.
	 */
	if	(!locale_suppress)
	{
		if	(NULL == (ui->setlocale_return
					= setlocale(LC_ALL, ""))
		&&	NULL == (ui->setlocale_return
					= setlocale(LC_CTYPE, "")))
		{
			const char	* const HNEF_RSTR lang =
				getenv(HNEF_UI_ENVVAR_SYS_LANG);

			if	(NULL != lang
			&&	NULL == (ui->setlocale_return
					= setlocale(LC_ALL, lang))
			&&	NULL == (ui->setlocale_return
					= setlocale(LC_CTYPE, lang)))
			{
				(void)fputs("!setlocale(); LC_ALL = ?, "
					"LC_CTYPE = ?, LANG = ?\n",
					stderr);
			}
		}
	}

	fr	= hnef_run_cli(ui,
			game_load_fname,
			run_x_then_quit,
			xui_theme);
	if	(!hnef_fr_good(fr))
	{
		(void)hnef_print_fail_fr(ui->lang, fr, stderr);
		hnef_free_ui(ui);
		return	EXIT_FAILURE;
	}

	hnef_free_ui(ui);
	return	EXIT_SUCCESS;
}

