/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_UI_CLI_H
#define HNEF_UI_CLI_H

#include "uit.h"

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_run_cli (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@in@*/
/*@null@*/
	const char	* const HNEF_RSTR game_load_fname,
	const HNEF_BOOL	run_x_then_quit,
/*@in@*/
/*@null@*/
	const char	* const HNEF_RSTR xui_theme
	)
#ifdef	HNEFATAFL_UI_XLIB
/*@globals errno, fileSystem, internalState, stderr, stdin, stdout@*/
/*@modifies errno, fileSystem, internalState, stderr, stdin, stdout@*/
#else	/* HNEFATAFL_UI_XLIB */
/*@globals errno, fileSystem, internalState, stderr, stdin, stdout@*/
/*@modifies errno, fileSystem, internalState, stderr, stdin, stdout@*/
#endif	/* HNEFATAFL_UI_XLIB */
/*@modifies * ui@*/
;
/*@=protoparamname@*/

#endif

