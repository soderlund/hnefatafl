/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_UI_FILEFIND_H
#define HNEF_UI_FILEFIND_H

#include "hnefatafl.h"

#include "filefindt.h"	/* hnef_filefind */

/*@-protoparamname@*/
/*@dependent@*/
/*@in@*/
/*@null@*/
extern
FILE *
hnef_fopen_dirguess (
/*@in@*/
/*@notnull@*/
	struct hnef_filefind	* const HNEF_RSTR filefind,
/*@in@*/
/*@null@*/
	const char		* const HNEF_RSTR subdir,
/*@in@*/
/*@notnull@*/
	const char		* const HNEF_RSTR filename,
	const HNEF_BOOL		try_absolute,
/*@out@*/
/*@notnull@*/
	enum HNEF_FR		* const HNEF_RSTR fr
	)
/*@globals errno, fileSystem@*/
/*@modifies errno, fileSystem, * filefind, * fr@*/
;
/*@=protoparamname@*/

/*@in@*/
/*@notnull@*/
/*@unused@*/	/* Unused without X */
extern
char *
hnef_fopen_filename (
/*@in@*/
/*@notnull@*/
/*@returned@*/
	struct hnef_filefind	* const HNEF_RSTR
	)
/*@modifies nothing@*/
;

/*@in@*/
/*@null@*/
/*@only@*/
extern
struct hnef_filefind *
hnef_alloc_filefind (void)
/*@modifies nothing@*/
;

/*@-protoparamname@*/
extern
void
hnef_free_filefind (
/*@in@*/
/*@notnull@*/
/*@owned@*/
	struct hnef_filefind	* const HNEF_RSTR filefind
	)
/*@modifies filefind@*/
/*@releases filefind@*/
;
/*@=protoparamname@*/

#endif

