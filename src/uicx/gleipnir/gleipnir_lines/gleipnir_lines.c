/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>		/* assert */
#include <stdlib.h>		/* malloc, free, realloc */
#include <string.h>		/* strlen */

#include "gleipnir_line.h"	/* gleip_line_* */

#include "gleipnir_lines.h"

const long	GLEIP_LINES_VERSION	= 140816L;

/*@unchecked@*/
static
const size_t	GLEIP_LINES_MEM_DEF	= (size_t)9;

/*@unchecked@*/
static
const double	GLEIP_LINES_MEM_GROWF	= 1.5;

const char *
gleip_lines_get_ro (
	const struct gleip_lines	* const GLEIP_RSTR lines,
	const size_t			i
	)
{
	assert	(NULL != lines);
	assert	(i < lines->size);

	return	lines->arr[i];
}

char *
gleip_lines_get (
	struct gleip_lines	* const GLEIP_RSTR lines,
	const size_t		i
	)
{
	assert	(NULL != lines);
	assert	(i < lines->size);

	return	lines->arr[i];
}

char * *
gleip_lines_get_ptr (
	struct gleip_lines	* const GLEIP_RSTR lines,
	const size_t		i
	)
{
	assert	(NULL != lines);
	assert	(i < lines->size);

	return	& lines->arr[i];
}

size_t
gleip_lines_mem (
	const struct gleip_lines	* const GLEIP_RSTR lines
	)
{
	assert	(NULL != lines);

	return	lines->mem;
}

size_t
gleip_lines_size (
	const struct gleip_lines	* const GLEIP_RSTR lines
	)
{
	assert	(NULL != lines);

	return	lines->size;
}

int
gleip_lines_empty (
	const struct gleip_lines	* const GLEIP_RSTR lines
	)
{
	assert	(NULL != lines);

/*@+tmpcomments@*/
/*@t1@*/\
	return	lines->size < (size_t)1;
/*@=tmpcomments@*/
}

int
gleip_lines_mem_trim (
	struct gleip_lines	* const GLEIP_RSTR lines
	)
{
	int	retval	= 1;
	size_t	mem_new,
		i;

	assert	(NULL != lines);
	assert	(NULL != lines->arr);

	mem_new	= lines->size;
	if	(mem_new < (size_t)1)
	{
		mem_new	= (size_t)1;
	}

	if	(lines->mem > mem_new)
	{
		char	* * arr_new	= NULL;

		for	(i = mem_new; i < lines->mem; ++i)
		{
			char * const GLEIP_RSTR line = lines->arr[i];
			if	(NULL != line)
			{
				free		(line);
				lines->arr[i]	= NULL;
			}
		}

		arr_new = realloc(lines->arr, sizeof(* lines->arr)
			* mem_new);
		if	(NULL == arr_new)
		{
/*@+tmpcomments@*/
/*@t2@*/\
			return	0;
/*@=tmpcomments@*/
		}

		lines->arr	= arr_new;
		lines->mem	= mem_new;
	}

	for	(i = 0; i < lines->mem; ++i)
	{
		char	* const line	= lines->arr[i],
			* tmp		= NULL;
		if	(NULL != line)
		{
			tmp		= gleip_line_mem_trim(line);
			if	(NULL == tmp)
			{
				retval	= 0;
			}
			else
			{
				lines->arr[i]	= tmp;
			}
		}
	}

/*@+tmpcomments@*/
/*@t1@*/\
	return	retval;
/*@=tmpcomments@*/
}

int
gleip_lines_mem_grow (
	struct gleip_lines	* const GLEIP_RSTR lines,
	const size_t		mem
	)
{
	size_t	i;
	char	* * arr_new	= NULL;

	assert	(NULL != lines);
	assert	(NULL != lines->arr);

	if	(lines->mem >= mem)
	{
		return	1;
	}
	assert	(mem > 0);

	arr_new = realloc(lines->arr, sizeof(* lines->arr) * mem);
	if	(NULL == arr_new)
	{
/*@+tmpcomments@*/
/*@t2@*/\
		return	0;
/*@=tmpcomments@*/
	}

	for	(i = lines->mem; i < mem; ++i)
	{
		arr_new[i]	= NULL;
	}

	lines->arr	= arr_new;
	lines->mem	= mem;
/*@+tmpcomments@*/
/*@t1@*/\
	return		1;
/*@=tmpcomments@*/
}

static
int
gleip_lines_mem_grow_auto (
/*@in@*/
/*@notnull@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
	const size_t		mem_min
	)
/*@modifies * lines->arr, lines->arr, lines->mem@*/
{
	size_t	mem_aut;

	assert	(NULL != lines);
	assert	(NULL != lines->arr);

	mem_aut	= (size_t)(lines->mem
		* GLEIP_LINES_MEM_GROWF + 0.000001);
	return	gleip_lines_mem_grow(lines,
		mem_aut < mem_min
		? mem_min
		: mem_aut);
}

static
size_t
gleip_lines_mem_best_pref_len (
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR lines,
	const size_t			i_begin,
	const size_t			len
	)
/*@*/
{
	size_t		i,
			i_best,
			mem_best;
	const size_t	mem_pref	= len + 2;

	assert	(NULL != lines);
	assert	(lines->size < lines->mem);
	assert	(i_begin < lines->mem);
	assert	(i_begin >= lines->size);

	i_best		= i_begin;
	mem_best	= 0;

	for	(i = i_begin; i < lines->mem; ++i)
	{
		const char * const GLEIP_RSTR line = lines->arr[i];

		if	(NULL != line)
		{
			const size_t	mem	= gleip_line_mem(line);

			if	(mem == mem_pref)
			{
				return	i;
			}

			if	(i_begin == i)
			{
				i_best			= i;
				mem_best		= mem;
			}
			else if	(mem >= mem_pref)
			{
				if	(mem < mem_best
				||	mem_best < mem_pref)
				{
					i_best		= i;
					mem_best	= mem;
				}
			}
/*@+tmpcomments@*/
/*@t1@*/\
			else if	(mem > mem_best)
/*@=tmpcomments@*/
			{
				i_best			= i;
				mem_best		= mem;
			}
		}
	}

	return	i_best;
}

int
gleip_lines_addcpy (
	struct gleip_lines	* const GLEIP_RSTR lines,
	const char		* const GLEIP_RSTR line,
	const size_t		i
	)
{
	size_t	i_use;
/*@temp@*/
	char	* tmp	= NULL;

	assert	(NULL != lines);
	assert	(NULL != line);
	assert	(i <= lines->size);

	if	(lines->size < lines->mem)
	{
		i_use	= gleip_lines_mem_best_pref_len
			(lines, lines->size, strlen(line));
		assert	(i_use >= lines->size
		&&	i_use < lines->mem);

		if	(NULL == lines->arr[i_use])
		{
			lines->arr[i_use] = gleip_line_alloc_cpy(line);
			if	(NULL == lines->arr[i_use])
			{
/*@+tmpcomments@*/
/*@t1@*/\
				return	0;
/*@=tmpcomments@*/
			}
		}
		else
		{
/*@+tmpcomments@*/
/*@t1@*/\
			if	(!gleip_line_cpy_b
					(& lines->arr[i_use], line))
/*@=tmpcomments@*/
			{
				return	0;
			}
		}
	}
	else
	{
		i_use	= lines->size;
		assert	(i_use ==	lines->size);
		assert	(i_use ==	lines->mem);

/*@+tmpcomments@*/
/*@t1@*/\
		if (!gleip_lines_mem_grow_auto(lines, lines->size + 1))
/*@=tmpcomments@*/
		{
			return	0;
		}
		assert	(i_use ==	lines->size);
		assert	(i_use <	lines->mem);
		assert	(NULL == lines->arr[i_use]);

		lines->arr[i_use]	= gleip_line_alloc_cpy(line);
		if	(NULL == lines->arr[i_use])
		{
/*@+tmpcomments@*/
/*@t1@*/\
			return	0;
/*@=tmpcomments@*/
		}
	}
	assert	(NULL != lines->arr[i_use]);
	assert	(0 == strcmp(line, lines->arr[i_use]));

	tmp	= lines->arr[i_use];
	assert	(i <= i_use);
	if	(i != i_use)
	{
		memmove	(& lines->arr[i + 1], & lines->arr[i],
			sizeof(* lines->arr) * (i_use - i));
	}
	++lines->size;
	lines->arr[i]	= tmp;
	return		1;
}

int
gleip_lines_addcpy_back (
	struct gleip_lines	* const GLEIP_RSTR lines,
	const char		* const GLEIP_RSTR line
	)
{
	assert	(NULL != lines);
	assert	(NULL != line);

	return	gleip_lines_addcpy(lines, line, lines->size);
}

static
size_t
gleip_lines_mem_best_pref_null (
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR lines
	)
/*@*/
{
	size_t	i,
		i_best,
		mem_best;

	assert	(NULL != lines);
	assert	(lines->size < lines->mem);

	i_best		= lines->size;
	mem_best	= 0;

	for	(i = lines->size; i < lines->mem; ++i)
	{
		const char * const GLEIP_RSTR line = lines->arr[i];
		size_t	mem;
		if	(NULL == line)
		{
			return	i;
		}

		mem	= gleip_line_mem(line);
		if	(lines->size == i
		||	mem < mem_best)
		{
			i_best		= i;
			mem_best	= mem;
		}
	}

	return	i_best;
}

int
gleip_lines_addref (
	struct gleip_lines	* const GLEIP_RSTR lines,
	char			* const GLEIP_RSTR ref,
	const size_t		i
	)
{
	size_t	i_use;

	assert	(NULL != lines);
	assert	(NULL != ref);
	assert	(i <= lines->size);

	if	(lines->size < lines->mem)
	{
		i_use	= gleip_lines_mem_best_pref_null(lines);
		assert	(i_use >= lines->size
		&&	i_use < lines->mem);

		if	(NULL != lines->arr[i_use])
		{
/*@+tmpcomments@*/
/*@t2@*/\
			free	(lines->arr[i_use]);
/*@=tmpcomments@*/
			lines->arr[i_use]	= NULL;
		}
	}
	else
	{
		i_use	= lines->size;
		assert	(i_use ==	lines->size);
		assert	(i_use ==	lines->mem);

/*@+tmpcomments@*/
/*@t1@*/\
		if (!gleip_lines_mem_grow_auto(lines, lines->size + 1))
/*@=tmpcomments@*/
		{
/*@+tmpcomments@*/
/*@t1@*/\
			return	0;
/*@=tmpcomments@*/
		}
		assert	(i_use ==	lines->size);
		assert	(i_use <	lines->mem);
	}
	assert	(NULL == lines->arr[i_use]);

	assert	(i <= i_use);
	if	(i != i_use)
	{
		memmove	(& lines->arr[i + 1], & lines->arr[i],
			sizeof(* lines->arr) * (i_use - i));
	}
	++lines->size;
/*@+tmpcomments@*/
/*@t3@*/\
	lines->arr[i]	= ref;
/*@=tmpcomments@*/
	return		1;
}

int
gleip_lines_addref_back (
	struct gleip_lines	* const GLEIP_RSTR lines,
	char			* const GLEIP_RSTR ref
	)
{
	assert	(NULL != lines);
	assert	(NULL != ref);

	return	gleip_lines_addref(lines, ref, lines->size);
}

void
gleip_lines_rm_range (
	struct gleip_lines	* const GLEIP_RSTR lines,
	size_t			i,
	const size_t		count
	)
{
	size_t	imax;

	assert	(NULL != lines);
	assert	(NULL != lines->arr);
	assert	(i		< lines->size);
	assert	(count		<= lines->size);
	assert	(i + count	<= lines->size);

	imax	= i + count;

	for	(; i < imax; ++i)
	{
		const size_t	j	= i + count;
		char		* tmp	= NULL;

		tmp			= lines->arr[i];
		gleip_line_rm		(tmp);
		if	(j < lines->size)
		{
			lines->arr[i]	= lines->arr[j];
			lines->arr[j]	= tmp;
		}
	}
	lines->size	-= count;
}

void
gleip_lines_rm_range_back (
	struct gleip_lines	* const GLEIP_RSTR lines,
	const size_t		count
	)
{
	assert	(NULL != lines);
	assert	(NULL != lines->arr);
	assert	(count <= lines->size);

	gleip_lines_rm_range(lines, lines->size - count, count);
}

void
gleip_lines_rm_line (
	struct gleip_lines	* const GLEIP_RSTR lines,
	const size_t		i
	)
{
	assert	(NULL != lines);
	assert	(NULL != lines->arr);
	assert	(i < lines->size);
	assert	(lines->size > 0);

	if	(i + 1 < lines->size)
	{
		char	* const tmp	= lines->arr[i];

		assert	(lines->size > 0);
		assert	(lines->size - i - 1 > 0);

		memmove	(& lines->arr[i], & lines->arr[i + 1],
			sizeof(* lines->arr) * (lines->size - i - 1));
		lines->arr[lines->size - 1]	= tmp;
	}
	gleip_line_rm	(lines->arr[--lines->size]);
}

void
gleip_lines_rm_line_back (
	struct gleip_lines	* const GLEIP_RSTR lines
	)
{
	assert	(NULL != lines);
	assert	(lines->size > 0);

	gleip_lines_rm_line(lines, lines->size - 1);
}

void
gleip_lines_rm (
	struct gleip_lines	* const GLEIP_RSTR lines
	)
{
	assert	(NULL != lines);

	if	(lines->size > 0)
	{
		gleip_lines_rm_range(lines, 0, lines->size);
	}
}

static
size_t
gleip_lines_max_size (
	const size_t	n1,
	const size_t	n2
	)
/*@*/
{
	return	n1 > n2
		? n1
		: n2;
}

static
size_t
gleip_lines_min_size (
	const size_t	n1,
	const size_t	n2
	)
/*@*/
{
	return	n1 < n2
		? n1
		: n2;
}

static
long
mod_l (
	const long	n1,
	const long	n2
	)
/*@*/
{
	long	rem;

	assert	(n2 > 0);

	rem	= n1 % n2;
	return	rem < 0
		? rem + n2
		: rem;
}

void
gleip_lines_shift (
	struct gleip_lines	* const GLEIP_RSTR lines,
	const size_t		i_to,
	const size_t		i_from,
	const size_t		count
	)
{
/*@+tmpcomments@*/
/*@t1@*/\
	const int	reverse	= i_to > i_from;
/*@=tmpcomments@*/
	const size_t	j	= gleip_lines_min_size(i_to, i_from);
/*@+tmpcomments@*/
/*@t1@*/\
	const size_t	other	= reverse
/*@=tmpcomments@*/
				? (gleip_lines_max_size(i_to, i_from)
					+ 1 - count - j)
				: (gleip_lines_max_size(i_to, i_from)
					- j);
	const size_t	sum	= count + other;
	const size_t	init	= other % sum + j;
	size_t		i;
	char		* next	= NULL;
	long		mod	= 0;
	int		restart	= 1;
/*@+tmpcomments@*/
/*@t1@*/\
	const long	revmod	= reverse
/*@=tmpcomments@*/
				? -1L
				: 1L;

	assert	(NULL != lines);
	assert	(i_to < lines->mem);
	assert	(i_from < lines->mem);
	assert	(i_to != i_from);
/*@+tmpcomments@*/
/*@t1@*/\
	assert	(!reverse
/*@=tmpcomments@*/
	||	gleip_lines_max_size(i_to, i_from) + 1 >= count + j);
	assert	(count > 0);
	assert	(other > 0);
	assert	(sum > 0);

	for	(i = 0; i < sum; ++i)
	{
		size_t	src,
			dst;
		char	* tmp	= NULL;

		src	= (size_t)mod_l((long)other
			+ (long)count * (long)i * revmod
			+ mod,
			(long)sum) + j;
/*@+tmpcomments@*/
/*@t1@*/\
		if	(!restart
/*@=tmpcomments@*/
		&&	src == init + (size_t)mod)
		{
			mod	+= 1;
			src	= (size_t)mod_l((long)other
				+ (long)count * (long)i * revmod
				+ mod,
				(long)sum) + j;
			restart	= 1;
		}
		dst	= (size_t)mod_l((long)other
			+ (long)count * ((long)i + 1L) * revmod
			+ mod,
			(long)sum) + j;

		assert	(src < lines->mem);
		assert	(dst < lines->mem);
		assert	(dst != src);

		tmp		= lines->arr[dst];
/*@+tmpcomments@*/
/*@t2@*/\
		lines->arr[dst]	= restart
/*@=tmpcomments@*/
				? lines->arr[src]
				: next;
		next		= tmp;
		restart		= 0;
	}
}

int
gleip_lines_new (
	struct gleip_lines	* const GLEIP_RSTR lines,
	const size_t		i,
	const size_t		count
	)
{
	size_t	j;

	assert	(NULL != lines);
	assert	(i <= lines->size);
	assert	(count > 0);

	if	(lines->size + count > lines->mem)
	{
/*@+tmpcomments@*/
/*@t1@*/\
		if	(!gleip_lines_mem_grow_auto
/*@=tmpcomments@*/
				(lines, lines->size + count))
		{
			return	0;
		}
	}
	assert	(lines->size + count <= lines->mem);

	for	(j = lines->size; j < lines->size + count; ++j)
	{
		if	(NULL == lines->arr[j])
		{
			size_t	k;
			for	(k = j + 1; k < lines->mem; ++k)
			{
				if	(NULL != lines->arr[k])
				{
					assert	(NULL == lines->arr[j]);
					lines->arr[j] = lines->arr[k];
					lines->arr[k] = NULL;
/*@innerbreak@*/
					break;
				}
			}
			if	(NULL == lines->arr[j])
			{
				lines->arr[j] = gleip_line_alloc();
				if	(NULL == lines->arr[j])
				{
/*@+tmpcomments@*/
/*@t1@*/\
					return	0;
/*@=tmpcomments@*/
				}
			}
			assert	(NULL != lines->arr[j]);
		}
		assert		(NULL != lines->arr[j]);
	}

#ifndef	NDEBUG
	{
		size_t	k;
		for	(k = lines->size; k < lines->size + count; ++k)
		{
			assert	(NULL != lines->arr[k]);
		}
	}
#endif	/* NDEBUG */

	if	(i != lines->size)
	{
		gleip_lines_shift(lines, i, lines->size, count);
	}
	lines->size += count;
/*@+tmpcomments@*/
/*@t1@*/\
	return	1;
/*@=tmpcomments@*/
}

int
gleip_lines_new_back (
	struct gleip_lines	* const GLEIP_RSTR lines,
	const size_t		count
	)
{
	assert	(NULL != lines);
	assert	(count > 0);

	return	gleip_lines_new(lines, lines->size, count);
}

int
gleip_lines_new_line (
	struct gleip_lines	* const GLEIP_RSTR lines,
	const size_t		i
	)
{
	assert	(NULL != lines);
	assert	(i <= lines->size);

	return	gleip_lines_new(lines, i, (size_t)1);
}

int
gleip_lines_new_line_back (
	struct gleip_lines	* const GLEIP_RSTR lines
	)
{
	assert	(NULL != lines);
	assert	(NULL != lines->arr);

	return	gleip_lines_new_line(lines, lines->size);
}

int
gleip_lines_mv_range (
	struct gleip_lines	* const GLEIP_RSTR dst,
	struct gleip_lines	* const GLEIP_RSTR src,
	const size_t		i_dst,
	const size_t		i_src,
	const size_t		count
	)
{
	size_t	i;

	assert	(NULL != dst);
	assert	(NULL != src);
	assert	(src->size > 0);
	assert	(dst != src);
	assert	(i_dst <= dst->size);
	assert	(i_src <= src->size);
	assert	(count > 0);
	assert	(count <= src->size);
	assert	(i_src + count <= src->size);

	if	(dst->mem < dst->size + count)
	{
/*@+tmpcomments@*/
/*@t1@*/\
		if (!gleip_lines_mem_grow_auto(dst, dst->size + count))
/*@=tmpcomments@*/
		{
			return	0;
		}
	}
	assert	(dst->mem >= dst->size + count);

	for	(i = 0; i < count; ++i)
	{
		const size_t	j_src			= i + i_src,
				j_dst			= i + dst->size;
		char		* GLEIP_RSTR tmp	= NULL;

		assert	(j_src < src->size);
		assert	(j_dst >= dst->size);
		assert	(j_dst < dst->mem);

		tmp	= src->arr[j_src];
		assert	(NULL != tmp);

		src->arr[j_src]	= dst->arr[j_dst];
		dst->arr[j_dst]	= tmp;
	}

	if	(dst->size > 0
	&&	i_dst != dst->size)
	{
		gleip_lines_shift(dst, i_dst, dst->size, count);
	}
	dst->size += count;

	for	(i = i_src + count; i < src->size; ++i)
	{
		const size_t	j			= i - count;
		char		* GLEIP_RSTR tmp	= NULL;

		assert	(i >= count);
		assert	(j < src->size);

		tmp	= src->arr[i];
		assert	(NULL != tmp);

		src->arr[i]	= src->arr[j];
		src->arr[j]	= tmp;
	}
	src->size -= count;

	return	1;
}

int
gleip_lines_mv_range_back (
	struct gleip_lines	* const GLEIP_RSTR dst,
	struct gleip_lines	* const GLEIP_RSTR src,
	const size_t		i_src,
	const size_t		count
	)
{
	assert	(NULL != dst);
	assert	(NULL != src);
	assert	(src->size > 0);
	assert	(dst != src);
	assert	(i_src <= src->size);
	assert	(count > 0);
	assert	(count <= src->size);
	assert	(i_src + count <= src->size);

	return	gleip_lines_mv_range(dst, src, dst->size, i_src, count);
}

int
gleip_lines_mv (
	struct gleip_lines	* const GLEIP_RSTR dst,
	struct gleip_lines	* const GLEIP_RSTR src,
	const size_t		i_dst
	)
{
	assert	(NULL != dst);
	assert	(NULL != src);
	assert	(src->size > 0);
	assert	(dst != src);
	assert	(i_dst <= dst->size);

	return	gleip_lines_mv_range(dst, src, i_dst, 0, src->size);
}

int
gleip_lines_mv_back (
	struct gleip_lines	* const GLEIP_RSTR dst,
	struct gleip_lines	* const GLEIP_RSTR src
	)
{
	assert	(NULL != dst);
	assert	(NULL != src);
	assert	(src->size > 0);
	assert	(dst != src);

	return	gleip_lines_mv(dst, src, dst->size);
}

int
gleip_lines_mv_line (
	struct gleip_lines	* const GLEIP_RSTR dst,
	struct gleip_lines	* const GLEIP_RSTR src,
	const size_t		i_dst,
	const size_t		i_src
	)
{
	assert	(NULL != dst);
	assert	(NULL != src);
	assert	(src->size > 0);
	assert	(dst != src);
	assert	(i_dst <= dst->size);
	assert	(i_src <= src->size);

	return	gleip_lines_mv_range(dst, src, i_dst, i_src, (size_t)1);
}

int
gleip_lines_mv_line_back (
	struct gleip_lines	* const GLEIP_RSTR dst,
	struct gleip_lines	* const GLEIP_RSTR src,
	const size_t		i_src
	)
{
	assert	(NULL != dst);
	assert	(NULL != src);
	assert	(src->size > 0);
	assert	(dst != src);
	assert	(i_src <= src->size);

	return	gleip_lines_mv_line(dst, src, dst->size, i_src);
}

static
void
gleip_lines_rm_range_mem (
/*@in@*/
/*@notnull@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
	const size_t		i_end
	)
/*@modifies * lines->arr@*/
{
	size_t	i;

	assert	(NULL != lines);
	assert	(NULL != lines->arr);
	assert	(i_end < lines->mem);

	for	(i = lines->size; i < i_end; ++i)
	{
		assert		(NULL != lines->arr[i]);
		gleip_line_rm	(lines->arr[i]);
	}
}

int
gleip_lines_cpy_range (
	struct gleip_lines		* const GLEIP_RSTR dst,
	const struct gleip_lines	* const GLEIP_RSTR src,
	const size_t			i_dst,
	const size_t			i_src,
	const size_t			count
	)
{
	size_t	i;

	assert	(NULL != dst);
	assert	(NULL != src);
	assert	(src->size > 0);
	assert	(dst != src);
	assert	(i_dst <= dst->size);
	assert	(i_src <= src->size);
	assert	(count > 0);
	assert	(count <= src->size);
	assert	(i_src + count <= src->size);

	if	(dst->mem < dst->size + count)
	{
/*@+tmpcomments@*/
/*@t1@*/\
		if (!gleip_lines_mem_grow_auto(dst, dst->size + count))
/*@=tmpcomments@*/
		{
			return	0;
		}
	}
	assert	(dst->mem >= dst->size + count);

	for	(i = 0; i < count; ++i)
	{
		const size_t	j_src			= i + i_src,
				j_dst			= i + dst->size;
		size_t		j_pref;
		char		* GLEIP_RSTR line_src	= NULL;

		assert	(j_src < src->size);
		assert	(j_dst >= dst->size);
		assert	(j_dst < dst->mem);

		line_src = src->arr[j_src];
		assert	(NULL != line_src);

		j_pref	= gleip_lines_mem_best_pref_len
			(dst, j_dst, strlen(line_src));
		assert	(j_pref >= j_dst);
		assert	(j_pref < dst->mem);

		if	(j_pref != j_dst)
		{
			char * const GLEIP_RSTR tmp = dst->arr[j_pref];
			dst->arr[j_pref]	= dst->arr[j_dst];
			dst->arr[j_dst]		= tmp;
		}

		if	(NULL == dst->arr[j_dst])
		{
			dst->arr[j_dst]	= gleip_line_alloc_cpy
					(line_src);
			if	(NULL == dst->arr[j_dst])
			{
/*@+tmpcomments@*/
/*@t1@*/\
				gleip_lines_rm_range_mem(dst, j_dst);
/*@t1@*/\
				return	0;
/*@=tmpcomments@*/
			}
		}
		else
		{
/*@+tmpcomments@*/
/*@t1@*/\
			if	(!gleip_line_cpy_b
/*@=tmpcomments@*/
					(& dst->arr[j_dst], line_src))
			{
				gleip_lines_rm_range_mem(dst, j_dst);
				return	0;
			}
		}
	}

	if	(dst->size > 0
	&&	i_dst != dst->size)
	{
		gleip_lines_shift(dst, i_dst, dst->size, count);
	}
	dst->size += count;

	return	1;
}

int
gleip_lines_cpy_range_back (
	struct gleip_lines		* const GLEIP_RSTR dst,
	const struct gleip_lines	* const GLEIP_RSTR src,
	const size_t			i_src,
	const size_t			count
	)
{
	assert	(NULL != dst);
	assert	(NULL != src);
	assert	(src->size > 0);
	assert	(dst != src);
	assert	(i_src <= src->size);
	assert	(count > 0);
	assert	(count <= src->size);
	assert	(i_src + count <= src->size);

	return gleip_lines_cpy_range(dst, src, dst->size, i_src, count);
}

int
gleip_lines_cpy (
	struct gleip_lines		* const GLEIP_RSTR dst,
	const struct gleip_lines	* const GLEIP_RSTR src,
	const size_t			i_dst
	)
{
	assert	(NULL != dst);
	assert	(NULL != src);
	assert	(src->size > 0);
	assert	(dst != src);
	assert	(i_dst <= dst->size);

	return	gleip_lines_cpy_range(dst, src, i_dst, 0, src->size);
}

int
gleip_lines_cpy_back (
	struct gleip_lines		* const GLEIP_RSTR dst,
	const struct gleip_lines	* const GLEIP_RSTR src
	)
{
	assert	(NULL != dst);
	assert	(NULL != src);
	assert	(src->size > 0);
	assert	(dst != src);

	return	gleip_lines_cpy(dst, src, dst->size);
}

int
gleip_lines_cpy_line (
	struct gleip_lines		* const GLEIP_RSTR dst,
	const struct gleip_lines	* const GLEIP_RSTR src,
	const size_t			i_dst,
	const size_t			i_src
	)
{
	assert	(NULL != dst);
	assert	(NULL != src);
	assert	(src->size > 0);
	assert	(dst != src);
	assert	(i_dst <= dst->size);
	assert	(i_src <= src->size);

	return gleip_lines_cpy_range(dst, src, i_dst, i_src, (size_t)1);
}

int
gleip_lines_cpy_line_back (
	struct gleip_lines		* const GLEIP_RSTR dst,
	const struct gleip_lines	* const GLEIP_RSTR src,
	const size_t			i_src
	)
{
	assert	(NULL != dst);
	assert	(NULL != src);
	assert	(src->size > 0);
	assert	(dst != src);
	assert	(i_src <= src->size);

	return	gleip_lines_cpy_line(dst, src, dst->size, i_src);
}

void
gleip_lines_free (
	struct gleip_lines	* const GLEIP_RSTR lines
	)
{
	size_t	i;

	assert	(NULL != lines);
	assert	(NULL != lines->arr);

	for	(i = 0; i < lines->mem; ++i)
	{
		char	* const GLEIP_RSTR line	= lines->arr[i];

		assert	(NULL != line
		||	i >= lines->size);

		if	(NULL != line)
		{
			free	(line);
		}
	}

	free	(lines->arr);
	free	(lines);
}

struct gleip_lines *
gleip_lines_alloc_mem (
	const size_t	mem
	)
{
	size_t			i;
	struct gleip_lines	* GLEIP_RSTR lines	= NULL;

	assert	(mem > 0);

	lines	= malloc(sizeof(* lines));
	if	(NULL == lines)
	{
		return	NULL;
	}

	lines->size	= 0;
	lines->mem	= mem;
	lines->arr	= malloc(sizeof(* lines->arr)
			* lines->mem);
	if	(NULL == lines->arr)
	{
		free	(lines);
		return	NULL;
	}

	for	(i = 0; i < lines->mem; ++i)
	{
		lines->arr[i]	= NULL;
	}

/*@+tmpcomments@*/
/*@t1@*/\
	return	lines;
/*@=tmpcomments@*/
}

struct gleip_lines *
gleip_lines_alloc (void)
{
	assert	(GLEIP_LINES_MEM_DEF > 0);
	return	gleip_lines_alloc_mem(GLEIP_LINES_MEM_DEF);
}

struct gleip_lines *
gleip_lines_alloc_mv_range (
	struct gleip_lines	* const GLEIP_RSTR src,
	const size_t		i_src,
	const size_t		count
	)
{
	struct gleip_lines	* GLEIP_RSTR dst	= NULL;

	assert	(NULL != src);
	assert	(src->size > 0);
	assert	(i_src <= src->size);
	assert	(count > 0);
	assert	(count <= src->size);
	assert	(i_src + count <= src->size);

	dst	= gleip_lines_alloc_mem(src->size);
	if	(NULL == dst)
	{
		return	NULL;
	}

/*@+tmpcomments@*/
/*@t1@*/\
	if	(gleip_lines_mv_range(dst, src, 0, i_src, count))
/*@=tmpcomments@*/
	{
		return	dst;
	}
	else
	{
		gleip_lines_free(dst);
		return	NULL;
	}
}

struct gleip_lines *
gleip_lines_alloc_mv_line (
	struct gleip_lines	* const GLEIP_RSTR src,
	const size_t		i_src
	)
{
	assert	(NULL != src);
	assert	(src->size > 0);
	assert	(i_src <= src->size);

	return	gleip_lines_alloc_mv_range(src, i_src, (size_t)1);
}

struct gleip_lines *
gleip_lines_alloc_cpy_range (
	const struct gleip_lines	* const GLEIP_RSTR src,
	const size_t			i_src,
	const size_t			count
	)
{
	struct gleip_lines	* GLEIP_RSTR dst	= NULL;

	assert	(NULL != src);
	assert	(src->size > 0);
	assert	(i_src <= src->size);
	assert	(count > 0);
	assert	(count <= src->size);
	assert	(i_src + count <= src->size);

	dst	= gleip_lines_alloc_mem(src->size);
	if	(NULL == dst)
	{
		return	NULL;
	}

/*@+tmpcomments@*/
/*@t1@*/\
	if	(gleip_lines_cpy_range(dst, src, 0, i_src, count))
/*@=tmpcomments@*/
	{
		return	dst;
	}
	else
	{
		gleip_lines_free(dst);
		return	NULL;
	}
}

struct gleip_lines *
gleip_lines_alloc_cpy (
	const struct gleip_lines	* const GLEIP_RSTR src
	)
{
	assert	(NULL != src);
	assert	(src->size > 0);

	return	gleip_lines_alloc_cpy_range(src, 0, src->size);
}

struct gleip_lines *
gleip_lines_alloc_cpy_line (
	const struct gleip_lines	* const GLEIP_RSTR src,
	const size_t			i_src
	)
{
	assert	(NULL != src);
	assert	(src->size > 0);
	assert	(i_src <= src->size);

	return	gleip_lines_alloc_cpy_range(src, i_src, (size_t)1);
}

