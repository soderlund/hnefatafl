/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef	GLEIP_LINES_H
#define	GLEIP_LINES_H

#include <stddef.h>	/* size_t */

#if	defined(__STDC_VERSION__) && __STDC_VERSION__ >= 199901L
#define GLEIP_RSTR restrict
#else
#define	GLEIP_RSTR
#endif

/*@unchecked@*/
/*@unused@*/
extern
const long	GLEIP_LINES_VERSION;

struct gleip_lines
{

	size_t	size,
		mem;

/*@in@*/
/*@notnull@*/
/*@owned@*/
	char	* * arr;

};

/*@in@*/
/*@notnull@*/
/*@unused@*/
extern
const char *
gleip_lines_get_ro (
/*@in@*/
/*@notnull@*/
/*@returned@*/
	const struct gleip_lines	* const GLEIP_RSTR,
	const size_t
	)
/*@*/
;

/*@in@*/
/*@notnull@*/
/*@unused@*/
extern
char *
gleip_lines_get (
/*@in@*/
/*@notnull@*/
/*@returned@*/
	struct gleip_lines	* const GLEIP_RSTR,
	const size_t
	)
/*@*/
;

/*@in@*/
/*@notnull@*/
/*@unused@*/
extern
char * *
gleip_lines_get_ptr (
/*@in@*/
/*@notnull@*/
/*@returned@*/
	struct gleip_lines	* const GLEIP_RSTR,
	const size_t
	)
/*@*/
;

/*@unused@*/
extern
size_t
gleip_lines_mem (
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR
	)
/*@*/
;

/*@unused@*/
extern
size_t
gleip_lines_size (
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR
	)
/*@*/
;

/*@unused@*/
extern
int
gleip_lines_empty (
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR
	)
/*@*/
;

/*@unused@*/
extern
int
gleip_lines_mem_trim (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines
/*@=protoparamname@*/
	)
/*@modifies * lines->arr, lines->arr, lines->mem@*/
;

/*@unused@*/
extern
int
gleip_lines_mem_grow (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
/*@=protoparamname@*/
	const size_t
	)
/*@modifies * lines->arr, lines->arr, lines->mem@*/
;

/*@unused@*/
extern
int
gleip_lines_addcpy (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
	const char		* const GLEIP_RSTR,
	const size_t
	)
/*@modifies * lines->arr, lines->arr, lines->size, lines->mem@*/
;

/*@unused@*/
extern
int
gleip_lines_addcpy_back (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
	const char		* const GLEIP_RSTR
	)
/*@modifies * lines->arr, lines->arr, lines->size, lines->mem@*/
;

/*@unused@*/
extern
int
gleip_lines_addref (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
/*@=protoparamname@*/
/*@in@*/
/*@keep@*/
/*@notnull@*/
	char			* const GLEIP_RSTR,
	const size_t
	)
/*@modifies * lines->arr, lines->arr, lines->size, lines->mem@*/
;

/*@unused@*/
extern
int
gleip_lines_addref_back (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
/*@=protoparamname@*/
/*@in@*/
/*@keep@*/
/*@notnull@*/
	char			* const GLEIP_RSTR
	)
/*@modifies * lines->arr, lines->arr, lines->size, lines->mem@*/
;

/*@unused@*/
extern
void
gleip_lines_rm_range (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
/*@=protoparamname@*/
	size_t,
	const size_t
	)
/*@modifies * lines->arr, lines->size@*/
;

/*@unused@*/
extern
void
gleip_lines_rm_range_back (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
/*@=protoparamname@*/
	const size_t
	)
/*@modifies * lines->arr, lines->size@*/
;

/*@unused@*/
extern
void
gleip_lines_rm_line (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
/*@=protoparamname@*/
	const size_t
	)
/*@modifies * lines->arr, lines->size@*/
;

/*@unused@*/
extern
void
gleip_lines_rm_line_back (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines
/*@=protoparamname@*/
	)
/*@modifies * lines->arr, lines->size@*/
;

/*@unused@*/
extern
void
gleip_lines_rm (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines
/*@=protoparamname@*/
	)
/*@modifies * lines->arr, lines->size@*/
;

/*@unused@*/
extern
void
gleip_lines_shift (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
/*@=protoparamname@*/
	const size_t,
	const size_t,
	const size_t
	)
/*@modifies * lines->arr@*/
;

/*@unused@*/
extern
int
gleip_lines_new (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
/*@=protoparamname@*/
	const size_t,
	const size_t
	)
/*@modifies * lines->arr, lines->arr, lines->size, lines->mem@*/
;

/*@unused@*/
extern
int
gleip_lines_new_back (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
/*@=protoparamname@*/
	const size_t
	)
/*@modifies * lines->arr, lines->arr, lines->size, lines->mem@*/
;

/*@unused@*/
extern
int
gleip_lines_new_line (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines,
/*@=protoparamname@*/
	const size_t
	)
/*@modifies * lines->arr, lines->arr, lines->size, lines->mem@*/
;

/*@unused@*/
extern
int
gleip_lines_new_line_back (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines
/*@=protoparamname@*/
	)
/*@modifies * lines->arr, lines->arr, lines->size, lines->mem@*/
;

/*@unused@*/
extern
int
gleip_lines_mv_range (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR dst,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR src,
/*@=protoparamname@*/
	const size_t,
	const size_t,
	const size_t
	)
/*@modifies * dst->arr, dst->arr, dst->size, dst->mem@*/
/*@modifies * src->arr, src->arr, src->size@*/
;

/*@unused@*/
extern
int
gleip_lines_mv_range_back (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR dst,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR src,
/*@=protoparamname@*/
	const size_t,
	const size_t
	)
/*@modifies * dst->arr, dst->arr, dst->size, dst->mem@*/
/*@modifies * src->arr, src->arr, src->size@*/
;

/*@unused@*/
extern
int
gleip_lines_mv (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR dst,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR src,
/*@=protoparamname@*/
	const size_t
	)
/*@modifies * dst->arr, dst->arr, dst->size, dst->mem@*/
/*@modifies * src->arr, src->arr, src->size@*/
;

/*@unused@*/
extern
int
gleip_lines_mv_back (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR dst,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR src
/*@=protoparamname@*/
	)
/*@modifies * dst->arr, dst->arr, dst->size, dst->mem@*/
/*@modifies * src->arr, src->arr, src->size@*/
;

/*@unused@*/
extern
int
gleip_lines_mv_line (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR dst,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR src,
/*@=protoparamname@*/
	const size_t,
	const size_t
	)
/*@modifies * dst->arr, dst->arr, dst->size, dst->mem@*/
/*@modifies * src->arr, src->arr, src->size@*/
;

/*@unused@*/
extern
int
gleip_lines_mv_line_back (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR dst,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR src,
/*@=protoparamname@*/
	const size_t
	)
/*@modifies * dst->arr, dst->arr, dst->size, dst->mem@*/
/*@modifies * src->arr, src->arr, src->size@*/
;

/*@unused@*/
extern
int
gleip_lines_cpy_range (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines		* const GLEIP_RSTR dst,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR,
	const size_t,
	const size_t,
	const size_t
	)
/*@modifies * dst->arr, dst->arr, dst->size, dst->mem@*/
;

/*@unused@*/
extern
int
gleip_lines_cpy_range_back (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines		* const GLEIP_RSTR dst,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR,
	const size_t,
	const size_t
	)
/*@modifies * dst->arr, dst->arr, dst->size, dst->mem@*/
;

/*@unused@*/
extern
int
gleip_lines_cpy (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines		* const GLEIP_RSTR dst,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR,
	const size_t
	)
/*@modifies * dst->arr, dst->arr, dst->size, dst->mem@*/
;

/*@unused@*/
extern
int
gleip_lines_cpy_back (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines		* const GLEIP_RSTR dst,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR
	)
/*@modifies * dst->arr, dst->arr, dst->size, dst->mem@*/
;

/*@unused@*/
extern
int
gleip_lines_cpy_line (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines		* const GLEIP_RSTR dst,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR,
	const size_t,
	const size_t
	)
/*@modifies * dst->arr, dst->arr, dst->size, dst->mem@*/
;

/*@unused@*/
extern
int
gleip_lines_cpy_line_back (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines		* const GLEIP_RSTR dst,
/*@=protoparamname@*/
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR,
	const size_t
	)
/*@modifies * dst->arr, dst->arr, dst->size, dst->mem@*/
;

/*@unused@*/
extern
void
gleip_lines_free (
/*@in@*/
/*@notnull@*/
/*@only@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR lines
/*@=protoparamname@*/
	)
/*@modifies lines->arr, lines@*/
/*@releases lines->arr, lines@*/
;

/*@in@*/
/*@null@*/
/*@only@*/
/*@unused@*/
extern
struct gleip_lines *
gleip_lines_alloc_mem (
	const size_t
	)
/*@*/
;

/*@in@*/
/*@null@*/
/*@only@*/
/*@unused@*/
extern
struct gleip_lines *
gleip_lines_alloc (void)
/*@*/
;

/*@in@*/
/*@null@*/
/*@only@*/
/*@unused@*/
extern
struct gleip_lines *
gleip_lines_alloc_mv_range (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR src,
/*@=protoparamname@*/
	const size_t,
	const size_t
	)
/*@modifies * src->arr, src->arr, src->size@*/
;

/*@in@*/
/*@null@*/
/*@only@*/
/*@unused@*/
extern
struct gleip_lines *
gleip_lines_alloc_mv_line (
/*@in@*/
/*@notnull@*/
/*@-protoparamname@*/
	struct gleip_lines	* const GLEIP_RSTR src,
/*@=protoparamname@*/
	const size_t
	)
/*@modifies * src->arr, src->arr, src->size@*/
;

/*@in@*/
/*@null@*/
/*@only@*/
/*@unused@*/
extern
struct gleip_lines *
gleip_lines_alloc_cpy_range (
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR,
	const size_t,
	const size_t
	)
/*@*/
;

/*@in@*/
/*@null@*/
/*@only@*/
/*@unused@*/
extern
struct gleip_lines *
gleip_lines_alloc_cpy (
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR
	)
/*@*/
;

/*@in@*/
/*@null@*/
/*@only@*/
/*@unused@*/
extern
struct gleip_lines *
gleip_lines_alloc_cpy_line (
/*@in@*/
/*@notnull@*/
	const struct gleip_lines	* const GLEIP_RSTR,
	const size_t
	)
/*@*/
;

#endif

