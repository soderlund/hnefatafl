/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifdef	HNEFATAFL_UI_XLIB

#ifndef HNEF_UI_UIXCONSOLET_H
#define HNEF_UI_UIXCONSOLET_H

#include "hnefatafl.h"

#include "gleipnir_lines.h"	/* gleip_lines */

/*
 * `input` is read from the keyboard in X as the user presses keys.
 *
 * `input_echo` is `input` echoed to the user with some extra chars.
 *
 * `input_tokens` is created from `input` when the user hits <Enter> to
 * execute the command. Then it can be handled like in `cli.c` when
 * reading to a `hnef_lines` struct from `stdin` (the only difference
 * is really that `stdin` isn't available in X so we have to read from
 * `KeyPress` XEvents instead).
 */
struct hnef_uix_console
{

	/*
	 * `NULM`-terminated.
	 *
	 * `*_last` is for knowing if the consoles should be repainted
	 * or not depending on if the text in them has changed since the
	 * last repaint (see the console painting functions in
	 * `uixpaint.c`).
	 */
/*@in@*/
/*@notnull@*/
/*@owned@*/
	char			* input,
				* input_echo,
				* output,
				* input_last,
				* output_last;

/*@in@*/
/*@notnull@*/
/*@owned@*/
	struct gleip_lines	* input_tokens;

	/*
	 * `output_error` is true if `output` is an error message.
	 *
	 * `input_computer_turn_last` is for remembering if the last
	 * input console repaint was done during the computer player's
	 * turn (in which case it's painted differently), so we can know
	 * if the console has changed. `output_error_last` works the
	 * same way but for output error comparison.
	 */
	HNEF_BOOL		output_error,
				output_error_last,
				input_computer_turn_last;

};

#endif

#endif	/* HNEFATAFL_UI_XLIB */

