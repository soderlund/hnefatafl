/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifdef	HNEFATAFL_UI_XLIB

#ifndef HNEF_UI_UIXTHEME_H
#define HNEF_UI_UIXTHEME_H

#include "funcx.h"	/* HNEF_FRX */
#include "uit.h"	/* hnef_ui */
#include "uixthemet.h"	/* hnef_uix_theme* */
#include "uixt.h"	/* hnef_uix */

/*@in@*/
/*@null@*/
extern
struct hnef_uix_theme *
hnef_uix_theme_cur (
/*@in@*/
/*@notnull@*/
/*@returned@*/
	struct hnef_uix_themes	* const HNEF_RSTR
	)
/*@modifies nothing@*/
;

/*@in@*/
/*@null@*/
extern
struct hnef_uix_img *
hnef_uix_img_get (
/*@in@*/
/*@notnull@*/
/*@returned@*/
	struct hnef_uix_theme	* const HNEF_RSTR,
	const HNEF_BIT_U8,
	const HNEF_BOOL
	)
/*@modifies nothing@*/
;

/*@in@*/
/*@null@*/
/*@only@*/
extern
struct hnef_uix_themes *
hnef_alloc_uix_themes (void)
/*@modifies nothing@*/
;

/*@-protoparamname@*/
extern
void
hnef_free_uix_themes_fini (
/*@partial@*/
/*@notnull@*/
	const struct hnef_uix	* const HNEF_RSTR uix,
/*@in@*/
/*@notnull@*/
/*@owned@*/
/*@special@*/
	struct hnef_uix_themes	* const HNEF_RSTR themes
	)
/*@modifies themes@*/
/*@releases themes@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_uix_themes_init_file (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@out@*/
/*@notnull@*/
	HNEF_BOOL	* const HNEF_RSTR success
	)
/*@globals errno, fileSystem@*/
/*@modifies errno, fileSystem, * ui, * success@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_uix_themes_load_rules (
/*@in@*/
/*@notnull@*/
	struct hnef_uix		* const HNEF_RSTR uix,
/*@in@*/
/*@notnull@*/
	struct hnef_game	* const HNEF_RSTR game,
/*@in@*/
/*@null@*/
	const char		* const HNEF_RSTR ui_theme,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX		* const HNEF_RSTR frx,
/*@out@*/
/*@notnull@*/
	HNEF_BOOL		* const HNEF_RSTR success
	)
/*@modifies * uix, * game, * frx, * success@*/
;
/*@=protoparamname@*/

#endif

#endif	/* HNEFATAFL_UI_XLIB */

