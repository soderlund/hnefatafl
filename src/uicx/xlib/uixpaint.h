/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifdef	HNEFATAFL_UI_XLIB

#ifndef HNEF_UI_UIXPAINT_H
#define HNEF_UI_UIXPAINT_H

#include "funcxt.h"	/* HNEF_FRX */
#include "uit.h"	/* hnef_ui */

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_uix_paint_board (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX	* const HNEF_RSTR frx,
	const HNEF_BOOL	force_repaint
	)
/*@modifies * ui, * frx@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
/*@unused@*/		/* Unused without AI */
extern
enum HNEF_FR
hnef_uix_paint_progress (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX	* const HNEF_RSTR frx,
	const HNEF_BOOL	force_repaint
	)
/*@modifies * ui, * frx@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_uix_paint_console_in (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX	* const HNEF_RSTR frx,
	const HNEF_BOOL	force_repaint
	)
/*@modifies * ui, * frx@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_uix_paint_console_out (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX	* const HNEF_RSTR frx,
	const HNEF_BOOL	force_repaint
	)
/*@modifies * ui, * frx@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_uix_paint (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX	* const HNEF_RSTR frx,
	const HNEF_BOOL	force_repaint
	)
/*@modifies * ui, * frx@*/
;
/*@=protoparamname@*/

#endif

#endif	/* HNEFATAFL_UI_XLIB */

