/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifdef	HNEFATAFL_UI_XLIB

#include <assert.h>	/* assert */
#include <string.h>	/* strcmp */

#include "gleipnir_line.h"	/* gleip_line_* */

#include "cliiot.h"	/* HNEF_UI_PROMPT */
#include "uiutil.h"	/* hnef_control_computer */
#include "uixfont.h"	/* hnef_uix_font_* */
#include "uixpaint.h"
#include "uixtheme.h"	/* hnef_uix_img_get */
#include "uixutil.h"	/* hnef_uix_* */
#include "uixutilt.h"	/* HNEF_UIX_FLAG_UNIT_* */

/*
 * Layout:
 *
 *	[Board]			Height: hnef_uix_board_h()
 *	[Progress bar]		Height: uix->progress_height
 *	[Console input]		Height: uix->font ascent + descent
 *	[Console output]	Height: uix->font ascent + descent
 *
 * The [Board] part is replaced by a placeholder graphic if there is no
 * ongoing game (so `hnef_uix_paint_board()` can be called even if the
 * game is invalid).
 *
 * Painting
 * --------
 * The painting functions all have `force_repaint` which causes them to
 * do a full repaint. If this is not indicated, then repaints will only
 * happen if the program determines that whatever is to be repainted has
 * changed since the last call. You can indicate that it has changed by
 * manipulating certain values to force a repaint on the next call (or
 * by calling the paint functions immediately with the `force_repaint`
 * parameter). This makes the code a bit more messy and error-prone (we
 * may miss a repaint if we're not careful in indicating that a repaint
 * must be done) since we need extra variables to keep track of this.
 *
 * In the console painting functions there are "last string" variables,
 * which have to be filled in every time the functions are called, but
 * this should still be faster than repainting the consoles on every
 * call because paint operations are usually very slow compared to
 * `memcpy` and the other functions we use to copy and clear strings.
 */

/*@observer@*/
/*@unchecked@*/
static
const char	* const HNEF_UIX_CARET	= "_";

/*
 * `typestruct` is a `struct hnef_type_square` if `square`; else it's a
 * `struct hnef_type_piece`.
 */
static
enum HNEF_FR
hnef_uix_paint_img (
/*@in@*/
/*@notnull@*/
	struct hnef_ui		* const HNEF_RSTR ui,
	const HNEF_BIT_U8	ui_bit,
	const HNEF_BOOL		square,
	const unsigned short	x,
	const unsigned short	y,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX		* const HNEF_RSTR frx,
/*@in@*/
/*@notnull@*/
	void			* const typestruct
	)
/*@modifies * ui, * frx@*/
{
	struct hnef_uix		* HNEF_RSTR uix		= NULL;
	struct hnef_uix_theme	* HNEF_RSTR theme	= NULL;
	struct hnef_uix_img	* HNEF_RSTR img		= NULL;
	size_t			i;
	unsigned short		pos;
	int			px,	/* Image x */
				py,	/* Image y */
				psqx,	/* Square x */
				psqy,	/* Square y */
				sq_w,
				sq_h,
				img_offset_x,
				img_offset_y;

	assert	(HNEF_BIT_U8_EMPTY != ui_bit);
	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->game->board);	/* Implies valid */
	assert	(x < ui->game->rules->bwidth);
	assert	(y < ui->game->rules->bheight);

	uix	= ui->uix;

	assert	(NULL != uix);
	assert	(NULL != uix->display);
	assert	(NULL != frx);
	assert	(NULL != uix->paintmap);
	assert	(uix->paintmap_width == ui->game->rules->bwidth);
	assert	(uix->paintmap_height == ui->game->rules->bheight);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

	/*
	 * Both `theme` and `img` may be `NULL`.
	 */
	theme	= hnef_uix_theme_cur(uix->themes);
	img	= theme == NULL
		? NULL
		: hnef_uix_img_get(theme, ui_bit, square);

	pos		= (unsigned short)
			(y * ui->game->rules->bwidth + x);
	sq_w		= hnef_uix_square_size_w(ui->game, uix->themes);
	sq_h		= hnef_uix_square_size_h(ui->game, uix->themes);
	img_offset_x	= NULL == img
			? 0
			: (sq_w - img->image->width) / 2;
	img_offset_y	= NULL == img
			? 0
			: (sq_h - img->image->height) / 2;
	px		= x * sq_w
			+ uix->opt_offset_x
			+ img_offset_x;
	py		= y * sq_h
			+ uix->opt_offset_y
			+ img_offset_y;
	psqx		= px - img_offset_x;
	psqy		= py - img_offset_y;

	if	(square)
	{
		/*
		 * Clear background before painting square.
		 */
/*@i@*/		XClearArea(uix->display, uix->window,
			px - img_offset_x,
			py - img_offset_y,
			(unsigned int)sq_w,
			(unsigned int)sq_h,
			False);
	}

	if	(NULL == img
	||	img->image->width < 1
	||	img->image->height < 1)
	{
		/*
		 * Placeholder graphics.
		 */
		if	(square)
		{
			struct hnef_type_square * const ts =
				(struct hnef_type_square *)typestruct;
/*@i@*/			XSetForeground(uix->display, uix->gc,
				uix->col_flag_blue.pixel);
/*@i@*/			XFillRectangle(uix->display, uix->window,
				uix->gc,
				psqx,
				psqy,
				(unsigned int)sq_w,
				(unsigned int)sq_h);
/*@i@*/			XSetForeground(uix->display, uix->gc,
				uix->px_black);
/*@i@*/			XDrawRectangle(uix->display, uix->window,
				uix->gc,
				psqx,
				psqy,
				(unsigned int)sq_w - 1,
				(unsigned int)sq_h - 1);
			if	(ts->escape)
			{
/*@i@*/				XDrawLine(uix->display, uix->window,
					uix->gc,
					psqx,		psqy,
					psqx + sq_w,	psqy + sq_h);
/*@i@*/				XDrawLine(uix->display, uix->window,
					uix->gc,
					psqx + sq_w,	psqy,
					psqx,		psqy + sq_h);
			}
/*@i@*/			XSetForeground(uix->display, uix->gc,
				uix->px_white);
		}
		else
		{
			char		pstr[4];
			int		textw,
					texth;
			const int	degmod = 64; /* XLib standard */
			struct hnef_type_piece * const tp =
				(struct hnef_type_piece *)typestruct;

			memset	(pstr, (int)'\0',
				sizeof(pstr) / sizeof(* pstr));

			if	((unsigned short)1 == tp->owner)
			{
/*@i@*/				XSetForeground(uix->display, uix->gc,
					uix->px_black);
			}
/*@i@*/			XFillArc(uix->display, uix->window, uix->gc,
				psqx,
				psqy,
				(unsigned int)sq_w,
				(unsigned int)sq_h,
				degmod * 0,
				degmod * 360);

			assert	(ui_bit <= (HNEF_BIT_U8)128);
/* SPLint snprintf is not in C89 */ /*@i@*/\
			if	(sprintf(pstr, "%hu",
					(unsigned short)ui_bit) < 0)
			{
				return	HNEF_FR_FAIL_IO_WRITE;
			}

			textw = hnef_uix_font_textwidth(uix, pstr);
			texth = uix->font->ascent + uix->font->descent;

			if	(textw <= sq_w
			&&	texth <= sq_h)
			{
				enum HNEF_FR fr = HNEF_FR_SUCCESS;
/*@i@*/				XSetForeground(uix->display, uix->gc,
					(unsigned short)1 == tp->owner
					? uix->px_white
					: uix->px_black);
				fr = hnef_uix_font_print(uix, pstr,
					psqx + (sq_w - textw) / 2,
					psqy + sq_h -
					(sq_h - uix->font->ascent) / 2,
						frx);
				if	(!hnef_fr_good(fr)
				||	!hnef_frx_good(* frx))
				{
					return	fr;
				}
			}
/*@i@*/			XSetForeground(uix->display, uix->gc,
					uix->px_white);
		}
	}
	else
	{
		assert	(NULL != img->image);

		if	(img->transparent)
		{
/*@i@*/			XSetClipMask	(uix->display, uix->gc,
					img->clipmask);
/*@i@*/			XSetClipOrigin	(uix->display, uix->gc, px, py);
		}

/*@i@*/		XPutImage(uix->display, uix->window, uix->gc,
			img->image, 0, 0,
			px,
			py,
			(unsigned int)img->image->width,
			(unsigned int)img->image->height);

		if	(img->transparent)
		{
/*@i@*/			XSetClipMask(uix->display, uix->gc, None);
		}
	}

	if	(HNEF_BOARDPOS_NONE != uix->pos_select
	&&	pos == uix->pos_select)
	{
/*@i@*/		XDrawRectangle(uix->display, uix->window, uix->gc,
			px - img_offset_x,
			py - img_offset_y,
			(unsigned int)(sq_w - 1),
			(unsigned int)(sq_h - 1));
	}

/*@i@*/	XSetForeground(uix->display, uix->gc, uix->col_red.pixel);
	for	(i = 0; i < ui->opt_movelist->elemc; ++i)
	{
		struct hnef_move	* const HNEF_RSTR move =
					& ui->opt_movelist->elems[i];
		const int	prop	= 6;
		const double	xlen	= (sq_w - 1.0) / (double)prop,
				ylen	= (sq_h - 1.0) / (double)prop;
		assert	(prop > 1);
		if	(move->pos == pos
		||	move->dest == pos)
		{
/*@i@*/			XDrawLine(uix->display, uix->window, uix->gc,
				psqx,
				psqy,
				(int)(psqx + xlen),
				psqy);
/*@i@*/			XDrawLine(uix->display, uix->window, uix->gc,
				psqx,
				psqy,
				psqx,
				(int)(psqy + ylen));

/*@i@*/			XDrawLine(uix->display, uix->window, uix->gc,
				psqx,
				psqy + (sq_h - 1),
				psqx,
				(int)(psqy + ylen * (prop - 1.0)));
/*@i@*/			XDrawLine(uix->display, uix->window, uix->gc,
				psqx,
				psqy + (sq_h - 1),
				(int)(psqx + xlen),
				psqy + (sq_h - 1));

/*@i@*/			XDrawLine(uix->display, uix->window, uix->gc,
				psqx + (sq_w - 1),
				psqy,
				psqx + (sq_w - 1),
				(int)(psqy + ylen));
/*@i@*/			XDrawLine(uix->display, uix->window, uix->gc,
				psqx + (sq_w - 1),
				psqy,
				(int)(psqx + xlen * (prop - 1.0)),
				psqy);

/*@i@*/			XDrawLine(uix->display, uix->window, uix->gc,
				psqx + (sq_w - 1),
				psqy + (sq_h - 1),
				psqx + (sq_w - 1),
				(int)(psqy + ylen * (prop - 1.0)));
/*@i@*/			XDrawLine(uix->display, uix->window, uix->gc,
				psqx + (sq_w - 1),
				psqy + (sq_h - 1),
				(int)(psqx + xlen * (prop - 1.0)),
				psqy + (sq_h - 1));
		}
	}
/*@i@*/	XSetForeground(uix->display, uix->gc, uix->px_white);

	return	HNEF_FR_SUCCESS;
}

static
enum HNEF_FR
hnef_uix_paint_pos (
/*@in@*/
/*@notnull@*/
	struct hnef_ui		* const HNEF_RSTR ui,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX		* const HNEF_RSTR frx,
	const HNEF_BOOL		force_repaint,
	const unsigned short	x,
	const unsigned short	y
	)
/*@modifies * ui, * frx@*/
{
	enum HNEF_FR	fr	= HNEF_FR_SUCCESS;
	unsigned short	pos;
	HNEF_BIT_U8	pbit,
			sbit;

	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->game->board);	/* Implies valid */
	assert	(NULL != ui->game->rules);
	assert	(NULL != ui->game->rules->pieces);
	assert	(NULL != ui->game->rules->squares);
	assert	(x < ui->game->rules->bwidth);
	assert	(y < ui->game->rules->bheight);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != frx);
	assert	(NULL != ui->uix->paintmap);
	assert	(ui->uix->paintmap_width == ui->game->rules->bwidth);
	assert	(ui->uix->paintmap_height == ui->game->rules->bheight);

	* frx	= HNEF_FRX_SUCCESS;
	pos	= (unsigned short)(y * ui->game->rules->bwidth + x);
	pbit	= ui->game->board->pieces[pos];

	if	(!force_repaint
	&&	pbit == ui->uix->paintmap[pos])
	{
		return	HNEF_FR_SUCCESS;
	}

	sbit	= ui->game->rules->squares[pos];
	if	(HNEF_BIT_U8_EMPTY != sbit)
	{
		struct hnef_type_square * const HNEF_RSTR ts =
			hnef_type_square_get(ui->game->rules, sbit);
		const HNEF_BIT_U8 ui_sbit = hnef_game_uibit_square(ts);
		assert	(HNEF_BIT_U8_EMPTY != ui_sbit);

		fr	= hnef_uix_paint_img
			(ui, ui_sbit, HNEF_TRUE, x, y, frx, ts);
		if	(!hnef_fr_good(fr)
		||	!hnef_frx_good(* frx))
		{
			return	fr;
		}
	}

	if	(HNEF_BIT_U8_EMPTY != pbit)
	{
		struct hnef_type_piece * const HNEF_RSTR tp =
			hnef_type_piece_get(ui->game->rules, pbit);
		const HNEF_BIT_U8 ui_pbit = hnef_game_uibit_piece(tp);
		assert	(HNEF_BIT_U8_EMPTY != ui_pbit);

		fr	= hnef_uix_paint_img
			(ui, ui_pbit, HNEF_FALSE, x, y, frx, tp);
		if	(!hnef_fr_good(fr)
		||	!hnef_frx_good(* frx))
		{
			return	fr;
		}
	}

	ui->uix->paintmap[pos]	= pbit;

	return	HNEF_FR_SUCCESS;
}

/*
 * NOTE:	`force_repaint` is ignored by this function. This
 *		function always repaints.
 */
static
enum HNEF_FR
hnef_uix_paint_board_placeholder (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX	* const HNEF_RSTR frx,
	const HNEF_BOOL	force_repaint
	)
/*@modifies * ui, * frx@*/
{
	/*
	 * The proportions of the Swedish flag:
	 * -	The whole flag is 10:16
	 * -	The inner blue fields are 4:5
	 * -	The out blue fields are 4:9
	 * -	The thickness of the yellow arms is half the height of
	 *	the blue fields (2; or height 4:2:4, width 5:2:9).
	 *
	 * The colors are exactly:
	 * -	NCS:
	 *	-	Yellow:	0580-Y10R
	 *	-	Blue:	4055-R95B
	 * -	CMYK:
	 *	-	Yellow:	100% yellow, 20% magenta; X200
	 *	-	Blue:	10% yellow, 50% magenta, 100% cyan; 15X0
	 *
	 * The colors are roughly (in RGB):
	 * -	Yellow:	#fecb00
	 * -	Blue:	#005293
	 */
	int	w_canvas,
		h_canvas,
		margin_bottom,
		unit,
		w_flag,
		h_flag,
		offset_x,
		offset_y;

	if	(force_repaint) {}	/* NOTE: GCC unused */

	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

	w_canvas	= ui->uix->window_width;
	margin_bottom	= hnef_uix_margin_bottom(ui->uix);
	h_canvas	= ui->uix->window_height - margin_bottom;
	unit		= hnef_min_int(w_canvas / HNEF_UIX_FLAG_UNITS_W,
				h_canvas / HNEF_UIX_FLAG_UNITS_H);
	if	(unit < 1)
	{
		return	HNEF_FR_SUCCESS;
	}

	w_flag		= unit * HNEF_UIX_FLAG_UNITS_W;
	h_flag		= unit * HNEF_UIX_FLAG_UNITS_H;
	offset_x	= (w_canvas - w_flag) / 2;
	offset_y	= (h_canvas - h_flag) / 2;

/*@i@*/	XSetForeground	(ui->uix->display, ui->uix->gc,
			ui->uix->col_flag_blue.pixel);

/*@i@*/	XFillRectangle(ui->uix->display, ui->uix->window, ui->uix->gc,
		offset_x,
		offset_y,
		(unsigned int)w_flag,
		(unsigned int)h_flag);

/*@i@*/	XSetForeground	(ui->uix->display, ui->uix->gc,
			ui->uix->col_flag_yellow.pixel);

/*@i@*/	XFillRectangle(ui->uix->display, ui->uix->window, ui->uix->gc,
		offset_x,
		offset_y + unit * 4,
		(unsigned int)w_flag,
		(unsigned int)unit * 2);
/*@i@*/	XFillRectangle(ui->uix->display, ui->uix->window, ui->uix->gc,
		offset_x + unit * 5,
		offset_y,
		(unsigned int)unit * 2,
		(unsigned int)h_flag);

/*@i@*/	XSetForeground	(ui->uix->display, ui->uix->gc,
			ui->uix->px_white);

	return	HNEF_FR_SUCCESS;
/* SPLint doesn't see that `ui->uix` is modified */ /*@i1@*/\
}

enum HNEF_FR
hnef_uix_paint_console_in (
	struct hnef_ui	* const HNEF_RSTR ui,
	enum HNEF_FRX	* const HNEF_RSTR frx,
	const HNEF_BOOL	force_repaint
	)
{
	enum HNEF_FR		fr		= HNEF_FR_SUCCESS;
	struct hnef_uix_console	* HNEF_RSTR console	= NULL;
	int			py,
				height;
	HNEF_BOOL		computer_turn;

	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->console);
	assert	(NULL != ui->uix->display);
	assert	(NULL != ui->uix->font);
	assert	(NULL != frx);

	console		= ui->uix->console;
	* frx		= HNEF_FRX_SUCCESS;

#ifdef	HNEFATAFL_UI_AIM
	computer_turn	= hnef_control_computer(ui);
#else	/* HNEFATAFL_UI_AIM */
	computer_turn	= HNEF_FALSE;
#endif	/* HNEFATAFL_UI_AIM */

	/*
	 * Actually, because of when the input console is repainted,
	 * this optimization (probably) never happens. The paint
	 * function is never called if clearing the console doesn't
	 * change its contents, and in other cases it's never called
	 * unless `console->input` changes (like when appending text,
	 * and adding text always changes `console->input`). But keep
	 * `input_last` anyway since all other painting functions have
	 * the same type of optimization (and maybe it does happen
	 * sometimes that I've not foreseen).
	 */
	if	(!force_repaint
	&&	0 == strcmp	(ui->uix->console->input_last,
				ui->uix->console->input)
/* SPLint boolean comparison */ /*@i1@*/\
	&&	ui->uix->console->input_computer_turn_last
				== computer_turn)
	{
		return	HNEF_FR_SUCCESS;
	}

	height	= ui->uix->font->ascent
		+ ui->uix->font->descent;
	py	= ui->uix->window_height
		- height
		- ui->uix->font->descent;

	gleip_line_rm(console->input_echo);
/*@-boolops@*/
	if	(!gleip_line_add_str_back_b(& console->input_echo,
							HNEF_UI_PROMPT)
	||	!gleip_line_add_str_back_b(& console->input_echo,
							console->input)
	||	!gleip_line_add_str_back_b(& console->input_echo,
							HNEF_UIX_CARET))
	{
		return	HNEF_FR_FAIL_ALLOC;
	}
/*@=boolops@*/

#ifdef	HNEFATAFL_UI_AIM
	if	(computer_turn)
	{
/*@i@*/		XSetForeground	(ui->uix->display, ui->uix->gc,
				ui->uix->col_red.pixel);
	}
#endif	/* HNEFATAFL_UI_AIM */

/*@i@*/	XClearArea(ui->uix->display, ui->uix->window,
		0,
		ui->uix->window_height - height * 2,
		(unsigned int)ui->uix->window_width,
		(unsigned int)height,
		False);

	fr	= hnef_uix_font_print(ui->uix, console->input_echo,
			0,
			py,
			frx);
	if	(!hnef_fr_good(fr)
	||	!hnef_frx_good(* frx))
	{
		return	fr;
	}

#ifdef	HNEFATAFL_UI_AIM
	if	(computer_turn)
	{
/*@i@*/		XSetForeground	(ui->uix->display, ui->uix->gc,
				ui->uix->px_white);
	}
#endif	/* HNEFATAFL_UI_AIM */

	ui->uix->console->input_computer_turn_last	= computer_turn;
/*@i1@*/\
	if	(!gleip_line_cpy_b(& ui->uix->console->input_last,
				ui->uix->console->input))
	{
		return	HNEF_FR_FAIL_ALLOC;
	}

	return	HNEF_FR_SUCCESS;
}

enum HNEF_FR
hnef_uix_paint_console_out (
	struct hnef_ui	* const HNEF_RSTR ui,
	enum HNEF_FRX	* const HNEF_RSTR frx,
	const HNEF_BOOL	force_repaint
	)
{
	enum HNEF_FR		fr		= HNEF_FR_SUCCESS;
	struct hnef_uix_console	* HNEF_RSTR console	= NULL;
	int			py,
				height;

	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->console);
	assert	(NULL != ui->uix->display);
	assert	(NULL != ui->uix->font);
	assert	(NULL != frx);

	console	= ui->uix->console;
	* frx	= HNEF_FRX_SUCCESS;

	/*
	 * Unlike the input console repaint optimization, it sometimes
	 * happens that the output text has not changed and therefore
	 * does not have to be repainted.
	 */
	if	(!force_repaint
	&&	0 == strcmp	(ui->uix->console->output_last,
				ui->uix->console->output)
	&&	ui->uix->console->output_error_last
				== ui->uix->console->output_error)
	{
		return	HNEF_FR_SUCCESS;
	}

	if	(ui->uix->console->output_error)
	{
/*@i@*/		XSetForeground	(ui->uix->display, ui->uix->gc,
				ui->uix->col_red.pixel);
	}

	height	= ui->uix->font->ascent
		+ ui->uix->font->descent;
	py	= ui->uix->window_height
		- ui->uix->font->descent;

/*@i@*/	XClearArea(ui->uix->display, ui->uix->window,
		0,
		ui->uix->window_height - height,
		(unsigned int)ui->uix->window_width,
		(unsigned int)height,
		False);

	fr	= hnef_uix_font_print	(ui->uix, console->output,
					0, py, frx);

	if	(ui->uix->console->output_error)
	{
/*@i@*/		XSetForeground	(ui->uix->display, ui->uix->gc,
				ui->uix->px_white);
	}

	if	(!hnef_fr_good(fr)
	||	!hnef_frx_good(* frx))
	{
		return	fr;
	}

	ui->uix->console->output_error_last =
			ui->uix->console->output_error;
/*@i1@*/\
	if	(!gleip_line_cpy_b(& ui->uix->console->output_last,
				ui->uix->console->output))
	{
		return	HNEF_FR_FAIL_ALLOC;
	}

	return	HNEF_FR_SUCCESS;
}

static
enum HNEF_FR
hnef_uix_paint_console (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX	* const HNEF_RSTR frx,
	const HNEF_BOOL	force_repaint
	)
/*@modifies * ui, * frx@*/
{
	enum HNEF_FR	fr	= HNEF_FR_SUCCESS;

	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != ui->uix->font);
	assert	(NULL != frx);

	fr	= hnef_uix_paint_console_in(ui, frx, force_repaint);
	if	(!hnef_fr_good(fr)
	||	!hnef_frx_good(* frx))
	{
		return	fr;
	}

	return	hnef_uix_paint_console_out(ui, frx, force_repaint);
}

/*
 * `hnef_uix_themes_load_rules()` must have been called for the current
 * ruleset.
 *
 * Normally this function does not paint squares that do not need to be
 * updated (because they have been painted previously and the board
 * hasn't changed since). But by passing `force_repaint`, you can force
 * every square to be painted anyway -- this is necessary when, for
 * example, the size of the window changes, and strongly recommended
 * when moves are undone.
 *
 * If the game is invalid, then a placeholder graphic is painted
 * instead.
 */
enum HNEF_FR
hnef_uix_paint_board (
	struct hnef_ui	* const HNEF_RSTR ui,
	enum HNEF_FRX	* const HNEF_RSTR frx,
	const HNEF_BOOL	force_repaint
	)
{
	enum HNEF_FR		fr		= HNEF_FR_SUCCESS;
	unsigned short		x,
				y;

	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

	if	(!hnef_rvalid_good(hnef_game_valid(ui->game)))
	{
		return	hnef_uix_paint_board_placeholder
			(ui, frx, force_repaint);
	}

	assert	(NULL != ui->game->board);
	assert	(NULL != ui->uix->paintmap);
	assert	(ui->uix->paintmap_width == ui->game->rules->bwidth);
	assert	(ui->uix->paintmap_height == ui->game->rules->bheight);

	for	(x = 0; x < ui->game->rules->bwidth; ++x)
	{
		for	(y = 0; y < ui->game->rules->bheight; ++y)
		{
			fr	= hnef_uix_paint_pos
				(ui, frx, force_repaint, x, y);
			if	(!hnef_fr_good(fr)
			||	!hnef_frx_good(* frx))
			{
				return	fr;
			}
		}
	}

	if	(ui->uix->lastmove_show
	&&	ui->game->movehist->elemc > 0)
	{
		const struct hnef_moveh	* HNEF_RSTR last =
				& ui->game->movehist->elems
				[ui->game->movehist->elemc - 1];
		const int	sq_w	= hnef_uix_square_size_w
					(ui->game, ui->uix->themes),
				sq_h	= hnef_uix_square_size_h
					(ui->game, ui->uix->themes);
		const unsigned short
			x1 = last->pos % ui->game->rules->bwidth,
			y1 = last->pos / ui->game->rules->bwidth,
			x2 = last->dest % ui->game->rules->bwidth,
			y2 = last->dest / ui->game->rules->bwidth;
		assert	(x1 < ui->game->rules->bwidth);
		assert	(x2 < ui->game->rules->bwidth);
		assert	(y1 < ui->game->rules->bheight);
		assert	(y2 < ui->game->rules->bheight);

/*@i@*/		XSetForeground	(ui->uix->display, ui->uix->gc,
				ui->uix->col_red.pixel);
/*@i@*/		XDrawLine(ui->uix->display, ui->uix->window,
			ui->uix->gc,
			ui->uix->opt_offset_x + x1 * sq_w + sq_w / 2,
			ui->uix->opt_offset_y + y1 * sq_h + sq_h / 2,
			ui->uix->opt_offset_x + x2 * sq_w + sq_w / 2,
			ui->uix->opt_offset_y + y2 * sq_h + sq_h / 2);
/*@i@*/		XSetForeground	(ui->uix->display, ui->uix->gc,
				ui->uix->px_white);
	}

	if	(hnef_uix_board_h(ui->game, ui->uix->themes)
			+ ui->uix->opt_offset_y
		> ui->uix->window_height
			- hnef_uix_margin_bottom(ui->uix))
	{
		/*
		 * This happens if the window is too small to fit the
		 * board. If so, the above board painting will have
		 * painted over the progress bar and / or consoles. So
		 * we have to paint the consoles and progress bar back
		 * again.
		 *
		 * NOTE:	When `hnef_uix_paint()` is called and
		 *		window is too small, consequently the
		 *		progress bar and consoles will be
		 *		painted twice. We could add code to cope
		 *		with that inefficiency so that it's only
		 *		painted once, but that would add
		 *		unnecessary complexity. Just make the
		 *		window larger!
		 */
		fr	= hnef_uix_paint_progress
			(ui, frx, HNEF_TRUE);
		if	(!hnef_fr_good(fr)
		||	!hnef_frx_good(* frx))
		{
			return	fr;
		}

		return	hnef_uix_paint_console(ui, frx, HNEF_TRUE);
	}
	else
	{
		return	HNEF_FR_SUCCESS;
	}
}

/*
 * The progress bar is so small that we paint it (in the background's
 * color, i.e. invisible) even if the computer player is not compiled
 * in. This of it as a margin between the board and the consoles.
 *
 * You can indicate that the next call to this function should be forced
 * by setting `ui->uix->progress_last` to `HNEF_UIX_PROGRESS_INVALID`.
 */
enum HNEF_FR
hnef_uix_paint_progress (
	struct hnef_ui	* const HNEF_RSTR ui,
	enum HNEF_FRX	* const HNEF_RSTR frx,
	const HNEF_BOOL	force_repaint
	)
{
	int	py,
		consoles_h;

	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

	if	(!force_repaint
	&&	ui->uix->progress	== ui->uix->progress_last
	&&	ui->uix->progress_max	== ui->uix->progress_max_last)
	{
		return	HNEF_FR_SUCCESS;
	}

	consoles_h	= (ui->uix->font->ascent
			+ ui->uix->font->descent)
			* 2;
	py		= ui->uix->window_height
			- consoles_h
			- ui->uix->progress_height;

/*@i@*/	XClearArea(ui->uix->display, ui->uix->window,
		0,
		py,
		(unsigned int)ui->uix->window_width,
		(unsigned int)ui->uix->progress_height,
		False);

#ifdef	HNEFATAFL_UI_AIM
	if	(hnef_control_computer(ui))
	{
		const int progw	= (ui->uix->window_width
				* ui->uix->progress)
				/ ui->uix->progress_max;
/*@i@*/		XSetForeground	(ui->uix->display, ui->uix->gc,
				ui->uix->col_red.pixel);
/*@i@*/		XFillRectangle(ui->uix->display, ui->uix->window,
			ui->uix->gc,
			0,
			py,
			(unsigned int)progw,
			(unsigned int)ui->uix->progress_height);
/*@i@*/		XSetForeground(ui->uix->display, ui->uix->gc,
			ui->uix->px_white);
	}
#endif	/* HNEFATAFL_UI_AIM */

	/*
	 * `progress_max` doesn't ever change, but check it
	 * anyway just to be safe.
	 */
	ui->uix->progress_last		= ui->uix->progress;
	ui->uix->progress_max_last	= ui->uix->progress_max;

	return	HNEF_FR_SUCCESS;
}

/*
 * The game does not have to be valid. If not, it will paint the board
 * differently.
 */
enum HNEF_FR
hnef_uix_paint (
	struct hnef_ui	* const HNEF_RSTR ui,
	enum HNEF_FRX	* const HNEF_RSTR frx,
	const HNEF_BOOL	force_repaint
	)
{
	enum HNEF_FR	fr	= HNEF_FR_SUCCESS;

	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != frx);

	if	(force_repaint)
	{
		ui->uix->opt_repaint_due	= HNEF_FALSE;

		/*
		 * Full repaint happens (among other cases) when the
		 * window is resized. If we don't clear the background,
		 * we can get all sorts of graphical "leftovers"; for
		 * example the old progress bar can still be around
		 * higher than it normally is painted if we increase the
		 * height of the window without clearing the background.
		 */
/*@i@*/		XClearArea(ui->uix->display, ui->uix->window,
			0,
			0,
			(unsigned int)ui->uix->window_width,
			(unsigned int)ui->uix->window_height,
			False);
	}

	fr	= hnef_uix_paint_board(ui, frx, force_repaint);
	if	(!hnef_fr_good(fr)
	||	!hnef_frx_good(* frx))
	{
		return	fr;
	}

	fr	= hnef_uix_paint_progress(ui, frx, force_repaint);
	if	(!hnef_fr_good(fr)
	||	!hnef_frx_good(* frx))
	{
		return	fr;
	}

	return	hnef_uix_paint_console(ui, frx, force_repaint);
}

#endif	/* HNEFATAFL_UI_XLIB */

