/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifdef	HNEFATAFL_UI_XLIB

#include <assert.h>	/* assert */

#include "X11/Xutil.h"	/* XK_* */

#include "gleipnir_line.h"	/* gleip_line_* */

#include "board.h"	/* hnef_game_moves_get_pos */
#include "cliio.h"	/* hnef_beep */
#include "funcx.h"	/* hnef_frx_good */
#include "langkeyt.h"	/* HNEF_LK_* */
#include "uiutil.h"	/* hnef_computer_* */
#include "uix.h"	/* hnef_uix_* */
#include "uixconsole.h"	/* hnef_uix_console* */
#include "uixgame.h"	/* hnef_uix_game* */
#include "uixpaint.h"	/* hnef_uix_paint* */
#include "uixtheme.h"	/* hnef_uix_theme* */
#include "uixutil.h"	/* hnef_uix_* */
#include "uixutilt.h"	/* HNEF_BIT_U8_REPAINT */
#include "xlib.h"

/*
 * NOTE:	Some of XLib's functions return integer values. The
 *		return values are checked when they return `Status` or
 *		when the return value is documented in the standard. The
 *		return values are ignored when they are undocumented and
 *		return an `int` that can mean anything. (Earlier
 *		versions of the X interface attempted to guess what they
 *		mean, but that may be even worse.)
 *
 * NOTE:	XLib is not annotated for SPLint, so we just suppress
 *		all XLib-related warnings.
 */

/*
 * The game may or may not be valid.
 */
static
enum HNEF_FR
hnef_handle_event_key (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX	* const HNEF_RSTR frx
#ifdef	HNEFATAFL_UI_AIM
	,
	const HNEF_BOOL	ai_callback
#endif	/* HNEFATAFL_UI_AIM */
	)
/*@globals errno, fileSystem, internalState, stderr, stdout@*/
/*@modifies errno, fileSystem, internalState, stderr, stdout@*/
/*@modifies * ui, ui->quit, * frx@*/
{
	XEvent		* HNEF_RSTR event	= NULL;
	KeySym		ks;

	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;
	event	= & ui->uix->event;

	assert	(KeyPress == event->type);

/*@i@*/	ks	= XLookupKeysym(& event->xkey, 0);
	if	((KeySym)XK_BackSpace == ks
	||	(KeySym)XK_Delete == ks)
	{
		return	hnef_uix_console_key_bkspace(ui, frx);
	}
	else if	((KeySym)XK_Return == ks)
	{
		return	hnef_uix_console_key_enter(ui, frx
#ifdef	HNEFATAFL_UI_AIM
			, ai_callback
#endif	/* HNEFATAFL_UI_AIM */
			);
	}
	else if	((KeySym)XK_Escape == ks)
	{
		return	hnef_uix_console_key_escape(ui, frx);
	}
	else
	{
		size_t	max	= gleip_line_mem
				(ui->uix->opt_lookupstring) - 2;
		assert	(max > 0);
		gleip_line_rm	(ui->uix->opt_lookupstring);
		/*
		 * NOTE:	`X*LookupString()` leaks memory through
		 *		`XrmQuark`s like `XCreateFontSet()` (see
		 *		`uixfont.c`). This is the same leak,
		 *		because:
		 *		-	If we create the FontSet, but
		 *			don't call `X*LookupString()`,
		 *			then it leaks (through
		 *			`XrmQuarkToString()`) 38684
		 *			bytes in 806 blocks ("still
		 *			reachable").
		 *		-	If we create the FontSet and
		 *			call `X*LookupString()`, then it
		 *			leaks 38684 bytes in 806 blocks
		 *			("still reachable") through
		 *			`XrmQuarkToString()`.
		 *		-	If we don't create the FontSet
		 *			(and instead use Latin-1
		 *			fallback) but call
		 *			`X*LookupString()`, then it
		 *			leaks 20958 bytes in 387 blocks
		 *			("still reachable") through
		 *			`XrmQuarkToString()`.
		 *		-	If we don't create the FontSet
		 *			(and use the Latin-1 fallback)
		 *			and don't call
		 *			`X*LookupString()` (we close X
		 *			by closing the window using the
		 *			window manager's close button or
		 *			"delete" function), then it
		 *			doesn't leak any memory at all.
		 *		Thus this *must* either be a false
		 *		positive (indeed, there are no "lost"
		 *		bytes, just "still reachable"), or the
		 *		quark returned by `XrmQuarkToString()`
		 *		must be freed somehow. However there is
		 *		no way to free `XrmQuark`s and the XLib
		 *		standard doesn't say that the returned
		 *		`XrmQuark` should be freed, or even that
		 *		they can be freed in any way.
		 */
		if	(ui->uix->font->use_fontset)
		{
			int	retval;
			Status	status;
			assert	(NULL != ui->uix->font->fontset);

/*@i@*/			retval	= XmbLookupString(ui->uix->xic,
				& event->xkey,
				ui->uix->opt_lookupstring, (int)max,
/*@i@*/				NULL, & status);
			/*
			 * NOTE:	If you make room for 1 `char`
			 *		(ASCII) and type in "Å" (two
			 *		`char`s), then `retval` is 0 and
			 *		`status` is not
			 *		`XBufferOverflow`. So the buffer
			 *		will never grow in that case.
			 *		But if you type in `é`, then it
			 *		returns `XBufferOverflow` (with
			 *		`retval == -1`) and grows like
			 *		it should. This must be an XLib
			 *		bug since overflow should be
			 *		returned for multi-byte chars if
			 *		there are not enough bytes.
			 *		However we allocate 3 times more
			 *		than the maximum UTF-8 sequence
			 *		to begin with, so we should be
			 *		safe.
			 */
/*@i@*/			if	(XBufferOverflow == status)
			{
/*@i1@*/\
				if	(!gleip_line_mem_grow_b
					(& ui->uix->opt_lookupstring,
						(size_t)(retval + 2)))
				{
					return	HNEF_FR_FAIL_ALLOC;
				}
				max	= gleip_line_mem
					(ui->uix->opt_lookupstring) - 2;
				assert	(max >= (size_t)retval);
/*@i@*/				retval	= XmbLookupString(ui->uix->xic,
					& event->xkey,
					ui->uix->opt_lookupstring,
/*@i@*/					(int)max, NULL, & status);
				assert	(XBufferOverflow != status);
			}

			if	(retval < 1
			||	XLookupNone	== status
			||	XLookupKeySym	== status)
			{
				return	HNEF_FR_SUCCESS;
			}
		}
		else
		{
/*@i@*/			if	(XLookupString(& event->xkey,
				ui->uix->opt_lookupstring, (int)max,
/*@i@*/				NULL, NULL) < 1)
			{
				/*
				 * `XLookupString()` returns number of
				 * chars stored in the buffer. This is
				 * empty string.
				 */
				return	HNEF_FR_SUCCESS;
			}
		}

		return	hnef_line_empty(ui->uix->opt_lookupstring)
			? HNEF_FR_SUCCESS
			: hnef_uix_console_append
				(ui, frx, ui->uix->opt_lookupstring);
	}
}

static
enum HNEF_FR
hnef_handle_event_click (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX	* const HNEF_RSTR frx
#ifdef	HNEFATAFL_UI_AIM
	,
	const HNEF_BOOL	ai_callback
#endif	/* HNEFATAFL_UI_AIM */
	)
/*@globals errno, fileSystem, internalState, stdout@*/
/*@modifies errno, fileSystem, internalState, stdout, * ui, * frx@*/
{
	enum HNEF_FR		fr		= HNEF_FR_SUCCESS;
	XEvent			* event			= NULL;
	XButtonPressedEvent	* event_press		= NULL;
	HNEF_BOOL		left;
	int			x_board,
				y_board;
	unsigned short		x_pos,
				y_pos,
				pos,
				ignored		= HNEF_PLAYER_UNINIT;
	HNEF_BIT_U8		pbit;
	struct hnef_type_piece	* HNEF_RSTR tp_friendly	= NULL;

	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != frx);

	* frx		= HNEF_FRX_SUCCESS;
	event		= & ui->uix->event;

	assert	(ButtonPress == event->type);

	event_press	= (XButtonPressedEvent *) event;

	if	(!hnef_rvalid_good((hnef_game_valid(ui->game))))
	{
		return	HNEF_FR_SUCCESS;
	}

	assert	(NULL != ui->uix->paintmap);
	assert	(ui->uix->paintmap_width == ui->game->rules->bwidth);
	assert	(ui->uix->paintmap_height == ui->game->rules->bheight);

	/*
	 * NOTE:	We have Button[1-5].
	 *		-	Button1 appears to be left.
	 *		-	Button2 appears to be middle.
	 *		-	Button3 appears to be right.
	 *		-	Button[4-5] appears to be scroll wheel.
	 *		So we do it like this:
	 *		-	Button[1]	= left click
	 *		-	Button[2-3]	= right click
	 *		-	Button[4-5]	= ignored
	 */
/*@i@*/	if	(Button4 == event_press->button
/*@i@*/	||	Button5 == event_press->button)
	{
		return	HNEF_FR_SUCCESS;
	}
/*@i@*/	left	= Button2 != event_press->button
/*@i@*/		&& Button3 != event_press->button;

	assert	(NULL != ui->game->board);

	x_board	= event->xbutton.x - ui->uix->opt_offset_x;
	y_board	= event->xbutton.y - ui->uix->opt_offset_y;
	if	(x_board < 0
	||	x_board > hnef_uix_board_w(ui->game, ui->uix->themes)
	||	y_board < 0
	||	y_board > hnef_uix_board_h(ui->game, ui->uix->themes))
	{
		/*
		 * Off-board.
		 */
		return	HNEF_FR_SUCCESS;
	}

	if (event->xbutton.y
	> ui->uix->window_height - hnef_uix_margin_bottom(ui->uix))
	{
		/*
		 * This happens of the screen is too small to fit the
		 * board. In this case, the user clicks on the console
		 * or the progress bar, and it would be odd if the board
		 * could be clicked through the console or progress bar.
		 */
		return	HNEF_FR_SUCCESS;
	}

	x_pos	= (unsigned short)(x_board / hnef_uix_square_size_w
				(ui->game, ui->uix->themes));
	y_pos	= (unsigned short)(y_board / hnef_uix_square_size_h
				(ui->game, ui->uix->themes));
	pos	= (unsigned short)
		(y_pos * ui->game->rules->bwidth + x_pos);
	pbit	= ui->game->board->pieces[pos];

	if	(x_pos >= ui->game->rules->bwidth
	||	y_pos >= ui->game->rules->bheight
	||	pos >= ui->game->rules->opt_blen)
	{
		/*
		 * This can rarely happen due to rounding when the user
		 * clicks on the edge of the board.
		 */
		return	HNEF_FR_SUCCESS;
	}

#ifdef	HNEFATAFL_UI_AIM
	if	(ai_callback)
	{
		/*
		 * Computer turn.
		 */
		(void)hnef_beep();
		return	HNEF_FR_SUCCESS;
	}
#endif	/* HNEFATAFL_UI_AIM */

	if	(hnef_game_over(ui->game, & ignored))
	{
		(void)hnef_beep();
		return	HNEF_FR_SUCCESS;
	}

	if	(HNEF_BIT_U8_EMPTY != pbit)
	{
		tp_friendly	= hnef_type_piece_get
				(ui->game->rules, pbit);
		if	(ui->game->board->turn != tp_friendly->owner)
		{
			(void)hnef_beep();
			return	HNEF_FR_SUCCESS;
		}
	}
	else
	{
		/*
		 * NOTE:	This code assumes that a selected piece
		 *		can only move to empty squares.
		 */
		if	(HNEF_BOARDPOS_NONE != ui->uix->pos_select
		&&	left)
		{
			HNEF_BOOL	legal;
			fr	= hnef_game_move(ui->game,
				ui->uix->pos_select, pos, & legal);
			if	(!hnef_fr_good(fr))
			{
				return	fr;
			}
			else if	(legal)
			{
				hnef_uix_marker_clear_all(ui);

				if (hnef_game_over(ui->game, & ignored))
				{
					fr	= hnef_uix_print_langkey
						(ui, frx,
						HNEF_LK_CMD_GAME_OVER,
							HNEF_FALSE);
					if	(!hnef_fr_good(fr)
					||	!hnef_frx_good(* frx))
					{
						return	fr;
					}
				}

				return	hnef_uix_paint_board
					(ui, frx, HNEF_FALSE);
			}
			else
			{
				(void)hnef_beep();
				return	HNEF_FR_SUCCESS;
			}
		}
		else
		{
			(void)hnef_beep();
			return	HNEF_FR_SUCCESS;
		}
	}
	assert	(NULL != tp_friendly);

	if	(left)
	{
		if	(HNEF_BOARDPOS_NONE == ui->uix->pos_select)
		{
			ui->uix->pos_select	= pos;
		}
		else
		{
			ui->uix->paintmap[ui->uix->pos_select] =
				HNEF_BIT_U8_REPAINT;
			if	(pos == ui->uix->pos_select)
			{
				hnef_uix_marker_clear_select(ui);
			}
			else
			{
				ui->uix->pos_select	= pos;
			}
		}
		ui->uix->paintmap[pos]	= HNEF_BIT_U8_REPAINT;

		return	hnef_uix_paint_board(ui, frx, HNEF_FALSE);
	}
	else
	{
		unsigned short	pos_old	= HNEF_BOARDPOS_NONE;
		size_t		i;

		for	(i = 0; i < ui->opt_movelist->elemc; ++i)
		{
			struct hnef_move * const HNEF_RSTR move =
					& ui->opt_movelist->elems[i];
			pos_old	= move->pos;
			assert	(HNEF_BOARDPOS_NONE != pos_old);

			/*
			 * All moves have the same `pos`, so we just
			 * need the first one.
			 */
			break;
		}

		hnef_uix_marker_clear_movelist(ui);

		if	(pos != pos_old)
		{
			fr	= hnef_game_moves_get_pos
				(ui->game, ui->opt_movelist, pos);
			if	(!hnef_fr_good(fr))
			{
				return	fr;
			}
			for (i = 0; i < ui->opt_movelist->elemc; ++i)
			{
				struct hnef_move
					* const HNEF_RSTR move =
					& ui->opt_movelist->elems[i];
				ui->uix->paintmap[move->pos] =
					HNEF_BIT_U8_REPAINT;
				ui->uix->paintmap[move->dest] =
					HNEF_BIT_U8_REPAINT;
			}
		}

		ui->uix->paintmap[pos]	= HNEF_BIT_U8_REPAINT;

		return	hnef_uix_paint_board(ui, frx, HNEF_FALSE);
	}
}

/*
 * The game may or may not be valid.
 */
static
enum HNEF_FR
hnef_handle_event (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX	* const HNEF_RSTR frx
#ifdef	HNEFATAFL_UI_AIM
	,
	const HNEF_BOOL	ai_callback
#endif	/* HNEFATAFL_UI_AIM */
	)
/*@globals errno, fileSystem, internalState, stderr, stdout@*/
/*@modifies errno, fileSystem, internalState, stderr, stdout@*/
/*@modifies * ui, ui->quit, * frx@*/
{
	HNEF_BOOL	repainted		= HNEF_FALSE;
	enum HNEF_FR	fr			= HNEF_FR_SUCCESS;
	XEvent		* HNEF_RSTR event	= NULL;

	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;
	event	= & ui->uix->event;

	if	(ui->uix->opt_repaint_due)
	{
		repainted	= HNEF_TRUE;

		* frx	= hnef_uix_window_size_update(ui, NULL);
		if	(!hnef_frx_good(* frx))
		{
			return	HNEF_FR_FAIL_OTHER;
		}

		/*
		 * `hnef_uix_paint()` resets `opt_repaint_due`.
		 */
		fr	= hnef_uix_paint(ui, frx, HNEF_TRUE);
		if	(!hnef_fr_good(fr)
		||	!hnef_frx_good(* frx))
		{
			return	fr;
		}
	}

/*@i2@*/\
	if	(XFilterEvent(event, ui->uix->window))
	{
		return	HNEF_FR_SUCCESS;
	}

	if	(ClientMessage == event->type
	&&	ui->uix->window_del == (Atom)event->xclient.data.l[0])
	{
#ifdef	HNEFATAFL_UI_AIM
		if	(ai_callback)
		{
			ui->uix->ai_stop	= HNEF_TRUE;
		}
#endif	/* HNEFATAFL_UI_AIM */
		ui->uix->quit_xlib		= HNEF_TRUE;
		return				HNEF_FR_SUCCESS;
	}
	else if	(ClientMessage == event->type
	&&	ui->uix->focus_take == (Atom)event->xclient.data.l[0])
	{
/*@i@*/		XSetInputFocus(ui->uix->display, ui->uix->window,
/*@i@*/			RevertToParent, (Time)event->xclient.data.l[1]);
		return	HNEF_FR_SUCCESS;
	}
	else if	(MappingNotify == event->type)
	{
/*@i@*/		XRefreshKeyboardMapping(& event->xmapping);
	}
	else if	(Expose == event->type)
	{
		if	(0 == event->xexpose.count
		&&	!repainted)
		{
			* frx	= hnef_uix_window_size_update(ui, NULL);
			if	(!hnef_frx_good(* frx))
			{
				return	HNEF_FR_FAIL_OTHER;
			}

			fr	= hnef_uix_paint(ui, frx, HNEF_TRUE);
			if	(!hnef_fr_good(fr)
			||	!hnef_frx_good(* frx))
			{
				return	fr;
			}
		}
	}
	else if	(ButtonPress == event->type)
	{
		return	hnef_handle_event_click(ui, frx
#ifdef	HNEFATAFL_UI_AIM
			, ai_callback
#endif	/* HNEFATAFL_UI_AIM */
			);
	}
/*@i1@*/\
	else if	(KeyPress == event->type)
	{
		return	hnef_handle_event_key(ui, frx
#ifdef	HNEFATAFL_UI_AIM
			, ai_callback
#endif	/* HNEFATAFL_UI_AIM */
			);
	}

	return	HNEF_FR_SUCCESS;
}

static
enum HNEF_FR
hnef_event_block (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX	* const HNEF_RSTR frx
	)
/*@globals errno, fileSystem, internalState, stderr, stdout@*/
/*@modifies errno, fileSystem, internalState, stderr, stdout@*/
/*@modifies * ui, ui->quit, * frx@*/
{
	enum HNEF_FR	fr		= HNEF_FR_SUCCESS;
	struct hnef_uix	* HNEF_RSTR uix	= NULL;

	assert	(NULL != ui);
	assert	(NULL != ui->game);

	uix	= ui->uix;

	assert	(NULL != uix);
	assert	(NULL != uix->display);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

/*@i@*/	while	(XPending(uix->display) > 0)
	{
		/*
		 * Flush old pendings events first. Handling these
		 * events allows the user to hold down, for example, the
		 * backspace key in the console to keep deleting
		 * characters until the key is released. Otherwise you
		 * would have to delete one character per key
		 * press-release.
		 */
/*@i@*/		XNextEvent(uix->display, & uix->event);

		if	(Expose == uix->event.type)
		{
			/*
			 * This is an optimization to reduce the amount
			 * of unnecessary repaints.
			 */
			uix->opt_repaint_due	= HNEF_TRUE;
		}
		else
		{
			fr	= hnef_handle_event(ui, frx
#ifdef	HNEFATAFL_UI_AIM
				, HNEF_FALSE
#endif	/* HNEFATAFL_UI_AIM */
				);
			if	(!hnef_fr_good(fr)
			||	!hnef_frx_good(* frx))
			{
				return	fr;
			}
		}

		if	(ui->quit
		||	ui->uix->quit_xlib)
		{
			return	HNEF_FR_SUCCESS;
		}
	}


	if	(ui->uix->opt_repaint_due)
	{
		* frx	= hnef_uix_window_size_update(ui, NULL);
		if	(!hnef_frx_good(* frx))
		{
			return	HNEF_FR_FAIL_OTHER;
		}

		/*
		 * `hnef_uix_paint()` resets `opt_repaint_due`.
		 */
		fr	= hnef_uix_paint(ui, frx, HNEF_TRUE);
		if	(!hnef_fr_good(fr)
		||	!hnef_frx_good(* frx))
		{
			return	fr;
		}
	}

/*@i@*/	XNextEvent(uix->display, & uix->event);

	fr	= hnef_handle_event(ui, frx
#ifdef	HNEFATAFL_UI_AIM
		, HNEF_FALSE
#endif	/* HNEFATAFL_UI_AIM */
		);
	if	(!hnef_fr_good(fr)
	||	!hnef_frx_good(* frx))
	{
		return	fr;
	}

	return	HNEF_FR_SUCCESS;
}

#ifdef	HNEFATAFL_UI_AIM

/*
 * Checks pending events without blocking. This is only used by the
 * computer player, and delegates `hnef_handle_event()` with
 * `ai_callback = true`.
 *
 * When it's the human player's turn, `hnef_event_block()` is called,
 * which waits (blocks) for the user to press some key, click somewhere
 * with the mouse or close the window.
 *
 * Other than that they work pretty much the same way, with some minor
 * differences arising out of the blocking of events (specifically, the
 * `hnef_event_block()` function discards all pending events without
 * handling them, except for window closing events) or whether it's an
 * AI callback (`hnef_event_block()` is always an AI callback and
 * `hnef_event_noblock()` is never an AI callback).
 *
 * The game must be valid (not really, but this is only called by the
 * AI, which implies that the game is valid).
 */
static
enum HNEF_FR
hnef_event_noblock (
/*@in@*/
/*@notnull@*/
	struct hnef_ui	* const HNEF_RSTR ui,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX	* const HNEF_RSTR frx
	)
/*@globals errno, fileSystem, internalState, stderr, stdout@*/
/*@modifies errno, fileSystem, internalState, stderr, stdout@*/
/*@modifies * ui, ui->quit, * frx@*/
{
	struct hnef_uix	* HNEF_RSTR uix	= NULL;

	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->game->board);	/* Implies valid */

	uix	= ui->uix;

	assert	(NULL != uix);
	assert	(NULL != uix->display);
	assert	(NULL != uix->paintmap);
	assert	(uix->paintmap_width == ui->game->rules->bwidth);
	assert	(uix->paintmap_height == ui->game->rules->bheight);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

/*@i@*/	while	(!ui->quit
	&&	!ui->uix->quit_xlib
/*@i@*/	&&	XPending(uix->display) > 0)
	{
		enum HNEF_FR	fr	= HNEF_FR_SUCCESS;

/*@i@*/		XNextEvent(uix->display, & uix->event);

		fr	= hnef_handle_event(ui, frx, HNEF_TRUE);
		if	(!hnef_fr_good(fr)
		||	!hnef_frx_good(* frx))
		{
			return	fr;
		}
	}

	return	HNEF_FR_SUCCESS;
}

/*
 * The game must be valid (not really, but this is only called by the
 * AI, which implies that the game is valid).
 */
static
enum HNEF_FR
hnef_callback_xlib_interrupt (
/*@in@*/
/*@notnull@*/
	void		* data,
/*@in@*/
/*@notnull@*/
	HNEF_BOOL	* force,
/*@in@*/
/*@notnull@*/
	HNEF_BOOL	* stop
	)
/*@globals errno, fileSystem, internalState, stderr, stdout@*/
/*@modifies errno, fileSystem, internalState, stderr, stdout@*/
/*@modifies * data, * force, * stop@*/
{
	enum HNEF_FR	fr		= HNEF_FR_SUCCESS;
	struct hnef_ui	* HNEF_RSTR ui	= NULL;

	assert	(NULL != data);
	assert	(NULL != force);
	assert	(NULL != stop);

	ui	= (struct hnef_ui *)data;

	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->paintmap);
	assert	(ui->uix->paintmap_width == ui->game->rules->bwidth);
	assert	(ui->uix->paintmap_height == ui->game->rules->bheight);

	/*
	 * May have been set by `hnef_callback_xlib_progress()`.
	 */
	if	(ui->uix->ai_stop
	||	ui->quit
	||	ui->uix->quit_xlib)
	{
		* stop	= HNEF_TRUE;
		return	HNEF_FR_SUCCESS;
	}
/*@i1@*/\
	else if	(ui->uix->ai_force)
	{
		* force	= HNEF_TRUE;
		return	HNEF_FR_SUCCESS;
	}

	assert	(NULL != ui->uix->display);
	assert	(NULL != ui->game);
	assert	(NULL != ui->game->board);	/* Implies valid */

	fr	= hnef_event_noblock(ui, & ui->uix->frx);

	if	(ui->uix->ai_stop
	||	ui->quit
	||	ui->uix->quit_xlib)
	{
		* stop	= HNEF_TRUE;
	}
/*@i1@*/\
	else if	(ui->uix->ai_force)
	{
		* force	= HNEF_TRUE;
	}

	if	(!hnef_fr_good(fr)
	||	!hnef_frx_good(ui->uix->frx))
	{
		return	fr;
	}

	return	HNEF_FR_SUCCESS;
}

/*
 * The game must be valid (not really, but this is only called by the
 * AI, which implies that the game is valid).
 */
static
enum HNEF_FR
hnef_callback_xlib_progress (
/*@in@*/
/*@notnull@*/
	void	* data,
	int	progress,
	int	progress_max
	)
/*@globals errno, fileSystem, internalState, stderr, stdout@*/
/*@modifies errno, fileSystem, internalState, stderr, stdout, * data@*/
{
	enum HNEF_FR	fr		= HNEF_FR_SUCCESS;
	struct hnef_ui	* HNEF_RSTR ui	= NULL;

	assert	(NULL != data);

	ui	= (struct hnef_ui *)data;

	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);
	assert	(NULL != ui->game);
	assert	(NULL != ui->game->board);	/* Implies valid */
	assert	(NULL != ui->uix->paintmap);
	assert	(ui->uix->paintmap_width == ui->game->rules->bwidth);
	assert	(ui->uix->paintmap_height == ui->game->rules->bheight);

	fr	= hnef_event_noblock(ui, & ui->uix->frx);
	if	(!hnef_fr_good(fr)
	||	!hnef_frx_good(ui->uix->frx))
	{
		return	fr;
	}

	ui->uix->progress	= progress;
	ui->uix->progress_max	= progress_max;

	fr = hnef_uix_paint_progress(ui, & ui->uix->frx, HNEF_FALSE);
	if	(!hnef_fr_good(fr)
	||	!hnef_frx_good(ui->uix->frx))
	{
		return	fr;
	}

	return	HNEF_FR_SUCCESS;
}

#endif	/* HNEFATAFL_UI_AIM */

/*
 * The game does not have to be valid. If so, a placeholder background
 * will be painted.
 *
 * Returns `HNEF_FR_OTHER` for XLib failures, in which case `frx` is
 * set. `frx` may be set for other `HNEF_FR_FAIL_*` values, but it's
 * never set for `HNEF_FR_SUCCESS`.
 */
enum HNEF_FR
hnef_run_xlib (
	struct hnef_ui	* const HNEF_RSTR ui,
	const char	* const HNEF_RSTR ui_theme,
	enum HNEF_FRX	* const HNEF_RSTR frx
	)
{
	enum HNEF_FR	fr		= HNEF_FR_SUCCESS;
	HNEF_BOOL	success		= HNEF_FALSE;

	assert	(NULL != ui);
	assert	(NULL != frx);

	* frx	= HNEF_FRX_SUCCESS;

	assert	(NULL != ui->game);

	/*
	 * One-time lazy initialization of XLib.
	 */
	if	(NULL == ui->uix
	&&	NULL == (ui->uix = hnef_alloc_uix_init
			(ui->lang, frx, ui->setlocale_return)))
	{
		return	hnef_frx_good(* frx)
			? HNEF_FR_FAIL_ALLOC
			: HNEF_FR_FAIL_OTHER;
	}

	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);

	ui->uix->quit_xlib	= HNEF_FALSE;
#ifdef	HNEFATAFL_UI_AIM
	ui->uix->ai_force	= ui->uix->ai_stop	= HNEF_FALSE;
#endif	/* HNEFATAFL_UI_AIM */

	(void)hnef_uix_console_clear(ui);

	/*
	 * Doing theme file initialization every time lets the user
	 * correct a missing theme file by closing the XLib interface
	 * and going back to the CLI, then starting the XLib interface
	 * again once the file (or path to it) has been fixed.
	 */
	fr	= hnef_uix_themes_init_file(ui, & success);
	if	(!hnef_fr_good(fr))
	{
		return	fr;
	}
/*@i1@*/\
	else if	(!success)
	{
		fr	= hnef_uix_print_langkey(ui, frx,
			HNEF_LK_XLIB_THEMES_NOT_FOUND, HNEF_TRUE);
		if	(!hnef_fr_good(fr)
		||	!hnef_frx_good(* frx))
		{
			return	fr;
		}
	}
	assert	(hnef_frx_good(* frx));

	if	(hnef_rvalid_good(hnef_game_valid(ui->game)))
	{
		fr	= hnef_uix_game_update_rules(ui, ui_theme, frx);
		if	(!hnef_fr_good(fr)
		||	!hnef_frx_good(* frx))
		{
			return	fr;
		}
	}
	else
	{
		/*
		 * NOTE:	Non-fatal failure; just print it.
		 */
		* frx	= hnef_uix_window_update(ui);
		if	(!hnef_frx_good(* frx))
		{
			(void)hnef_print_fail_frx
					(ui->lang, * frx, stderr);
			* frx	= HNEF_FRX_SUCCESS;
		}
	}

	hnef_uix_marker_clear_all(ui);

	* frx	= hnef_uix_window_show(ui);
	if	(!hnef_frx_good(* frx))
	{
		fr	= HNEF_FR_FAIL_OTHER;
		goto	RETURN_FR;
	}

	if	(hnef_line_empty(ui->uix->console->output)
	&&	!hnef_rvalid_good(hnef_game_valid(ui->game))
/*@i1@*/\
	&&	gleip_lang_valid(ui->lang))
	{
		fr	= hnef_uix_print_langkey(ui, frx,
			HNEF_LK_CMD_RULES_LOADHINT, HNEF_FALSE);
		if	(!hnef_fr_good(fr)
		||	!hnef_frx_good(* frx))
		{
			goto	RETURN_FR;
		}
	}

	fr	= hnef_uix_paint(ui, frx, HNEF_TRUE);
	if	(!hnef_fr_good(fr)
	||	!hnef_frx_good(* frx))
	{
		goto	RETURN_FR;
	}

	while	(!ui->quit
	&&	!ui->uix->quit_xlib)
	{
		enum HNEF_CONTROL_TYPE type = hnef_control_type_get(ui);
		if	(HNEF_CONTROL_TYPE_HUMAN == type)
		{
			fr	= hnef_event_block(ui, frx);
			if	(!hnef_fr_good(fr)
			||	!hnef_frx_good(* frx))
			{
				goto	RETURN_FR;
			}
		}
#ifdef	HNEFATAFL_UI_AIM
/*@i2@*/\
		else if	(HNEF_CONTROL_TYPE_AIM == type)
		{
			fr	= hnef_uix_paint_console_in
				(ui, frx, HNEF_FALSE);
			if	(!hnef_fr_good(fr)
			||	!hnef_frx_good(* frx))
			{
				goto	RETURN_FR;
			}

			/*
			 * Force progress repaint on next paint call.
			 */
			ui->uix->progress_last =
					HNEF_UIX_PROGRESS_INVALID;

			ui->uix->ai_force	= ui->uix->ai_stop
						= HNEF_FALSE;

			/*
			 * The callback functions don't know anything
			 * about `HNEF_FRX` since that enum doesn't
			 * exist in the `core` or `aim` component. So we
			 * have to have the `HNEF_FRX` return value in
			 * `uix` temporarily (just while calling this
			 * function).
			 */
			ui->uix->frx	= HNEF_FRX_SUCCESS;
			fr	= hnef_computer_move(ui,
					ui,	/* data */
					hnef_callback_xlib_interrupt,
					hnef_callback_xlib_progress,
					& success);
			* frx		= ui->uix->frx;
			ui->uix->frx	= HNEF_FRX_SUCCESS;

			if	(!hnef_fr_good(fr)
			||	!hnef_frx_good(* frx))
			{
				goto	RETURN_FR;
			}

			fr	= hnef_uix_paint_console_in
				(ui, frx, HNEF_FALSE);
			if	(!hnef_fr_good(fr)
			||	!hnef_frx_good(* frx))
			{
				goto	RETURN_FR;
			}

			fr	= hnef_uix_paint_progress
				(ui, frx, HNEF_TRUE);
			if	(!hnef_fr_good(fr)
			||	!hnef_frx_good(* frx))
			{
				goto	RETURN_FR;
			}

			if	(success)
			{
				unsigned short	ignored =
						HNEF_PLAYER_UNINIT;

				hnef_uix_marker_clear_all(ui);
				fr	= hnef_uix_paint_board
					(ui, frx, HNEF_FALSE);
				if	(!hnef_fr_good(fr)
				||	!hnef_frx_good(* frx))
				{
					goto	RETURN_FR;
				}

				if (hnef_game_over(ui->game, & ignored))
				{
					fr	= hnef_uix_print_langkey
						(ui, frx,
						HNEF_LK_CMD_GAME_OVER,
							HNEF_FALSE);
					if	(!hnef_fr_good(fr)
					||	!hnef_frx_good(* frx))
					{
						goto	RETURN_FR;
					}
				}
			}
		}
#endif	/* HNEFATAFL_UI_AIM */
	}

	RETURN_FR:

	hnef_uix_window_hide(ui->uix);

	return	fr;
}

#endif	/* HNEFATAFL_UI_XLIB */

