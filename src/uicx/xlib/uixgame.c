/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifdef	HNEFATAFL_UI_XLIB

#include <assert.h>	/* assert */

#include "cliio.h"	/* hnef_print_* */
#include "langkeyt.h"	/* HNEF_LK_* */
#include "uixconsole.h"	/* hnef_uix_print* */
#include "uixgame.h"
#include "uixtheme.h"	/* hnef_uix_theme* */
#include "uixutil.h"	/* hnef_uix_window_update */

/*
 * Called after loading a ruleset. `ui->game` must be valid.
 */
enum HNEF_FR
hnef_uix_game_update_rules (
	struct hnef_ui	* const HNEF_RSTR ui,
	const char	* const HNEF_RSTR ui_theme,
	enum HNEF_FRX	* const HNEF_RSTR frx
	)
{
	enum HNEF_FR	fr	= HNEF_FR_SUCCESS;
	HNEF_BOOL	success;

	assert	(NULL != ui);
	assert	(NULL != ui->game);
/*@i2@*/\
	assert	(hnef_rvalid_good(hnef_game_valid(ui->game)));
	assert	(NULL != ui->uix);
	assert	(NULL != frx);

	/*
	 * Success.
	 */
	fr	= hnef_uix_themes_load_rules
		(ui->uix, ui->game, ui_theme, frx, & success);
	if	(HNEF_FR_FAIL_OTHER == fr)
	{
		assert	(!hnef_frx_good(* frx));

		if	(HNEF_FRX_FAIL_XPM_READ == * frx)
		{
			const enum HNEF_FRX tmp = * frx;
			fr	= hnef_uix_print_fail_frx(ui, frx, tmp);
			if	(!hnef_fr_good(fr)
			||	!hnef_frx_good(* frx))
			{
				return	fr;
			}
			fr	= HNEF_FR_SUCCESS;
			* frx	= HNEF_FRX_SUCCESS;
		}
		else
		{
			return	fr;
		}
	}
	else if	(!hnef_fr_good(fr))
	{
		assert	(hnef_frx_good(* frx));
		return	fr;
	}
/*@i1@*/\
	else if	(!success)
	{
		fr	= hnef_uix_print_langkey(ui, frx,
			HNEF_LK_XLIB_THEMES_NOT_FOUND, HNEF_TRUE);
		if	(!hnef_fr_good(fr)
		||	!hnef_frx_good(* frx))
		{
			return	fr;
		}
	}

	/*
	 * NOTE:	Non-fatal failure, just print it.
	 */
	* frx	= hnef_uix_window_update(ui);
	if	(!hnef_frx_good(* frx))
	{
		(void)hnef_print_fail_frx(ui->lang, * frx, stderr);
		* frx	= HNEF_FRX_SUCCESS;
	}

	hnef_uix_marker_clear_all(ui);

	assert	(hnef_fr_good(fr));
	return	HNEF_FR_SUCCESS;
}

#endif	/* HNEFATAFL_UI_XLIB */

