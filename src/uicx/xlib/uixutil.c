/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifdef	HNEFATAFL_UI_XLIB

#include <assert.h>	/* assert */

#include "X11/Xutil.h"	/* XTextProperty */

#include "gleipnir_line.h"	/* gleip_line_* */

#include "uixthemet.h"	/* hnef_uix_themes */
#include "uixutil.h"
#include "uixutilt.h"	/* HNEF_UIX_FLAG_UNIT_* */

/*@unchecked@*/
static
const int	HNEF_BOARD_SIZE_WIDTH_MAX	= 400,
		HNEF_BOARD_SIZE_HEIGHT_MAX	= 400,
		HNEF_UIX_FLAG_UNIT_DEF		= 20;

/*
 * Also clears paintmap for deselected position if the game is valid.
 */
void
hnef_uix_marker_clear_select (
	struct hnef_ui	* const HNEF_RSTR ui
	)
{
	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->uix);

	if	(hnef_rvalid_good(hnef_game_valid(ui->game)))
	{
		assert	(NULL != ui->uix->paintmap);
		assert	(ui->uix->paintmap_width
			== ui->game->rules->bwidth);
		assert	(ui->uix->paintmap_height
			== ui->game->rules->bheight);

		if	(HNEF_BOARDPOS_NONE != ui->uix->pos_select)
		{
			ui->uix->paintmap[ui->uix->pos_select] =
					HNEF_BIT_U8_REPAINT;
		}
	}

	ui->uix->pos_select	= HNEF_BOARDPOS_NONE;
}

/*
 * Also clears paintmap for movelist if the game is valid.
 */
void
hnef_uix_marker_clear_movelist (
	struct hnef_ui	* const HNEF_RSTR ui
	)
{
	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->uix);

	if	(hnef_rvalid_good(hnef_game_valid(ui->game)))
	{
		size_t	i;

		assert	(NULL != ui->uix->paintmap);
		assert	(ui->uix->paintmap_width
			== ui->game->rules->bwidth);
		assert	(ui->uix->paintmap_height
			== ui->game->rules->bheight);

		for	(i = 0; i < ui->opt_movelist->elemc; ++i)
		{
			struct hnef_move	* const HNEF_RSTR move =
					& ui->opt_movelist->elems[i];
			ui->uix->paintmap[move->pos] =
				HNEF_BIT_U8_REPAINT;
			ui->uix->paintmap[move->dest] =
				HNEF_BIT_U8_REPAINT;
		}
	}

	hnef_listm_clear(ui->opt_movelist);
}

/*
 * Clears the paintmap between `pos` and `dest` in `move` (inclusive).
 */
static
void
hnef_uix_paintmap_clear_between (
/*@in@*/
/*@notnull@*/
	struct hnef_ui		* const HNEF_RSTR ui,
/*@in@*/
/*@notnull@*/
	const struct hnef_moveh	* const HNEF_RSTR move
	)
/*@modifies * ui@*/
{
	int		x_mod,
			y_mod;
	unsigned short	x_pos,
			y_pos,
			x_dest,
			y_dest,
			x,
			y;

	assert	(NULL != ui);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->game);
/*@i2@*/\
	assert	(hnef_rvalid_good(hnef_game_valid(ui->game)));
	assert	(NULL != ui->uix->paintmap);
	assert	(ui->uix->paintmap_width == ui->game->rules->bwidth);
	assert	(ui->uix->paintmap_height == ui->game->rules->bheight);

	x_pos	= move->pos % ui->game->rules->bwidth;
	y_pos	= move->pos / ui->game->rules->bwidth;
	x_dest	= move->dest % ui->game->rules->bwidth;
	y_dest	= move->dest / ui->game->rules->bwidth;

	x_mod	= x_pos == x_dest
		? 0
		:	(x_pos > x_dest
			? -1
			: 1);
	y_mod	= y_pos == y_dest
		? 0
		:	(y_pos > y_dest
			? -1
			: 1);

	if	(0 != x_mod)
	{
		assert	(y_pos == y_dest);
		y	= y_pos;
		for	(x = x_pos; x != x_dest;
			x = (unsigned short)(x + x_mod))
		{
			unsigned short	clear = (unsigned short)
				(x + y * ui->game->rules->bwidth);
			assert	(x < ui->game->rules->bwidth);
			assert	(y < ui->game->rules->bheight);
			assert	(clear < ui->game->rules->opt_blen);
			ui->uix->paintmap[clear] = HNEF_BIT_U8_REPAINT;
		}
	}
	else
	{
		assert	(x_pos == x_dest);
		x	= x_pos;
		for	(y = y_pos; y != y_dest;
			y = (unsigned short)(y + y_mod))
		{
			unsigned short	clear = (unsigned short)
				(x + y * ui->game->rules->bwidth);
			assert	(x < ui->game->rules->bwidth);
			assert	(y < ui->game->rules->bheight);
			assert	(clear < ui->game->rules->opt_blen);
			ui->uix->paintmap[clear] = HNEF_BIT_U8_REPAINT;
		}
	}
	ui->uix->paintmap[move->pos]	= HNEF_BIT_U8_REPAINT;
	ui->uix->paintmap[move->dest]	= HNEF_BIT_U8_REPAINT;
}

void
hnef_uix_marker_clear_last (
	struct hnef_ui	* const HNEF_RSTR ui
	)
{
	assert	(NULL != ui);
	assert	(NULL != ui->uix);

	if	(hnef_rvalid_good(hnef_game_valid(ui->game)))
	{
		assert	(NULL != ui->uix->paintmap);
		assert	(ui->uix->paintmap_width
			== ui->game->rules->bwidth);
		assert	(ui->uix->paintmap_height
			== ui->game->rules->bheight);

		if	(ui->game->movehist->elemc > 0)
		{
			struct hnef_moveh * const HNEF_RSTR last =
					& ui->game->movehist->elems
					[ui->game->movehist->elemc - 1];
			hnef_uix_paintmap_clear_between(ui, last);
		}

		if	(ui->game->movehist->elemc > (size_t)1)
		{
			struct hnef_moveh * const HNEF_RSTR lastlast =
					& ui->game->movehist->elems
					[ui->game->movehist->elemc - 2];
			hnef_uix_paintmap_clear_between(ui, lastlast);
		}
	}
}

/*
 * If the game is valid, this function also clears the paintmap for the
 * last move and the move before the last move.
 */
void
hnef_uix_marker_clear_all (
	struct hnef_ui	* const HNEF_RSTR ui
	)
{
	assert	(NULL != ui);
	assert	(NULL != ui->uix);

	hnef_uix_marker_clear_select	(ui);
	hnef_uix_marker_clear_movelist	(ui);
	hnef_uix_marker_clear_last	(ui);
}

/*
 * Clears the paintmap if it has been allocated. Else does nothing. This
 * makes `hnef_uix_paint()` repaint the whole board on the next call.
 */
void
hnef_uix_paintmap_clear (
	struct hnef_uix	* const HNEF_RSTR uix
	)
{
	unsigned short	blen,
			i;

	assert	(NULL != uix);

	if	(NULL == uix->paintmap)
	{
		assert	(HNEF_BOARDPOS_NONE == uix->paintmap_width
		&&	HNEF_BOARDPOS_NONE == uix->paintmap_height);
		return;
	}

	blen	= (unsigned short)
		(uix->paintmap_width * uix->paintmap_height);

	for	(i = 0; i < blen; ++i)
	{
		uix->paintmap[i]	= HNEF_BIT_U8_REPAINT;
	}
}

int
hnef_uix_margin_bottom (
	const struct hnef_uix	* const HNEF_RSTR uix
	)
{
	assert	(NULL != uix);
	assert	(NULL != uix->font);

	return	uix->progress_height
		+	(uix->font->ascent
			+ uix->font->descent)
			* 2;
}

static
int
hnef_uix_square_size_fallback (
/*@in@*/
/*@notnull@*/
	const struct hnef_game	* const HNEF_RSTR game,
	const HNEF_BOOL		width
	)
/*@modifies nothing@*/
{
	int	max	= width
			? HNEF_BOARD_SIZE_WIDTH_MAX
			: HNEF_BOARD_SIZE_HEIGHT_MAX,
		count,
		size;

	assert	(NULL != game);
	assert	(NULL != game->board);	/* Implies valid */

	count	= (int)(width
		? game->rules->bwidth
		: game->rules->bheight);

	size	= max / count;
	if	(size < 1)
	{
		size	= 1;
	}

	return	size;
}

/*
 * This function *can* be called regardless if
 * `hnef_uix_themes_load_rules()` has been called. However, in order to
 * return the current theme's desired square width or height, then the
 * theme must have been loaded for the current ruleset using that
 * function (which also calculates the size).
 *
 * If you have not called `hnef_uix_themes_load_rules()` prior to
 * calling this function, then it will return a fallback size based on
 * the assumption that placeholder graphics will be painted.
 */
static
int
hnef_uix_square_size (
/*@in@*/
/*@notnull@*/
	const struct hnef_game		* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	const struct hnef_uix_themes	* const HNEF_RSTR themes,
	const HNEF_BOOL			width
	)
/*@modifies nothing@*/
{
	assert	(NULL != game);
	assert	(NULL != game->board);	/* Implies valid */
	assert	(NULL != themes);
	assert	(NULL != themes->themev);

	if	(themes->themec < (size_t)1
	||	SIZET_MAX == themes->themei_cur)
	{
		return	hnef_uix_square_size_fallback(game, width);
	}
	else
	{
		struct hnef_uix_theme	* HNEF_RSTR theme	= NULL;

		assert	(themes->themec > 0);
		assert	(themes->themei_cur < themes->themec);

		theme	= themes->themev[themes->themei_cur];

		assert	((	theme->square_width > 0
			&&	theme->square_height > 0)
		||	(	HNEF_THEME_SIZE_INVALID
					== theme->square_width
			&&	HNEF_THEME_SIZE_INVALID
					== theme->square_height));

		if	(theme->square_width < 1
		||	theme->square_height < 1)
		{
			return	hnef_uix_square_size_fallback
				(game, width);
		}
		else
		{
			assert	(theme->square_width > 0
			&&	theme->square_height > 0);
			return	width
				? theme->square_width
				: theme->square_height;
		}
	}
}

int
hnef_uix_square_size_w (
	const struct hnef_game		* const HNEF_RSTR game,
	const struct hnef_uix_themes	* const HNEF_RSTR themes
	)
{
	assert	(NULL != game);
	assert	(NULL != game->board);	/* Implies valid */
	assert	(NULL != themes);
	assert	(NULL != themes->themev);

	return	hnef_uix_square_size(game, themes, HNEF_TRUE);
}

int
hnef_uix_square_size_h (
	const struct hnef_game		* const HNEF_RSTR game,
	const struct hnef_uix_themes	* const HNEF_RSTR themes
	)
{
	assert	(NULL != game);
	assert	(NULL != game->board);	/* Implies valid */
	assert	(NULL != themes);
	assert	(NULL != themes->themev);

	return	hnef_uix_square_size(game, themes, HNEF_FALSE);
}

int
hnef_uix_board_w (
	struct hnef_game		* const HNEF_RSTR game,
	const struct hnef_uix_themes	* const HNEF_RSTR themes
	)
{
	assert	(NULL != game);
	assert	(NULL != themes);
	assert	(NULL != themes->themev);

	if	(hnef_rvalid_good(hnef_game_valid(game)))
	{
		const int sq_w = hnef_uix_square_size_w(game, themes);
		assert	(sq_w > 0);

		return	sq_w * game->rules->bwidth;
	}
	else
	{
		return	HNEF_UIX_FLAG_UNIT_DEF * HNEF_UIX_FLAG_UNITS_W;
	}
}

int
hnef_uix_board_h (
	struct hnef_game		* const HNEF_RSTR game,
	const struct hnef_uix_themes	* const HNEF_RSTR themes
	)
{
	assert	(NULL != game);
	assert	(NULL != themes);
	assert	(NULL != themes->themev);

	if	(hnef_rvalid_good(hnef_game_valid(game)))
	{
		const int sq_h = hnef_uix_square_size_h(game, themes);
		assert	(sq_h > 0);

		return	sq_h * game->rules->bheight;
	}
	else
	{
		return	HNEF_UIX_FLAG_UNIT_DEF * HNEF_UIX_FLAG_UNITS_H;
	}
}

/*
 * Returns preferred size, not actual size (the user can resize the
 * window, in which case this function will not return the actual size).
 */
static
int
hnef_uix_window_pref_w (
/*@in@*/
/*@notnull@*/
	struct hnef_game		* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	const struct hnef_uix_themes	* const HNEF_RSTR themes
	)
/*@modifies * game@*/
{
	assert	(NULL != game);
	assert	(NULL != themes);
	assert	(NULL != themes->themev);

	return	hnef_uix_board_w(game, themes);
}

/*
 * Like `hnef_uix_window_pref_h()` but returns height.
 */
static
int
hnef_uix_window_pref_h (
/*@in@*/
/*@notnull@*/
	const struct hnef_ui	* const HNEF_RSTR ui
	)
/*@modifies * ui@*/
{
	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->themes);
	assert	(NULL != ui->uix->themes->themev);

	return	hnef_uix_board_h(ui->game, ui->uix->themes)
		+ hnef_uix_margin_bottom(ui->uix);
}

/*
 * Sets actual window size.
 *
 * `changed` is set to true if the size was updated. Else it's set to
 * false. If you don't care, you can pass a `NULL` parameter.
 */
enum HNEF_FRX
hnef_uix_window_size_update (
	struct hnef_ui	* const HNEF_RSTR ui,
	HNEF_BOOL	* const HNEF_RSTR changed
	)
{
	struct hnef_uix	* HNEF_RSTR uix	= NULL;

	assert	(NULL != ui);

	uix	= ui->uix;

	assert	(NULL != uix);
	assert	(NULL != uix->display);

	if	(NULL != changed)
	{
		* changed	= HNEF_FALSE;
	}

/*@i@*/	if	(!XGetWindowAttributes(uix->display, uix->window,
		& uix->window_atts))
	{
		return	HNEF_FRX_FAIL_WINDOW_ATTS;
	}

	if	(HNEF_UIX_SIZE_UNINIT	== uix->window_width
	||	HNEF_UIX_SIZE_UNINIT	== uix->window_height
	||	uix->window_atts.width	!= uix->window_width
	||	uix->window_atts.height	!= uix->window_height)
	{
		/*
		 * This game may or may not be valid.
		 */
		struct hnef_game * const HNEF_RSTR game	= ui->game;
		int	board_w	= hnef_uix_board_w(game, uix->themes),
			board_h	= hnef_uix_board_h(game, uix->themes);

		uix->window_width	= uix->window_atts.width;
		uix->window_height	= uix->window_atts.height;
		uix->opt_offset_x	= (uix->window_width - board_w)
					/ 2;
		uix->opt_offset_y	= (uix->window_height - board_h
					- hnef_uix_margin_bottom(uix))
					/ 2;

		if	(NULL != changed)
		{
			* changed	= HNEF_TRUE;
		}
	}
	return	HNEF_FRX_SUCCESS;
}

/*
 * `uix` must have been initialized by `hnef_uix_init()`.
 */
void
hnef_uix_window_hide (
	struct hnef_uix	* const HNEF_RSTR uix
	)
{
	assert	(NULL != uix);
	assert	(NULL != uix->display);

/*@i@*/	XSelectInput(uix->display, uix->window, NoEventMask);

/*@i@*/	XSelectInput(uix->display, uix->window, StructureNotifyMask);

/*@i@*/	XUnmapWindow(uix->display, uix->window);

	do
	{
/*@i@*/		XNextEvent(uix->display, & uix->event);
	}
/*@i@*/	while	(UnmapNotify != uix->event.type);

/*@i@*/	XSelectInput(uix->display, uix->window, NoEventMask);
}

/*
 * The failure returned by this function is non-fatal and can be
 * ignored.
 */
static
enum HNEF_FRX
hnef_uix_window_name_set (
/*@in@*/
/*@notnull@*/
	struct hnef_uix		* const HNEF_RSTR uix,
/*@in@*/
/*@notnull@*/
	struct hnef_game	* const HNEF_RSTR game
	)
/*@modifies * uix, * game@*/
{
	/*
	 * Passing `& name_placeholder` to `XStringListToTextProperty`
	 * causes a segfault. So we have to go through this ordeal.
	 *
	 * NOTE:	"HNEFATAFL" is hard-coded as the placeholder
	 *		window name until a ruleset is loaded.
	 */
	char		name_placeholder[]	= "HNEFATAFL";
	char		* namearg		= NULL;
	XTextProperty	textprop;

	assert	(!hnef_line_empty(name_placeholder));
	assert	(NULL != uix);
	assert	(NULL != uix->display);
	assert	(NULL != game);

	if	(hnef_rvalid_good(hnef_game_valid(game)))
	{
		/*
		 * `name` is mandatory in ruleset (see `rvalid.c`).
		 */
		assert	(NULL != game->rules->name);
		assert	(!hnef_line_empty(game->rules->name));
		namearg	= game->rules->name;
	}
	else
	{
		namearg	= name_placeholder;
	}
	assert	(!hnef_line_empty(namearg));

	if	(uix->font->use_fontset)
	{
		/*
		 * We *could* check if this function will return
		 * `XLocaleNotSupported` (as opposed to `Success`) by
		 * calling `XSupportsLocale()` before calling, but
		 * what's the point? It will just return `!Success`.
		 */
/*@i@*/		if	(Success != XmbTextListToTextProperty
/*@i@*/				(uix->display, & namearg, 1, XTextStyle,
/*@i@*/				& textprop))
		{
			return	HNEF_FRX_FAIL_TEXTPROP;
		}
/*@i@*/		XSetWMName	(uix->display, uix->window, & textprop);
/*@i@*/		XSetWMIconName	(uix->display, uix->window, & textprop);
/*@i@*/		(void)XFree	(textprop.value);
	}
	else
	{
/*@i@*/		if	(!XStringListToTextProperty
/*@i@*/					(& namearg, 1, & textprop))
		{
			return	HNEF_FRX_FAIL_TEXTPROP;
		}
/*@i@*/		XSetWMName	(uix->display, uix->window, & textprop);
/*@i@*/		XSetWMIconName	(uix->display, uix->window, & textprop);
/*@i@*/		(void)XFree	(textprop.value);
	}

/*@i1@*/\
	return	HNEF_FRX_SUCCESS;
/*@i1@*/\
}

/*
 * The game does not have to be valid.
 *
 * The failure returned by this function is non-fatal and can be
 * ignored.
 */
enum HNEF_FRX
hnef_uix_window_update (
	struct hnef_ui	* const HNEF_RSTR ui
	)
{
	enum HNEF_FRX	frx	= HNEF_FRX_SUCCESS;
	unsigned int	w,
			h;

	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);

	frx	= hnef_uix_window_name_set(ui->uix, ui->game);

	w = (unsigned int)hnef_uix_window_pref_w(ui->game,
						ui->uix->themes);
	h = (unsigned int)hnef_uix_window_pref_h(ui);

/*@i@*/	XResizeWindow(ui->uix->display, ui->uix->window, w, h);

	return	frx;
}

/*
 * `uix` must have been initialized by `hnef_uix_init()`.
 */
enum HNEF_FRX
hnef_uix_window_show (
	struct hnef_ui	* const HNEF_RSTR ui
	)
{
	assert	(NULL != ui);
	assert	(NULL != ui->game);
	assert	(NULL != ui->uix);
	assert	(NULL != ui->uix->display);

/*@i@*/	XMapWindow(ui->uix->display, ui->uix->window);

/*@i@*/	XSelectInput(ui->uix->display, ui->uix->window,
		StructureNotifyMask);

	do
	{
/*@i@*/		XNextEvent(ui->uix->display, & ui->uix->event);
	}
/*@i@*/	while	(MapNotify != ui->uix->event.type);

/*@i@*/	XSelectInput(ui->uix->display, ui->uix->window, NoEventMask);

/*@i@*/	XSelectInput(ui->uix->display, ui->uix->window,
		(long)	((unsigned long)ButtonPressMask
			| (unsigned long)ExposureMask
			| (unsigned long)KeyPressMask));

	return	hnef_uix_window_size_update(ui, NULL);
}

void
hnef_uix_gc_conf (
	const struct hnef_uix	* const HNEF_RSTR uix,
	GC			gc
	)
{
	assert	(NULL != uix);
	assert	(NULL != uix->display);
	assert	(NULL != gc);

/*@i@*/	XSetForeground(uix->display, gc, uix->px_white);
/*@i@*/	XSetBackground(uix->display, gc, uix->px_black);
}

#endif	/* HNEFATAFL_UI_XLIB */

