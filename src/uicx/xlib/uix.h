/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifdef	HNEFATAFL_UI_XLIB

#ifndef HNEF_UI_UIX_H
#define HNEF_UI_UIX_H

#include "gleipnir_lang.h"	/* gleip_lang */

#include "hnefatafl.h"

#include "funcxt.h"	/* HNEF_FRX */
#include "uixt.h"	/* hnef_uix */

/*@-protoparamname@*/
/*@in@*/
/*@null@*/
/*@only@*/
extern
struct hnef_uix *
hnef_alloc_uix_init (
/*@in@*/
/*@notnull@*/
	const struct gleip_lang	* const GLEIP_RSTR lang,
/*@out@*/
/*@notnull@*/
	enum HNEF_FRX		* const HNEF_RSTR frx,
/*@in@*/
/*@null@*/
	const char		* const HNEF_RSTR setlocale_return
	)
/*@globals fileSystem, stderr@*/
/*@modifies fileSystem, stderr, * frx@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
void
hnef_free_uix_fini (
/*@in@*/
/*@notnull@*/
/*@owned@*/
/*@special@*/
	struct hnef_uix	* const HNEF_RSTR uix
	)
/*@modifies uix@*/
/*@releases uix@*/
;
/*@=protoparamname@*/

#endif

#endif	/* HNEFATAFL_UI_XLIB */

