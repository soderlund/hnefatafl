/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <limits.h>	/* USHRT_MAX */

#include "types.h"	/* HNEF_TYPEC_* */

/*
 * Max, min and default values for type_square/type_piece.capt_sides.
 */
const unsigned short	HNEF_TPIECE_CAPT_SIDES_MAX = (unsigned short)4,
			HNEF_TPIECE_CAPT_SIDES_MIN = (unsigned short)1,
			HNEF_TPIECE_CAPT_SIDES_DEF = (unsigned short)2;

/*
 * Returns index based on bit.
 *
 * The bit must be a single unsigned 1 byte bit that's not 0, id est
 * 1, 2, 4 ... 128. Else HNEF_TYPEC_INDEX_INVALID is returned -- and
 * this index does indeed point to a type_piece/square in ruleset: the
 * invalid piece/square type.
 *
 * This could be done by exploiting the fact that 2^index = bit, and
 * thus log(bit) / log(2) = index, but I think this is faster.
 */
unsigned short
hnef_type_index_get (
	const HNEF_BIT_U8	bit
	)
{
	switch (bit)
	{
		case 1:
			return (unsigned short)0;
		case 2:
			return (unsigned short)1;
		case 4:
			return (unsigned short)2;
		case 8:
			return (unsigned short)3;
		case 16:
			return (unsigned short)4;
		case 32:
			return (unsigned short)5;
		case 64:
			return (unsigned short)6;
		case 128:
			return (unsigned short)7;
		default:
			return HNEF_TYPE_INDEX_INVALID;
	}
}

