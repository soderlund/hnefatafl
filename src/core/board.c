/*
 * Copyright © 2013-2014 Alexander Söderlund (Sweden),
 * 2013-2014 Alexander Dolgunin (Russia)
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */
#include <stdlib.h>	/* malloc, free */
#include <string.h>	/* memset */

#include "board.h"
#include "func.h"		/* hnef_fr_* */
#include "listm.h"		/* hnef_listm_add */
#include "listmh.h"		/* hnef_listmh_add */
#include "num.h"		/* hnef_min* */
#include "type_piece.h"		/* hnef_type_piece_get */
#include "type_square.h"	/* hnef_type_square_get */

#ifdef HNEFATAFL_ZHASH
#include "types.h"		/* hnef_type_index_get */
#endif

/*
 * All functions in this source file requires that the game is valid.
 */

/*
 * Copies src into dest.
 *
 * NOTE:	This function is called lots of times by aimalgo.c. It
 *		should be as fast as possible.
 */
void
hnef_board_copy (
	const struct hnef_board	* const HNEF_RSTR src,
	struct hnef_board	* const HNEF_RSTR dest,
	const unsigned short	blen
	)
{
	assert	(NULL != src);
	assert	(NULL != dest);

	memcpy(dest->pieces, src->pieces, (size_t)blen);
	dest->turn	= src->turn;
	dest->opt_captloss_piece_lastseen =
		src->opt_captloss_piece_lastseen;
}

/*
 * Advances the turn in b.
 *
 * For efficiency reasons, this ignores game->playerc, and instead looks
 * at HNEF_PLAYERS_MAX, abusing the fact that they must be equal.
 * Otherwise this function would need a struct hnef_game parameter for
 * game->playerc.
 */
static
void
hnef_board_turn_adv (
/*@in@*/
/*@notnull@*/
	struct hnef_board	* const HNEF_RSTR board
	)
/*@modifies board->turn@*/
{
	assert	(NULL != board);
	if	(++board->turn >= HNEF_PLAYERS_MAX)
	{
		board->turn	= 0;
	}
}

#ifdef HNEFATAFL_ZHASH

/*
 * Flags a piece in hashkey and hashlock as captured.
 */
static
void
hnef_board_hash_xor (
/*@in@*/
/*@notnull@*/
	const struct hnef_zhashtable	* const HNEF_RSTR hashtable,
/*@in@*/
/*@notnull@*/
	unsigned int			* const HNEF_RSTR hashkey,
/*@in@*/
/*@notnull@*/
	unsigned int			* const HNEF_RSTR hashlock,
	const unsigned short		piece_index,
	const unsigned short		pos
	)
/*@modifies * hashkey, * hashlock@*/
{
	assert	(NULL != hashtable);
	assert	(NULL != hashkey);
	assert	(NULL != hashlock);

	* hashkey ^= hashtable->zobrist_key	[piece_index][pos];
	* hashlock ^= hashtable->zobrist_lock	[piece_index][pos];
}

#endif /* HNEFATAFL_ZHASH */

/*
 * Checks if pt_adj (at b->pieces[adj]) is captured by an enemy piece
 * (whose type_piece is irrelevant) moving to dest. Returns HNEF_TRUE if
 * so.
 *
 * This function assumes:
 *
 * *	pb_mov (the moving piece, who moved to dest) is a non-empty
 *	piece on the board, that can (potentially) capture pt_adj.
 *
 * *	pb_adj is a non-empty piece that can be captured by pb_mov.
 *
 * If that does not hold true, then this function may not be called.
 *
 * skipx and skipy are used to determine double-trapping. If you are not
 * checking a double trap, both should be 0. If you are checking a
 * double trap, then skipx should be -1 if the piece adjacent to pt_adj
 * (which may be surrounded by the double trap) is to the left of
 * pt_adj; +1 if it's to the right, and so on. This simply tells that
 * this function to hop one step further when checking in that
 * direction.
 *
 *	. . m .	// When checking if x is captured, you can give -1 skipx
 *	n y x m	// to move one more step in the x-axis to the left when
 *	. . m . // determining if x is surrounded (thus checking if x is
 *		// surrounded by m and n).
 *
 * skipx and skipy will maintain custodial capture rules, even when
 * hopping over several pieces.
 *
 * If either skipx or skipy is not 0, then this function may adapt
 * capt_sides and custodial (of pt_adj) to double trap rules. If both
 * are 0, this is not done.
 *
 * When using double capture rules, the following always happens (other
 * than maybe using other capt_sides and custodial rules):
 *
 * *	Lax custodial capture requirement is used. Normally the moving
 *	piece has to be the piece that surrounds custodially, but in
 *	this case it's enough that the piece has been surrounded
 *	custodially by any two pieces (not necessarily the moving one).
 *	The normal checking can be called "strict" custodial checking.
 *
 * (skipx and skipy can simply be negative, positive or 0. It doesn't
 * distinguish between -100 and -1 -- they are both treated as -1. Thus
 * you can only "hop" over one piece using skipx / skipy.)
 *
 * If a non-empty piece or square type that is on the board is not
 * defined in ruleset, then it always returns HNEF_FALSE, without giving
 * an indication of failure.
 */
static
HNEF_BOOL
hnef_board_is_capt (
/*@in@*/
/*@notnull@*/
	const struct hnef_game		* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	const struct hnef_board		* const HNEF_RSTR board,
	const unsigned short		dest,
/*@in@*/
/*@notnull@*/
	const struct hnef_type_piece	* const HNEF_RSTR pt_adj,
	const unsigned short		adj,
	const int			skipx,
	const int			skipy
	)
/*@modifies nothing@*/
{
	HNEF_BIT_U8	pb_adj;
	unsigned short	adjx,
			adjy,
			capt_sides,
			offbx,
			offby,
			surrxaxis,
			surryaxis;
	int		i;
	HNEF_BOOL	custodial,
			cust_strict,
			capt_edge,
			dtrap;
	struct hnef_ruleset	* HNEF_RSTR rules = NULL;

	dtrap	= skipx != 0
		|| skipy != 0;

	assert	(NULL != game);
	assert	(NULL != board);
	assert	(NULL != pt_adj);

	rules	= game->rules;
	assert	(NULL != rules->pieces);
	assert	(NULL != rules->squares);
	assert	(dest	< rules->opt_blen);
	assert	(adj	< rules->opt_blen);

	if	(dtrap)
	{
		/*
		 * Use double trap rules (and lax custodial checking).
		 */
		capt_edge	= pt_adj->dbl_trap_edge;
		capt_sides	= (unsigned short)(pt_adj->dbl_trap_encl
				? (unsigned short)4
				: pt_adj->capt_sides);
		custodial	= pt_adj->dbl_trap_encl
				? HNEF_FALSE
				: pt_adj->custodial;
		cust_strict	= HNEF_FALSE;
	}
	else
	{
		/*
		 * Do not use double trap rules (and strict custodial
		 * checking).
		 */
		capt_edge	= pt_adj->capt_edge;
		capt_sides	= pt_adj->capt_sides;
		custodial	= pt_adj->custodial;
		cust_strict	= HNEF_TRUE;
	}
	pb_adj	= board->pieces[adj];

	if	(!(	dtrap
		&&	pt_adj->dbl_trap_encl))
	{
		/*
		 * Don't override if double trapping and dbl_trap_encl
		 * makes the piece have to be surrounded.
		 *
		 * Note that if the square is among opt_ts_capt_sides,
		 * then the square type does override capt_sides and
		 * indicates a valid amount of sides.
		 */
		const HNEF_BIT_U8	sb_adj = rules->squares[adj];

		if	(((unsigned int)sb_adj
			& (unsigned int)rules->opt_ts_capt_sides)
			== (unsigned int)sb_adj)
		{
			const struct hnef_type_square
				* const HNEF_RSTR ts =
				hnef_type_square_get(rules, sb_adj);

			if	(((unsigned int)pb_adj
				& (unsigned int)ts->capt_sides_pieces)
				== (unsigned int)pb_adj)
			{
				/*
				 * `ts->capt_sides` only applies if
				 * `pb_adj` is among
				 * `ts->capt_sides_pieces`.
				 */
				capt_sides = ts->capt_sides;
				if	(capt_sides >= (unsigned short)4
				||	capt_sides < (unsigned short)2)
				{
					custodial	= HNEF_FALSE;
				}
			}
		}
	}
	adjx	= adj % rules->bwidth;
	adjy	= adj / rules->bwidth;

	/*
	 * Note that custodial is not dropped by setting custodial to
	 * false if it's not possible due do a custodial capture due to
	 * off-board squares. This particular situation is checked
	 * later, without altering the value of custodial.
	 *
	 * If a square overrides capt_sides to 1 or 4, however, then
	 * custodial is set to false.
	 */

	/*
	 * Amount of squares adjacent to adjx, adjy that are off-board.
	 * HNEF_BIT_U8_EMPTY are added to these counts later.
	 */
	offbx	= offby	= 0;
	if	(adjx < (unsigned short)1
	||	adjx >= rules->bwidth - 1)
	{
		offbx++;
	}
	if	(adjy < (unsigned short)1
	||	adjy >= rules->bheight - 1)
	{
		offby++;
	}

	/*
	 * Amount of pieces that have surrounded pt_adj in x and
	 * y-axises. Used to determine custodial capture.
	 */
	surrxaxis	= surryaxis	= 0;

	/*
	 * Check every piece adjacent to pt_adj (on adjx, adjy).
	 */
	for	(i = 0; i < 4; ++i)
	{
		const HNEF_BOOL	axisx	= i < 2;
		unsigned short	adjadjx	= adjx;
		unsigned short	adjadjy	= adjy;
		unsigned short	adjadj;
		unsigned short	empty_adj;
		unsigned short	surr_tot;
		HNEF_BIT_U8	pb_adjadj,
				sb_adjadj;
		switch	(i)
		{
			case 0:
				adjadjx = (unsigned short)
					(adjadjx + (skipx > 0 ? 2 : 1));
/*@i1@*/			break;
			case 1:
				adjadjx = (unsigned short)
					(adjadjx - (skipx < 0 ? 2 : 1));
/*@i1@*/			break;
			case 2:
				adjadjy = (unsigned short)
					(adjadjy + (skipy > 0 ? 2 : 1));
/*@i1@*/			break;
			default:
				adjadjy = (unsigned short)
					(adjadjy - (skipy < 0 ? 2 : 1));
/*@i1@*/			break;
		}

		/*
		 * Note that adjadjx and adjadjy will roll over instead
		 * of becoming negative, thus exceeding g->rules->bwidth
		 * or bheight.
		 */
		if	(adjadjx >= rules->bwidth
		||	adjadjy >= rules->bheight)
		{
			continue;
		}

		adjadj		= (unsigned short)
				(adjadjy * rules->bwidth + adjadjx);
		sb_adjadj	= rules->squares	[adjadj];
		pb_adjadj	= board->pieces		[adjadj];
		if	(HNEF_BIT_U8_EMPTY == sb_adjadj)
		{
			switch	(axisx)
			{
				case HNEF_TRUE:
					offbx++;
/*@i1@*/				break;
				case HNEF_FALSE:
				default:
					offby++;
/*@i1@*/				break;
			}
		}
		else if	(dest == adjadj)
		{
			switch	(axisx)
			{
				case HNEF_TRUE:
					surrxaxis++;
/*@i1@*/				break;
				case HNEF_FALSE:
				default:
					surryaxis++;
/*@i1@*/				break;
			}
		}
		else if	(HNEF_BIT_U8_EMPTY != pb_adjadj)
		{
			/*
			 * Assumes pt_adjadj is a defined piece.
			 */
			const struct hnef_type_piece
				* const HNEF_RSTR pt_adjadj =
				hnef_type_piece_get(rules, pb_adjadj);

			assert	(HNEF_BIT_U8_EMPTY != pt_adjadj->bit);

			if	(hnef_type_piece_can_capture
					(pt_adjadj, pt_adj->bit))
			{
				switch	(axisx)
				{
					case HNEF_TRUE:
						surrxaxis++;
/*@i1@*/					break;
					case HNEF_FALSE:
					default:
						surryaxis++;
/*@i1@*/					break;
				}
			}
		}
		else	/* Empty on-board square. */
		{
			const struct hnef_type_square
				* const HNEF_RSTR st_adjadj =
				hnef_type_square_get(rules, sb_adjadj);

			assert	(HNEF_BIT_U8_EMPTY != st_adjadj->bit);

			if	(hnef_type_square_can_capture
						(st_adjadj, pb_adj))
			{
				switch	(axisx)
				{
					case HNEF_TRUE:
						surrxaxis++;
/*@i1@*/					break;
					case HNEF_FALSE:
					default:
						surryaxis++;
/*@i1@*/					break;
				}
			}
		}

		empty_adj = (unsigned short)(4 - (offbx + offby));
		if	(capt_edge)	/* Relax capt_sides? */
		{
			if	(capt_sides > empty_adj)
			{
				capt_sides = empty_adj;
			}
		}

		/*
		 * Note that custodial requirement is dropped if
		 * capt_edge, and [the piece is completely surrounded on
		 * all on-board adjacent squares, or custodial capture
		 * is impossible due to off-board squares].
		 */
		surr_tot = (unsigned short)(surrxaxis + surryaxis);
		if	(surr_tot >= capt_sides)
		{
			if	(custodial
			&&	!(	capt_edge
				&&	surr_tot >= empty_adj)
			&&	!(	capt_edge
				&&	offbx > 0
				&&	offby > 0))
			{
				/*
				 * Custodial capture applies. Is dest on
				 * the same row as adj (we already know
				 * that they are orthogonally adjacent)?
				 * If so, and if the piece is surrounded
				 * by 2 pieces along the row, then it's
				 * captured custodially.
				 */
				if	(cust_strict)
				{
					const HNEF_BOOL xaxisdest =
						dest % rules->bwidth
						!= adj % rules->bwidth;
					if	((surrxaxis >
						(unsigned short)1
							&& xaxisdest)
					||	(surryaxis >
						(unsigned short)1
							&& !xaxisdest))
					{
						return	HNEF_TRUE;
					}
				}
/*@i1@*/\
				else if	(surrxaxis > (unsigned short)1
				||	surryaxis > (unsigned short)1)
				{
					return	HNEF_TRUE;
				}
				/*
				 * Else: not captured custodially by
				 * strict or lax checking.
				 */
			}
			else
			{
				return	HNEF_TRUE;
			}
		}
	}
	return	HNEF_FALSE;
}

/*
 * Checks if pt_adj (at b->pieces[adj]) is captured by an enemy piece
 * (whose type_piece is irrelevant) moving to dest, according to double
 * trap capturing rules. Returns HNEF_TRUE if so.
 *
 * This function assumes:
 *
 * *	pb_mov (the moving piece, who moved to dest) is a non-empty
 *	piece on the board, that can (potentially) capture pt_adj.
 *
 * *	pb_adj is a non-empty piece that can be captured by pb_mov.
 *
 * If that does not hold true, then this function may not be called.
 *
 * The following is checked by this function, and does not have to be
 * checked in advance:
 *
 * *	If pt_adj is not double-trappable, then this funciton returns
 *	false immediately.
 *
 * Further, two pieces are not even considered for double trap if any of
 * the following conditions is true:
 *
 * *	The pieces are owned by different players.
 *
 * *	Either piece can not trigger double trap.
 *
 * *	None of the pieces are captured by a double trap.
 *
 * *	Both pieces are double trap complements (can't be trapped on
 *	their own).
 *
 * This function calls hnef_board_is_capt() with double trap parameters.
 * Thus lax custodial checking will be used (according to non-0
 * skipx/skipy parameters) and double trap settings may override
 * capt_sides and custodial.
 *
 * Returns HNEF_TRUE if adj is captured in double trap; false if not.
 */
static
HNEF_BOOL
hnef_board_do_dtrap (
/*@in@*/
/*@notnull@*/
	const struct hnef_game		* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	struct hnef_board		* const board,
	const unsigned short		dest,
/*@in@*/
/*@notnull@*/
	const struct hnef_type_piece	* const HNEF_RSTR pt_adj,
	const unsigned short		adj
#ifdef HNEFATAFL_ZHASH
	,
/*@in@*/
/*@null@*/
	const struct hnef_zhashtable	* const HNEF_RSTR hashtable,
/*@in@*/
/*@null@*/
	unsigned int			* const hashkey,
/*@in@*/
/*@null@*/
	unsigned int			* const hashlock
#endif
	)
/*@modifies * board@*/
#ifdef HNEFATAFL_ZHASH
/*@modifies * hashkey, * hashlock@*/
#endif
{
	/*
	 * pt_adj is the piece adjacent to the moving, hostile piece.
	 *
	 * pt_adjadj is the piece adjacent to pt_adj, which may be
	 * double-trapped with pt_adj.
	 */

	HNEF_BIT_U8	pb_adj,
			padj_opt_owned;
	unsigned short	adjx,
			adjy;
	int		i;
	struct hnef_ruleset	* HNEF_RSTR rules = NULL;

	assert	(NULL != game);
	assert	(NULL != board);
	assert	(NULL != pt_adj);

	rules	= game->rules;
	assert	(NULL != rules->pieces);
	assert	(NULL != rules->squares);
	assert	(dest	< rules->opt_blen);
	assert	(adj	< rules->opt_blen);

	if	(!pt_adj->dbl_trap)
	{
		return	HNEF_FALSE;
	}

	pb_adj		= board->pieces[adj];
	padj_opt_owned	= game->players[pt_adj->owner]->opt_owned;
	adjx		= adj % rules->bwidth;
	adjy		= adj / rules->bwidth;
	for	(i = 0; i < 4; ++i)
	{
		unsigned short	adjadjx	= adjx;
		unsigned short	adjadjy	= adjy;
		unsigned short	adjadj;
		HNEF_BIT_U8	pb_adjadj,
				sb_adjadj;
		int		skipx	= 0;
		int		skipy	= 0;
		switch	(i)
		{
			case 0:
				++skipx;
/*@i1@*/			break;
			case 1:
				--skipx;
/*@i1@*/			break;
			case 2:
				++skipy;
/*@i1@*/			break;
			default:
				--skipy;
/*@i1@*/			break;
		}
		adjadjx	= (unsigned short)(adjadjx + skipx);
		adjadjy	= (unsigned short)(adjadjy + skipy);

		/*
		 * Note that adjadjx and adjadjy will roll over instead
		 * of becoming negative, thus exceeding g->rules->bwidth
		 * or bheight.
		 */
		if	(adjadjx >= rules->bwidth
		||	adjadjy >= rules->bheight)
		{
			continue;
		}

		adjadj		= (unsigned short)
				(adjadjy * rules->bwidth + adjadjx);
		sb_adjadj	= rules->squares[adjadj];
		pb_adjadj	= board->pieces	[adjadj];
		if	(HNEF_BIT_U8_EMPTY == sb_adjadj)
		{
			continue;
		}
/*@i1@*/\
		else if (HNEF_BIT_U8_EMPTY == pb_adjadj
/*
 * pb_adj and pb_adjadj are owned by different players:
 */
		|| ((unsigned int)padj_opt_owned & (unsigned int)
			pb_adjadj) != (unsigned int)pb_adjadj
/*
 * pb_adjadj is not dbl_trap (we already know that pb_adj is):
 */
		|| ((unsigned int)rules->opt_tp_dbl_trap &
			(unsigned int)pb_adjadj) !=
			(unsigned int)pb_adjadj
/*
 * Neither piece is dbl_trap_capt:
 */
		|| (((unsigned int)rules->opt_tp_dbl_trap_capt &
			(unsigned int)pb_adj) != (unsigned int)pb_adj
			&& ((unsigned int)
			rules->opt_tp_dbl_trap_capt &
			(unsigned int)pb_adjadj) !=
			(unsigned int)pb_adjadj)
/*
 * Both pieces are dbl_trap_compl:
 */
		|| (((unsigned int)rules->opt_tp_dbl_trap_compl &
			(unsigned int)pb_adj) == (unsigned int)pb_adj
			&& ((unsigned int)
			rules->opt_tp_dbl_trap_compl &
			(unsigned int)pb_adjadj) ==
			(unsigned int)pb_adjadj))
		{
			continue;
		}

		{
			const struct hnef_type_piece
				* const HNEF_RSTR pt_adjadj =
				hnef_type_piece_get(rules, pb_adjadj);
			const HNEF_BIT_U8 sb_adj = rules->squares[adj];

			if	(((unsigned int)sb_adj
			& (unsigned int)pt_adj->dbl_trap_squares)
				!= (unsigned int)sb_adj
			||	((unsigned int)sb_adjadj
			& (unsigned int)pt_adjadj->dbl_trap_squares)
				!= (unsigned int)sb_adjadj)
			{
				/*
				 * Either piece is not on a double trap
				 * square.
				 */
				continue;
			}

			if	(hnef_board_is_capt(game, board, dest,
					pt_adj, adj, skipx, skipy)
			&&	hnef_board_is_capt(game, board, dest,
				pt_adjadj, adjadj, -skipx, -skipy))
			{
#ifdef HNEFATAFL_ZHASH
				if	(NULL != hashkey)
				{
					assert	(NULL != hashlock);
					assert	(NULL != hashtable);
					if	(pt_adj->dbl_trap_capt)
					{
						hnef_board_hash_xor
						(hashtable, hashkey,
							hashlock,
						hnef_type_index_get
							(pb_adj), adj);
					}
					if (pt_adjadj->dbl_trap_capt)
					{
						hnef_board_hash_xor
						(hashtable, hashkey,
						hashlock,
						hnef_type_index_get
						(pb_adjadj), adjadj);
					}
				}
#endif /* HNEFATAFL_ZHASH */
				if	(pt_adj->dbl_trap_capt)
				{
					board->pieces[adj] =
						HNEF_BIT_U8_EMPTY;
				}
				if	(pt_adjadj->dbl_trap_capt)
				{
					board->pieces[adjadj] =
						HNEF_BIT_U8_EMPTY;
				}
				return	HNEF_TRUE;
			}
		}
	}
	return	HNEF_FALSE;
}

/*
 * Checks if the piece at adj is captured (either normally or by double
 * trap) by pt_mov moving to dest, and if so, removes it from the board.
 *
 * Returns true if any piece was captured.
 *
 * hashtable may be NULL if hashkey also is NULL, as in
 * hnef_board_do_captures.
 *
 * Returns false if any parameter is invalid, with no other indication
 * of failure.
 *
 * NOTE:	Checking for double trap takes precedence over checking
 *		for ordinary captures. If a double trap triggers (and
 *		pt_adj is not captured, even though pt_adj triggers the
 *		double trap), then it doesn't even check if pt_adj is
 *		captured according to normal capturing rules.
 *
 *		It's up to the ruleset writer to make sure that this
 *		doesn't cause strange behavior in the game.
 */
static
HNEF_BOOL
hnef_board_do_capture (
/*@in@*/
/*@notnull@*/
	const struct hnef_game		* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	struct hnef_board		* const board,
/*@in@*/
/*@notnull@*/
	const struct hnef_type_piece	* const HNEF_RSTR pt_mov,
	const unsigned short		dest,
	const unsigned short		adj
#ifdef HNEFATAFL_ZHASH
	,
/*@in@*/
/*@null@*/
	const struct hnef_zhashtable	* const HNEF_RSTR hashtable,
/*@in@*/
/*@null@*/
	unsigned int			* const hashkey,
/*@in@*/
/*@null@*/
	unsigned int			* const hashlock
#endif
	)
/*@modifies * board@*/
#ifdef HNEFATAFL_ZHASH
/*@modifies * hashkey, * hashlock@*/
#endif
{
	HNEF_BIT_U8			pb_adj;
	const struct hnef_type_piece	* HNEF_RSTR pt_adj	= NULL;
	struct hnef_ruleset		* HNEF_RSTR rules	= NULL;

	assert	(NULL != game);
	assert	(NULL != board);
	assert	(NULL != pt_mov);

	rules	= game->rules;
	assert	(dest	< rules->opt_blen);
	assert	(adj	< rules->opt_blen);

	pb_adj	= board->pieces[adj];
	if	(HNEF_BIT_U8_EMPTY == pb_adj
	||	!hnef_type_piece_can_capture(pt_mov, pb_adj))
	{
		return	HNEF_FALSE;
	}

	pt_adj	= hnef_type_piece_get(rules, pb_adj);

	assert	(HNEF_BIT_U8_EMPTY != pt_adj->bit);

	/*
	 * If pt_mov can't capture pb_adj, then don't check double trap
	 * or ordinary capture.
	 */

	if	(hnef_board_do_dtrap(game, board, dest, pt_adj, adj
#ifdef HNEFATAFL_ZHASH
		, hashtable, hashkey, hashlock
#endif
		))
	{
		return	HNEF_TRUE;
	}

	if (hnef_board_is_capt(game, board, dest, pt_adj, adj, 0, 0))
	{
#ifdef HNEFATAFL_ZHASH
		if	(NULL != hashkey)
		{
			assert	(NULL != hashlock);
			assert	(NULL != hashtable);
			hnef_board_hash_xor(hashtable, hashkey,
			hashlock, hnef_type_index_get(pb_adj), adj);
		}
#endif
		board->pieces[adj]	= HNEF_BIT_U8_EMPTY;
		return			HNEF_TRUE;
	}
	else
	{
		return		HNEF_FALSE;
	}
}

/*
 * Checks if a piece that moved to dest results in pieces being
 * captured.
 *
 * Assumes that the moving piece at dest is owned by the player whose
 * turn it currently is according to b->turn. Otherwise this function
 * will fail to capture enemy pieces and instead capture friendly
 * pieces.
 *
 * This is a pretty slow function, but there's nothing that can be done
 * about that since we need to cover all possible cases based on ruleset
 * options.
 *
 * hashtable may be NULL if hashkey is also NULL, like in
 * hnef_board_move_unsafe.
 *
 * opt_pt_mov is the moving piece. It's given as a parameter as an
 * optimization because move_unsafe() has to retrieve it anyway, which
 * saves us one lookup.
 *
 * Returns true if some piece was captured.
 *
 * Returns false if some argument or state is invalid, with no other
 * indication of failure.
 */
static
HNEF_BOOL
hnef_board_do_captures (
/*@in@*/
/*@notnull@*/
	const struct hnef_game		* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	struct hnef_board		* const board,
/*@in@*/
/*@notnull@*/
	const struct hnef_type_piece	* const HNEF_RSTR opt_pt_mov,
	const unsigned short		dest
#ifdef HNEFATAFL_ZHASH
	,
/*@in@*/
/*@null@*/
	const struct hnef_zhashtable	* const HNEF_RSTR hashtable,
/*@in@*/
/*@null@*/
	unsigned int			* const hashkey,
/*@in@*/
/*@null@*/
	unsigned int			* const hashlock
#endif
	)
/*@modifies * board@*/
#ifdef HNEFATAFL_ZHASH
/*@modifies * hashkey, * hashlock@*/
#endif
{
	HNEF_BOOL			captured	= HNEF_FALSE;
	unsigned short			destx,
					desty;
	int				i;
	const struct hnef_ruleset	* HNEF_RSTR rules = NULL;

	assert	(NULL != game);
	assert	(NULL != board);
	assert	(HNEF_BIT_U8_EMPTY != opt_pt_mov->bit);

	rules	= game->rules;
	destx	= dest % rules->bwidth;
	desty	= dest / rules->bwidth;

	for	(i = 0; i < 4; ++i)
	{
		unsigned short	adjx	= destx,
				adjy	= desty,
				adj;

		switch	(i)
		{
			case 0:
				++adjx;
/*@i1@*/			break;
			case 1:
				--adjx;
/*@i1@*/			break;
			case 2:
				++adjy;
/*@i1@*/			break;
			default:
				--adjy;
/*@i1@*/			break;
		}

		if	(adjx >= rules->bwidth
		||	adjy >= rules->bheight)
		{
			continue;
		}
		adj = (unsigned short)(adjy * rules->bwidth + adjx);

		captured = hnef_board_do_capture(game, board,
			opt_pt_mov, dest, adj
#ifdef HNEFATAFL_ZHASH
			, hashtable, hashkey, hashlock
#endif
			) || captured;
	}
	return	captured;
}

/*
 * Checks if pos to dest constitutes repetition, considering the given
 * move history.
 *
 * MOVE	POS		DEST	REP?	TYPE		CHECK
 * 1	A	->	B	No	threat		. -> B
 * 2	X	->	Y	No	defense			. -> Y
 * 3	B	->	C	No	revert-threat	B -> C
 * 4	Y	->	Z	No	revert-defense		Y -> .
 * 5	C	->	B	Yes	repeat-threat	C -> B
 *
 * (Note that MOVE 5 is pos / dest. Also note that we don't need to know
 * if pos / dest is a reversible move, because we know that if it moves
 * from C to B, and previously moved from ? -> B and then B -> C and
 * MOVE 1 and 3 are reversible, then MOVE 5 must also be reversible.)
 *
 * *	If any of the moves 1-4 is irreversible, then it's not
 *	repetition.
 *
 * *	A does not have to (but may) equal C. We already know it is the
 *	same piece.
 *	Thus move 3 is not necessarily strictly a reversion from C to A.
 *
 * *	X does not have to (but may) equal Z. We already know it is the
 *	same piece.
 *	Thus move 4 is not necessarily strictly a reversion from Y to X.
 *
 * *	A, X and Z are completely irrelevant since they don't have
 *	anything to do with the threat-defense exchange; for example, a
 *	piece could move from random square A to threat-square B, then
 *	to random square C (A != C) and back to threat-square B, and it
 *	would still be repetition. X and Z do not matter since the piece
 *	that moves to / from Y is assumed to defend, and the purpose of
 *	the rule is to forbit perpetual threats (you can usually choose
 *	to stop threatening without immediate loss, but that's typically
 *	not the case when defending -- thus the attacker is expected to
 *	alternate).
 *
 * NOTE:	Note that a piece moving from pos to dest, where dest is
 *		an escape square for the piece, causes the game to be
 *		over (won). Then no more moves will be made.
 *		moveh are only added by two functions:
 *		hnef_board_move_safe() and hnef_board_move_unsafe().
 *		moveh structs are never retrieved from
 *		hnef_board_moves_get(). Putting these facts together, it
 *		follows that an escaping move will never be the last
 *		move in a listmh when this function is called. The move
 *		to be evaluated is pos / dest, and if this function
 *		returns true, then it's implied that pos / dest does not
 *		constitute an escaping move.
 *		Therefore (in hnef_board_move_(un)safe()) we never check
 *		if a move is escaping when determining moveh->irrev. It
 *		will not affect the behavior of this function in any
 *		possible situation.
 *
 *		Also, if you moved from B to C, and are now moving back
 *		from C (pos) to B (dest), then C can not be a noreturn
 *		square for that piece while B is not -- then the move
 *		from B to C would've been illegal. Therefore we don't
 *		have to check for that in this function -- but we do
 *		need to check for it in hnef_board_move_unsafe() (which
 *		we also do).
 *
 * NOTE:	There may be odd situations where this gives false
 *		positives; and there may be odd situations where this
 *		doesn't properly trigger repetition.
 */
static
HNEF_BOOL
hnef_board_is_repeat (
/*@in@*/
/*@notnull@*/
	const struct hnef_game		* const HNEF_RSTR game,
	const unsigned short		pos,
	const unsigned short		dest,
/*@in@*/
/*@notnull@*/
	const struct hnef_listmh	* const HNEF_RSTR movehist
	)
/*@modifies nothing@*/
{
	unsigned short			pos_b,
					pos_c,
					pos_y;
	size_t				i;
	const struct hnef_ruleset	* HNEF_RSTR rules = NULL;

	assert	(NULL != game);
	assert	(NULL != movehist);

	rules	= game->rules;

	if	(!rules->forbid_repeat
	||	movehist->elemc < (size_t)4)
	{
		/*
		 * Repetition not forbidden, or too few moves.
		 */
		return	HNEF_FALSE;
	}

	pos_y	= HNEF_BOARDPOS_NONE;
	/*
	 * The last move in the move history that we are considering, is
	 * the move that we are about to make, id est the pos / dest
	 * parameters.
	 */
	pos_b	= dest;
	pos_c	= pos;

	/*
	 * Go from last move and check backwards. i = 1 is last move.
	 */
	for	(i = (size_t)1; i <= (size_t)4; ++i)
	{
		const struct hnef_moveh * const HNEF_RSTR mh =
			& movehist->elems[movehist->elemc - i];
		if	(mh->irrev)
		{
			/*
			 * Game end is irreversible, and the last move
			 * should not trigger repetition if it causes
			 * win or loss (which is impossible because then
			 * the win should have happened several moves
			 * ago, but still).
			 */
			return	HNEF_FALSE;
		}

		switch	(i)
		{
			case 1:
				pos_y	= mh->pos;
/*@i1@*/			break;
			case 2:
				if	(mh->pos != pos_b
				||	mh->dest != pos_c)
				{
					return	HNEF_FALSE;
				}
/*@i1@*/			break;
			case 3:
				if	(mh->dest != pos_y)
				{
					return	HNEF_FALSE;
				}
/*@i1@*/			break;
			/* case 4: */
			default:
				if	(mh->dest != pos_b)
				{
					return	HNEF_FALSE;
				}
/*@i1@*/			break;
		}
	}
	return	HNEF_TRUE;
}

/*
 * Returns true if the move from pos to dest is legal; else returns
 * false.
 *
 * This function also returns false if a piece on the board was not
 * found as a type_piece in g.
 *
 * Assumes that the piece at pos is the moving piece, and that the
 * player who owns that piece type is the player to move in b (else the
 * move is of course never legal -- though it is valid to call this
 * function in that case).
 *
 * A move can be illegal for the following reasons:
 *
 * *	pos is dest.
 *
 * *	pos or dest is out of board bounds.
 *
 * *	pos to dest indicates a non-orthogonal move.
 *
 * *	The piece on pos (the moving piece) is HNEF_BIT_U8_EMPTY.
 *
 * *	The moving piece traverses from a no-noreturn square to a
 *	noreturn square.
 *
 * *	The moving piece traverses a non-occupiable (according to
 *	piece_type->occupies) or off-board (HNEF_BIT_U8_EMPTY) square.
 *
 * *	The moving piece traverses a square that is already occupied by
 *	some piece.
 *
 * *	The player whose turn it is does not own the moving piece.
 *
 * index is whose turn it is. If it's the current player's turn, then
 * give board->turn as index parameter.
 * (This exists so that the computer player can check how many moves
 * both players have, in which case it has to assume that both players
 * can move at any given board position.)
 *
 * This function does not check if the game is over. You are not allowed
 * to call this function if the game is over.
 *
 * NOTE:	This function assumes that pieces can only move
 *		orthogonally. If you add functionality for pieces that
 *		can move diagonally or in any other way, then this
 *		function will not work any more.
 *
 * `impediment` is false if the move is illegal due to repetition
 * (thanks to Alexander Dolgunin for this).
 */
static
HNEF_BOOL
hnef_board_move_legal (
/*@in@*/
/*@notnull@*/
	const struct hnef_game		* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	const struct hnef_board		* const HNEF_RSTR board,
/*@in@*/
/*@notnull@*/
	const struct hnef_listmh	* const HNEF_RSTR movehist,
	const unsigned short		index,
	const unsigned short		pos,
	const unsigned short		dest,
/*@in@*/
/*@notnull@*/
	const struct hnef_type_piece	* const HNEF_RSTR opt_pt_mov,
/*@out@*/
/*@notnull@*/
	HNEF_BOOL			* const impediment
	)
/*@modifies impediment@*/
{
	unsigned short			posx,
					posy,
					destx,
					desty;
	HNEF_BOOL			axisx;
	const struct hnef_ruleset	* HNEF_RSTR rules = NULL;

	assert	(NULL != game);
	assert	(NULL != board);
	assert	(NULL != movehist);
	assert	(NULL != impediment);

	rules	= game->rules;
	assert	(NULL != rules->pieces);
	assert	(NULL != rules->squares);

	* impediment	= HNEF_TRUE;

	if	(pos == dest)
	{
		return	HNEF_FALSE;
	}

	if	(pos >= rules->opt_blen
	||	dest >= rules->opt_blen)
	{
		return	HNEF_FALSE;
	}

	/*
	 * opt_pt_mov is an optimization. This is the code used for
	 * retrieving them without using the parameters:
	 *
	 *	pb_mov	= b->pieces[pos];
	 *	if (HNEF_BIT_U8_EMPTY == pb_mov) { ...fail... }
	 *
	 *	pt_mov	= type_piece_get(g->rules, pb_mov);
	 *	if (NULL == pt_mov) { ...fail... }
	 *
	 */
	if	(opt_pt_mov->owner != index)
	{
		return	HNEF_FALSE;
	}

	posx	= pos	% rules->bwidth;
	posy	= pos	/ rules->bwidth;
	destx	= dest	% rules->bwidth;
	desty	= dest	/ rules->bwidth;
	{
		const unsigned short dx =
			(unsigned short)(hnef_max_ushrt(posx, destx)
				- hnef_min_ushrt(posx, destx));
		const unsigned short dy =
			(unsigned short)(hnef_max_ushrt(posy, desty)
				- hnef_min_ushrt(posy, desty));
		if	(dx > 0
		&&	dy > 0)
		{
			return	HNEF_FALSE;
		}
		axisx	= dx > 0;
	}

	if	(axisx)	/* posy == desty */
	{
		HNEF_BIT_U8	sbdest,
				pbdest;
		const HNEF_BOOL	dir_e	= destx > posx;
		unsigned short	tmpx	= posx;
		unsigned short	tmppos,		/* New */
				tmpdest	= pos;	/* Start position */
		do
		{
			tmpx	= (unsigned short)
				(tmpx + (dir_e ? 1 : -1));

			tmppos	= tmpdest;	/* Old dest. */
			tmpdest	= (unsigned short)
				(posy * rules->bwidth + tmpx);
			sbdest	= rules->squares[tmpdest];
			pbdest	= board->pieces[tmpdest];

			if	(!hnef_type_piece_can_return(opt_pt_mov,
				rules->squares[tmppos], sbdest))
			{
				return	HNEF_FALSE;
			}

			if	(HNEF_BIT_U8_EMPTY != pbdest
			||	!hnef_type_piece_can_occupy(opt_pt_mov,
							sbdest))
			{
				return	HNEF_FALSE;
			}
		}
		while (tmpx != destx);
	}
	else		/* posx == destx */
	{
		HNEF_BIT_U8	sbdest,
				pbdest;
		const HNEF_BOOL	dir_s	= desty > posy;
		unsigned short	tmpy	= posy;
		unsigned short	tmppos,		/* New */
				tmpdest	= pos;	/* Start position */
		do
		{
			tmpy	= (unsigned short)
				(tmpy + (dir_s ? 1 : -1));

			tmppos	= tmpdest;	/* Old dest. */
			tmpdest	= (unsigned short)
				(tmpy * rules->bwidth + posx);
			sbdest	= rules->squares[tmpdest];
			pbdest	= board->pieces[tmpdest];

			if	(!hnef_type_piece_can_return(opt_pt_mov,
				rules->squares[tmppos], sbdest))
			{
				return	HNEF_FALSE;
			}

			if	(HNEF_BIT_U8_EMPTY != pbdest
			||	!hnef_type_piece_can_occupy(opt_pt_mov,
							sbdest))
			{
				return	HNEF_FALSE;
			}
		}
		while	(tmpy != desty);
	}

	* impediment	= HNEF_FALSE;
	return	!hnef_board_is_repeat(game, pos, dest, movehist);
}

/*
 * Makes a move on b from pos to dest, ignoring if it's legal.
 *
 * The move is added to movehist.
 *
 * This should only be done after retrieving a list of moves that are
 * known to be legal. This function exists for the minimax computer
 * player to reduce the time it takes to check if every move is legal.
 *
 * This will even ignore if pos and dest are out of bounds coordinates,
 * and so may write past the end of allocated memory if you give it bad
 * coordinates.
 *
 * The hashkey is allowed to be NULL. If so it will be ignored, and
 * hashtable will also be expected to be NULL (but you are allowed to
 * pass a non-NULL hashtable anyway). If the hashkey is non-NULL, then
 * the hashtable must also be a valid non-NULL hashtable.
 */
enum HNEF_FR
hnef_board_move_unsafe (
	const struct hnef_game		* const HNEF_RSTR game,
	struct hnef_board		* const board,
	struct hnef_listmh		* const movehist,
	const unsigned short		pos,
	const unsigned short		dest
#ifdef HNEFATAFL_ZHASH
	,
	const struct hnef_zhashtable	* const HNEF_RSTR hashtable,
	unsigned int			* const hashkey,
	unsigned int			* const hashlock
#endif
	)
{
	const struct hnef_type_piece	* HNEF_RSTR pt_mov	= NULL;
	struct hnef_ruleset		* HNEF_RSTR rules	= NULL;
	HNEF_BOOL			irrev		= HNEF_FALSE;

	assert	(NULL != game);
	assert	(NULL != board);
	assert	(NULL != movehist);

	rules	= game->rules;
	assert	(NULL != rules->pieces);
	assert	(NULL != rules->squares);

	pt_mov	= hnef_type_piece_get(rules, board->pieces[pos]);

	assert	(HNEF_BIT_U8_EMPTY != pt_mov->bit);

	/*
	 * Set the hashkey at dest and unset it at pos.
	 *
	 * NOTE:	This assumes that dest was empty before the
	 *		move, id est that pieces can not move to
	 *		non-empty squares.
	 */
#ifdef HNEFATAFL_ZHASH
	if	(NULL != hashkey)
	{
		const unsigned short pb_ind =
			hnef_type_index_get(board->pieces[pos]);

		assert	(NULL != hashlock);
		assert	(NULL != hashtable);

		hnef_board_hash_xor(hashtable, hashkey,
			hashlock, pb_ind, dest);
		hnef_board_hash_xor(hashtable, hashkey,
			hashlock, pb_ind, pos);
	}
#endif

	board->pieces[dest]	= board->pieces[pos];
	board->pieces[pos]	= HNEF_BIT_U8_EMPTY;

	/*
	 * If the move is a capture, or moves from a noreturn square to
	 * a !noreturn-square (and therefore can't move back), then the
	 * move is not reversible.
	 */
	irrev	= hnef_board_do_captures(game, board, pt_mov, dest
#ifdef HNEFATAFL_ZHASH
		, hashtable, hashkey, hashlock
#endif
		)
		||	(((unsigned int)pt_mov->noreturn &
				(unsigned int)rules->squares[pos])
				== (unsigned int)pt_mov->noreturn
		&&	((unsigned int)pt_mov->noreturn &
				(unsigned int)rules->squares[dest])
				!= (unsigned int)pt_mov->noreturn);

	hnef_board_turn_adv(board);

	return	hnef_listmh_add(movehist, pos, dest, irrev);
}

/*
 * Makes a move on b from pos to dest, if it's legal. If so, sets legal
 * to true. Else sets legal to false and doesn't make the move.
 *
 * This function checks if the game is over. If the game is over, it
 * will return illegal = true.
 *
 * NOTE:	This function does not set hashkeys.
 */
static
enum HNEF_FR
hnef_board_move_safe (
/*@in@*/
/*@notnull@*/
	struct hnef_game	* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	struct hnef_board	* const board,
/*@in@*/
/*@notnull@*/
	struct hnef_listmh	* const movehist,
	const unsigned short	pos,
	const unsigned short	dest,
/*@out@*/
/*@notnull@*/
	HNEF_BOOL		* const HNEF_RSTR legal
	)
/*@modifies * board, * movehist, * legal@*/
{
	HNEF_BIT_U8			pb_mov;
	const struct hnef_type_piece	* HNEF_RSTR pt_mov	= NULL;
	struct hnef_ruleset		* HNEF_RSTR rules	= NULL;
	HNEF_BOOL		ignored_bool;
	unsigned short		ignored_shrt	= HNEF_PLAYER_UNINIT;

	assert	(NULL != game);
	assert	(NULL != board);
	assert	(NULL != legal);

	rules	= game->rules;

	if	(pos >= rules->opt_blen
	||	dest >= rules->opt_blen)
	{
		* legal	= HNEF_FALSE;
		return	HNEF_FR_SUCCESS;
	}

	pb_mov	= board->pieces[pos];
	if	(HNEF_BIT_U8_EMPTY == pb_mov)
	{
		* legal	= HNEF_FALSE;
		return	HNEF_FR_SUCCESS;
	}
	pt_mov	= hnef_type_piece_get(rules, pb_mov);

	assert	(HNEF_BIT_U8_EMPTY != pt_mov->bit);

	* legal	= hnef_board_move_legal(game, board, movehist,
		board->turn, pos, dest, pt_mov, & ignored_bool);
	if	(!(* legal))
	{
		return	HNEF_FR_SUCCESS;
	}

	if	(hnef_board_game_over(game, board, movehist,
			& ignored_shrt))
	{
		* legal	= HNEF_FALSE;
		return	HNEF_FR_SUCCESS;
	}

	return	hnef_board_move_unsafe(game, board, movehist, pos, dest
#ifdef HNEFATAFL_ZHASH
		, NULL, NULL, NULL
#endif
		);
}

/*
 * Delegates hnef_board_move_safe with g->b and g->movehist.
 */
enum HNEF_FR
hnef_game_move (
	struct hnef_game		* const HNEF_RSTR game,
	const unsigned short		pos,
	const unsigned short		dest,
	HNEF_BOOL			* const HNEF_RSTR legal
	)
{
	assert	(NULL != game);
	assert	(NULL != game->board);
	assert	(NULL != legal);
	return	hnef_board_move_safe(game, game->board, game->movehist,
					pos, dest, legal);
}

/*
 * Adds all moves that are possible for the current player to move in b
 * (according to b->turn) to list.
 *
 * Does not clear moves before adding.
 *
 * NOTE:	This function assumes that pieces can only move
 *		orthogonally, and will not work if you add ability to
 *		move in other directions.
 *		It further assumes that a piece can never move past a
 *		square that it can't occupy.
 *
 * NOTE:	Any changes to this function must be reflected by
 *		hnef_board_movec_get.
 *
 * If `pos_piece` is `HNEF_BOARDPOS_NONE`, then this function returns
 * the moves for all pieces. If it indicates a position existing on the
 * board, then it returns the moves for the piece on that position, if
 * any (the piece or square indicated by `pos_piece` does not have to
 * exist on the board, but it has be `< opt_blen`).
 *
 * This function SHOULD not be called if the game is over, although
 * tests indicate that it may be safe to do so.
 */
enum HNEF_FR
hnef_board_moves_get_pos (
	const struct hnef_game		* const HNEF_RSTR game,
	const struct hnef_board		* const HNEF_RSTR board,
	const struct hnef_listmh	* const HNEF_RSTR movehist,
	struct hnef_listm		* const list,
	const unsigned short		pos_piece
	)
{
	/*
	 * Algorithm:
	 *
	 * 1.	For every piece on the board owned by the player to
	 *	move:
	 *
	 * 2.	For every square in every direction (north, east, south,
	 *	west) from the piece's position to the edge of the
	 *	board, check if the piece can move to that destination.
	 *	If not, then stop checking in that direction.
	 */
	unsigned short	pos,
			posx,
			posy,
			pos_begin,
			pos_end,
			dest,
			destx,
			desty;
	HNEF_BIT_U8	opt_p_owned,
			p_pos;
	int		i,
			modx,
			mody;
	const struct hnef_type_piece	* HNEF_RSTR opt_pt_mov	= NULL;
	struct hnef_ruleset		* HNEF_RSTR rules	= NULL;

	assert	(NULL != game);
	assert	(NULL != board);
	assert	(NULL != list);

	rules		= game->rules;
	opt_p_owned	= game->players[board->turn]->opt_owned;

	if	(HNEF_BOARDPOS_NONE == pos_piece)
	{
		assert	(rules->opt_blen > 0);
		pos_begin	= 0;
		pos_end		= (unsigned short)(rules->opt_blen - 1);
	}
	else
	{
		assert	(pos_piece < rules->opt_blen);
		pos_begin	= pos_end	= pos_piece;
	}

	for	(pos = pos_begin; pos <= pos_end; ++pos)
	{
		p_pos	= board->pieces[pos];
		if	(HNEF_BIT_U8_EMPTY == p_pos
		||	((unsigned int)opt_p_owned
			& (unsigned int)p_pos) != (unsigned int)p_pos)
		{
			continue;
		}
		posx	= (unsigned short)(pos % rules->bwidth);
		posy	= (unsigned short)(pos / rules->bwidth);

		for	(i = 0; i < 4; ++i)
		{
			modx	= (unsigned short)0;
			mody	= (unsigned short)0;
			switch	(i)
			{
				case 0:
					++modx;
/*@i1@*/				break;
				case 1:
					--modx;
/*@i1@*/				break;
				case 2:
					++mody;
/*@i1@*/				break;
				default:
					--mody;
/*@i1@*/				break;
			}
			destx	= (unsigned short)(posx + modx);
			desty	= (unsigned short)(posy + mody);
			/*
			 * Check every square from pos to the edge of
			 * the board. Break the loop as soon as we hit a
			 * square that we're not allowed to move to.
			 */
			while	(destx	< rules->bwidth
			&&	desty	< rules->bheight)
			{
				HNEF_BOOL impediment;
				dest	= (unsigned short)(desty
					* rules->bwidth + destx);
				if	(NULL == opt_pt_mov
				||	p_pos != opt_pt_mov->bit)
				{
					/*
					 * Lazily retrieve opt_pt_mov if
					 * needed.
					 */
					opt_pt_mov = hnef_type_piece_get
						(rules, p_pos);
					assert	(HNEF_BIT_U8_EMPTY
						!= opt_pt_mov->bit);
				}
				if	(hnef_board_move_legal(game,
					board, movehist, board->turn,
					pos, dest, opt_pt_mov,
					& impediment))
				{
					const enum HNEF_FR fr =
						hnef_listm_add
						(list, pos, dest);
					if	(!hnef_fr_good(fr))
					{
						return	fr;
					}
				}
/*@i1@*/			else if	(impediment)
				{
					/*
					 * Stop checking this direction.
					 */
/*@innerbreak@*/			break;
				}
				destx = (unsigned short)(destx + modx);
				desty = (unsigned short)(desty + mody);
			}
		}
	}
	return	HNEF_FR_SUCCESS;
}

enum HNEF_FR
hnef_board_moves_get (
	const struct hnef_game		* const HNEF_RSTR game,
	const struct hnef_board		* const HNEF_RSTR board,
	const struct hnef_listmh	* const HNEF_RSTR movehist,
	struct hnef_listm		* const list
	)
{
	assert	(NULL != game);
	assert	(NULL != board);
	assert	(NULL != movehist);
	assert	(NULL != list);
	return	hnef_board_moves_get_pos(game, board, movehist, list,
					HNEF_BOARDPOS_NONE);
}

enum HNEF_FR
hnef_game_moves_get_pos (
	const struct hnef_game		* const HNEF_RSTR game,
	struct hnef_listm		* const list,
	const unsigned short		pos_piece
	)
{
	assert	(NULL != game);
	assert	(NULL != game->board);
	assert	(NULL != list);
	return	hnef_board_moves_get_pos
		(game, game->board, game->movehist, list, pos_piece);
}

/*
 * Works like hnef_board_moves_get, but returns the move count (movec).
 *
 * piecemask is the bitmask of pieces to get move count for.
 *
 * Negative return value is a failure.
 *
 * If `fast`, then it will return 1 if any amount of moves are available
 * or 0 otherwise. This significantly speeds up the computer player.
 * Optimization by Alexander Dolgunin.
 *
 * NOTE:	Any changes to this function must be reflected by
 *		hnef_board_moves_get.
 */
int
hnef_board_movec_get (
	const struct hnef_game		* const HNEF_RSTR game,
	struct hnef_board		* const HNEF_RSTR board,
	const struct hnef_listmh	* const HNEF_RSTR movehist,
	const HNEF_BIT_U8		piecemask,
	const HNEF_BOOL			fast
	)
{
	unsigned short	pos,
			posx,
			posy,
			dest,
			destx,
			desty;
	HNEF_BIT_U8	p_pos;
	int		i,
			modx,
			mody;
	const struct hnef_type_piece	* HNEF_RSTR opt_pt_mov	= NULL;
	int				movec			= 0;
	struct hnef_ruleset		* HNEF_RSTR rules	= NULL;

	assert	(NULL != game);
	assert	(NULL != board);
	assert	(NULL != movehist);

	rules	= game->rules;

	if	(piecemask == rules->opt_tp_escape
	&&	rules->opt_escape_piecec == (unsigned short)1
	&&	rules->opt_tp_escape == rules->opt_tp_captloss)
	{
		/*
		 * All code involving `b->opt_captloss_piece_lastseen`
		 * are optimizations by Alexander Dolgunin to remember
		 * the king's last position. Earlier we would search the
		 * whole board for the king.
		 */
		unsigned short	pos1,
				pos2;
		/*
		 * Find king's moves, reusing king's position from
		 * `b->opt_captloss_piece_lastseen`.
		 */
		HNEF_BIT_U8	pbit = rules->opt_tp_escape;

		/*
		 * Check old square first.
		 */
		if	(board->pieces
			[board->opt_captloss_piece_lastseen] == pbit)
		{
			goto CONTINUE_KINGFOUND;
		}

		/*
		 * Check even number of squares, alternating one in each
		 * direction.
		 */
		pos1	= pos2 = board->opt_captloss_piece_lastseen;
		while	(pos2 > 0
		&&	++pos1 < rules->opt_blen)
		{
			if	(board->pieces[pos1] == pbit)
			{
				goto CONTINUE_MAINLOOP_POS1;
			}
			if	(board->pieces[--pos2] == pbit)
			{
				goto CONTINUE_MAINLOOP_POS2;
			}
		}

		/*
		 * Remaining odd number of squares is scanned
		 * unidirectionally.
		 */
		if	(0 == pos2)
		{
			/*
			 * Piece was in the upper half, scan tail.
			 */
			while	(++pos1 < rules->opt_blen)
			{
				if	(board->pieces[pos1] == pbit)
				{
					goto CONTINUE_MAINLOOP_POS1;
				}
			}
		}
		else
		{
			/*
			 * Scan head.
			 */
			while	(pos2 > 0)
			{
				if (board->pieces[--pos2] == pbit)
				{
					goto CONTINUE_MAINLOOP_POS2;
				}
			}
		}
		/*
		 * King not found.
		 */
		return movec;

		CONTINUE_MAINLOOP_POS1:
		board->opt_captloss_piece_lastseen = pos1;
		goto CONTINUE_KINGFOUND;

		CONTINUE_MAINLOOP_POS2:
		board->opt_captloss_piece_lastseen = pos2;

		CONTINUE_KINGFOUND:
		pos		= board->opt_captloss_piece_lastseen;
		p_pos		= board->pieces[pos];
		opt_pt_mov	= hnef_type_piece_get(rules, p_pos);

		assert	(HNEF_BIT_U8_EMPTY != opt_pt_mov->bit);

		posx	= (unsigned short)(pos % rules->bwidth);
		posy	= (unsigned short)(pos / rules->bwidth);

		for	(i = 0; i < 4; ++i)
		{
			modx	= (unsigned short)0;
			mody	= (unsigned short)0;
			switch	(i)
			{
				case 0:
					++modx;
/*@i1@*/				break;
				case 1:
					--modx;
/*@i1@*/				break;
				case 2:
					++mody;
/*@i1@*/				break;
				default:
					--mody;
/*@i1@*/				break;
			}
			destx	= (unsigned short)(posx + modx);
			desty	= (unsigned short)(posy + mody);
			/*
			 * Check every square from pos to the edge of
			 * the board. Break the loop as soon as we hit a
			 * square that we're not allowed to move to.
			 */
			while	(destx	< rules->bwidth
			&&	desty	< rules->bheight)
			{
				HNEF_BOOL impediment;
				dest = (unsigned short)(desty
					* rules->bwidth + destx);
				if	(hnef_board_move_legal(game,
					board, movehist,
					opt_pt_mov->owner, pos,
					dest, opt_pt_mov, & impediment))
				{
					++movec;
					if	(fast)
					{
						return movec;
					}
				}
/*@i1@*/			else if	(impediment)
				{
					/*
					 * Stop checking this direction.
					 */
/*@innerbreak@*/
					break;
				}
				destx = (unsigned short)(destx + modx);
				desty = (unsigned short)(desty + mody);
			}
		}
		return movec;

	}

	for	(pos = 0; pos < rules->opt_blen; ++pos)
	{
		p_pos	= board->pieces[pos];
		if	(HNEF_BIT_U8_EMPTY == p_pos
		||	((unsigned int)piecemask & (unsigned int)p_pos)
			!= (unsigned int)p_pos)
		{
			continue;
		}
		posx	= (unsigned short)(pos % rules->bwidth);
		posy	= (unsigned short)(pos / rules->bwidth);

		for	(i = 0; i < 4; ++i)
		{
			modx	= (unsigned short)0;
			mody	= (unsigned short)0;
			switch	(i)
			{
				case 0:
					++modx;
/*@i1@*/				break;
				case 1:
					--modx;
/*@i1@*/				break;
				case 2:
					++mody;
/*@i1@*/				break;
				default:
					--mody;
/*@i1@*/				break;
			}
			destx	= (unsigned short)(posx + modx);
			desty	= (unsigned short)(posy + mody);
			/*
			 * Check every square from pos to the edge of
			 * the board. Break the loop as soon as we hit a
			 * square that we're not allowed to move to.
			 */
			while	(destx	< rules->bwidth
			&&	desty	< rules->bheight)
			{
				HNEF_BOOL impediment;
				dest = (unsigned short)(desty
					* rules->bwidth + destx);
				if	(NULL == opt_pt_mov
				||	p_pos != opt_pt_mov->bit)
				{
					/*
					 * Lazily retrieve opt_pt_mov if
					 * needed.
					 */
					opt_pt_mov = hnef_type_piece_get
						(rules, p_pos);
					assert	(HNEF_BIT_U8_EMPTY
						!= opt_pt_mov->bit);
				}
				if	(hnef_board_move_legal(game,
					board, movehist,
					opt_pt_mov->owner, pos,
					dest, opt_pt_mov, & impediment))
				{
					++movec;
					if	(fast)
					{
						return movec;
					}
				}
/*@i1@*/			else if (impediment)
				{
					/*
					 * Stop checking this direction.
					 */
/*@innerbreak@*/
					break;
				}
				destx = (unsigned short)(destx + modx);
				desty = (unsigned short)(desty + mody);
			}
		}
	}
	return	movec;
}

/*
 * Checks if the game is over because a piece has escaped.
 *
 * One piece being on an escape square is always enough to trigger
 * victory. If several pieces are on escape squares, then the first one
 * found on the board will trigger victory for the owning player, so
 * this function assumes that there will only be one piece, that can
 * escape, on an escape square.
 *
 * This function now only checks escape squares in
 * `ruleset->opt_squares_escape`, thanks to an optimization by Alexander
 * Dolgunin.
 */
static
HNEF_BOOL
hnef_board_game_over_escape (
/*@in@*/
/*@notnull@*/
	const struct hnef_game	* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	const struct hnef_board	* const HNEF_RSTR board,
/*@in@*/
/*@notnull@*/
	unsigned short		* const HNEF_RSTR winner
	)
/*@modifies * winner@*/
{
	unsigned short		i			= 0,
				sq_pos;
	struct hnef_ruleset	* HNEF_RSTR rules	= NULL;

	assert	(NULL != game);
	assert	(NULL != board);
	assert	(NULL != winner);

	rules	= game->rules;

	/*
	 * All code involving `r->opt_squares_escape` are optimizations
	 * by Alexander Dolgunin to check only escape squares. Earlier
	 * we would check all squares.
	 */
	while	(rules->opt_blen
		!= (sq_pos = rules->opt_squares_escape[i++]))
	{
		const HNEF_BIT_U8	pbit	= board->pieces[sq_pos];

		assert	(i <= rules->opt_blen);

		if	(HNEF_BIT_U8_EMPTY != pbit
		&&	((unsigned int)pbit
				& (unsigned int)rules->opt_tp_escape)
				== (unsigned int)pbit)
		{
			* winner = hnef_type_piece_get
				(rules, pbit)->owner;
			return	HNEF_TRUE;
		}
	}
	return	HNEF_FALSE;
}

/*
 * Checks if the game is over due to a particular type of piece being
 * captured.
 *
 * This requires all pieces on the board of that type to be captured. If
 * there is one piece on the board left of that type, then the game is
 * not over.
 */
static
HNEF_BOOL
hnef_board_game_over_captloss (
/*@in@*/
/*@notnull@*/
	const struct hnef_game	* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	struct hnef_board	* const HNEF_RSTR board,
/*@in@*/
/*@notnull@*/
	unsigned short		* const HNEF_RSTR winner
	)
/*@modifies * board, * winner@*/
{
	unsigned short			i,
					j;
	HNEF_BIT_U8			pbit;
	const struct hnef_ruleset	* HNEF_RSTR rules = NULL;

	/*
	 * This optimization probably makes sense only if there's one
	 * piece type in the rules that has capt_loss bit, otherwise
	 * we'd have to keep last seen positions of each type in an
	 * array. So we do special board scanning only for the first
	 * such type, and revert to default algorithm for the rest of
	 * them.
	 */
	HNEF_BOOL	first_captloss_type	= HNEF_TRUE;

	assert	(NULL != game);
	assert	(NULL != board);
	assert	(NULL != winner);

	rules	= game->rules;

	for	(i = 0; i < rules->type_piecec; ++i)
	{
		if	(!rules->type_pieces[i].capt_loss)
		{
			continue;
		}
		pbit	= rules->type_pieces[i].bit;

		if	(first_captloss_type)
		{
			/*
			 * Optimization by Alexander Dolgunin. Remember
			 * the king's old position.
			 */
			unsigned short	pos1,
					pos2;
			first_captloss_type = HNEF_FALSE;

			/*
			 * Check old square first.
			 */
			if	(board->pieces
				[board->opt_captloss_piece_lastseen]
								== pbit)
			{
				goto CONTINUE_NOTMOVED;
			}

			/*
			 * Check even number of squares, alternating one
			 * in each direction.
			 */
			pos1	= pos2 =
				board->opt_captloss_piece_lastseen;
			while	(pos2 > 0
			&&	++pos1 < rules->opt_blen)
			{
				if	(board->pieces[pos1] == pbit)
				{
					goto CONTINUE_MAINLOOP_POS1;
				}
				if	(board->pieces[--pos2] == pbit)
				{
					goto CONTINUE_MAINLOOP_POS2;
				}
			}
			/*
			 * Remaining odd number of squares is scanned
			 * unidirectionally.
			 */
			if	(0 == pos2)
			{
				/*
				 * Piece was in the upper half, scan
				 * tail.
				 */
				while	(++pos1 < rules->opt_blen)
				{
					if (board->pieces[pos1] == pbit)
					{
						goto
						CONTINUE_MAINLOOP_POS1;
					}
				}
			}
			else
			{
				/*
				 * Scan head.
				 */
				while	(pos2 > 0)
				{
					if	(board->pieces[--pos2]
						== pbit)
					{
						goto
						CONTINUE_MAINLOOP_POS2;
					}
				}
			}

			/*
			 * No pieces of type pbit left on the board.
			 */
			* winner = (unsigned short)(0 == board->turn
				? 1
				: 0);
			return	HNEF_TRUE;

			CONTINUE_MAINLOOP_POS1:
			board->opt_captloss_piece_lastseen = pos1;
			continue;

			CONTINUE_MAINLOOP_POS2:
			board->opt_captloss_piece_lastseen = pos2;
			continue;
		}
		else
		{
			for	(j = 0; j < rules->opt_blen; ++j)
			{
				if (board->pieces[j] == pbit)
				{
					goto CONTINUE_NOTMOVED;
				}
			}
		}
		/*
		 * No pieces of type pbit left on the board.
		 */
		* winner = (unsigned short)(0 == board->turn
			? 1
			: 0);
		return	HNEF_TRUE;

		CONTINUE_NOTMOVED:;	/* gcc requires ; */
	}
	return	HNEF_FALSE;
}

/*
 * Returns true if game is over. If so, winner is set. If not, winner is
 * not set.
 *
 * The game is over if:
 *
 * *	The player to move in b doesn't have any moves left (he loses).
 *
 *	Note that we only check if the player whose move it is has any
 *	moves left. We don't care if the other player has moves left,
 *	since he has no obligation to move during this turn. If the
 *	other player can't move, then the game is not over, unless he
 *	still can't move when it's his turn (this can happen but should
 *	be very rare).
 *
 * *	Any piece escaped (according to hnef_board_game_over_escape).
 *
 * *	All pieces of a single type_piece with capt_loss are captured
 *	(according to hnef_board_game_over_captloss).
 *
 * NOTE:	This function assumes that there are only two players.
 *		If one has won (or lost), then the other has lost (or
 *		won).
 */
HNEF_BOOL
hnef_board_game_over (
	const struct hnef_game		* const HNEF_RSTR game,
	struct hnef_board		* const HNEF_RSTR board,
	const struct hnef_listmh	* const HNEF_RSTR movehist,
	unsigned short			* const winner
	)
{
	assert	(NULL != game);
	assert	(NULL != board);
	assert	(NULL != movehist);
	assert	(NULL != winner);

	/*
	 * NOTE:	-1 moves left indicates a failure, but there's
	 *		nothing we can do to report it. However it's
	 *		guaranteed to not occur because all pieces must
	 *		be defined, and the failure can only happen if
	 *		type_piece_get fails to return a type_piece.
	 */
	if	(hnef_board_movec_get(game, board, movehist,
		game->players[board->turn]->opt_owned, HNEF_TRUE) < 1)
	{
		* winner = (unsigned short)(0 == board->turn
			? 1
			: 0);
		return	HNEF_TRUE;
	}

	return	hnef_board_game_over_escape	(game, board, winner)
	||	hnef_board_game_over_captloss	(game, board, winner);
}

/*
 * Delegates hnef_board_game_over with g->b and g->movehist.
 */
HNEF_BOOL
hnef_game_over (
	struct hnef_game	* const HNEF_RSTR game,
	unsigned short		* const winner
	)
{
	assert	(NULL != game);
	assert	(NULL != game->board);
	assert	(NULL != winner);
	return	hnef_board_game_over(game, game->board, game->movehist,
					winner);
}

/*
 * Sets turn to 0 (player 0 always starts) and all pieces to
 * HNEF_BIT_U8_EMPTY.
 */
void
hnef_board_init (
	struct hnef_board	* const HNEF_RSTR board,
	const unsigned short	blen
	)
{
	assert	(NULL != board);
	memset	(board->pieces, (int)HNEF_BIT_U8_EMPTY, (size_t)blen);
	board->turn				= (unsigned short)0;
	board->opt_captloss_piece_lastseen	= (unsigned short)0;
}

struct hnef_board *
hnef_alloc_board (
	const unsigned short	blen
	)
{
	struct hnef_board * HNEF_RSTR board = malloc(sizeof(* board));
	if	(NULL == board)
	{
		return	NULL;
	}

	board->pieces	= malloc(sizeof(* board->pieces) * blen);
	if	(NULL == board->pieces)
	{
		free	(board);
		return	NULL;
	}

	return	board;
}

void
hnef_free_board (
	struct hnef_board	* const HNEF_RSTR board
	)
{
	assert	(NULL != board);
	if	(NULL != board->pieces)
	{
		free	(board->pieces);
	}
	free	(board);
}

