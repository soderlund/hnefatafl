/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */
#include <stdlib.h>	/* malloc */

#include "movet.h"		/* HNEF_BOARDPOS_NONE */
#include "ruleset.h"
#include "type_piece.h"		/* hnef_type_piece_init */
#include "type_square.h"	/* hnef_type_square_init */
#include "types.h"		/* HNEF_TYPEC_* */

/*
 * Minimum and maximum allowed board size.
 */
/*@unchecked@*/
static
const unsigned short	SIZE_BOARD_MIN = (unsigned short)1,
			SIZE_BOARD_MAX = (unsigned short)31;

/*
 * Returns HNEF_TRUE if board size (which may be width or height) is
 * allowed; false otherwise.
 */
HNEF_BOOL
hnef_ruleset_valid_size (
	const unsigned short	w_or_h
	)
{
	return	w_or_h >= SIZE_BOARD_MIN
	&&	w_or_h <= SIZE_BOARD_MAX;
}

/*
 * Initializes optimization variables.
 *
 * The game must be valid for this function.
 */
static
void
hnef_ruleset_initopt (
/*@notnull@*/
/*@partial@*/
	struct hnef_ruleset	* const HNEF_RSTR rules
	)
/*@modifies * rules@*/
{
	unsigned short	i,
			j;

	assert	(NULL != rules);
	assert	(NULL != rules->pieces);
	assert	(NULL != rules->squares);

	rules->opt_tp_dbl_trap			=
		rules->opt_tp_dbl_trap_capt	=
		rules->opt_tp_dbl_trap_compl	=
		rules->opt_ts_capt_sides	= HNEF_BIT_U8_EMPTY;

	for	(i = (unsigned short)0; i < rules->type_piecec; ++i)
	{
		const struct hnef_type_piece * const HNEF_RSTR type =
			& rules->type_pieces[i];

		if	(type->dbl_trap)
		{
			rules->opt_tp_dbl_trap = (HNEF_BIT_U8)
				((unsigned int)rules->opt_tp_dbl_trap
				| (unsigned int)type->bit);
		}
		if	(type->dbl_trap_capt)
		{
			rules->opt_tp_dbl_trap_capt = (HNEF_BIT_U8)
			((unsigned int)rules->opt_tp_dbl_trap_capt
				| (unsigned int)type->bit);
		}
		if	(type->dbl_trap_compl)
		{
			rules->opt_tp_dbl_trap_compl = (HNEF_BIT_U8)
			((unsigned int)rules->opt_tp_dbl_trap_compl
				| (unsigned int)type->bit);
		}
		if	(type->escape)
		{
			rules->opt_tp_escape = (HNEF_BIT_U8)
				((unsigned int)rules->opt_tp_escape
				| (unsigned int)type->bit);
		}
		if	(type->capt_loss)
		{
			rules->opt_tp_captloss = (HNEF_BIT_U8)
				((unsigned int)rules->opt_tp_captloss
				| (unsigned int)type->bit);
		}
	}

	for	(i = (unsigned short)0; i < rules->type_squarec; ++i)
	{
		const struct hnef_type_square * const HNEF_RSTR type =
			& rules->type_squares[i];
		if (HNEF_TSQUARE_CAPT_SIDES_NONE != type->capt_sides)
		{
			rules->opt_ts_capt_sides = (HNEF_BIT_U8)
				((unsigned int)rules->opt_ts_capt_sides
				| (unsigned int)type->bit);
		}
		if	(type->escape)
		{
			rules->opt_ts_escape = (HNEF_BIT_U8)
				((unsigned int)rules->opt_ts_escape
				| (unsigned int)type->bit);
		}
	}

	/*
	 * Fill escape squares array.
	 */
	j = 0;
	for	(i = 0; i < rules->opt_blen; ++i)
	{
		const HNEF_BIT_U8 sbit	= rules->squares[i];
		if	(((unsigned int)sbit &
			(unsigned int)rules->opt_ts_escape)
			== (unsigned int)sbit)
		{
			rules->opt_squares_escape[j++] = i;
		}
	}
	rules->opt_squares_escape[j] = rules->opt_blen;

	/*
	 * Count number of escape pieces, to speed up tactical value
	 * calculation via `board_movec_get()`.
	 */
	for	(i = 0; i < rules->opt_blen; ++i)
	{
		const HNEF_BIT_U8 pbit	= rules->pieces[i];
		if	(HNEF_BIT_U8_EMPTY != pbit
		&&	((unsigned int) pbit
				& (unsigned int) rules->opt_tp_escape)
				== (unsigned int) pbit)
		{
			++rules->opt_escape_piecec;
		}
	}
}

/*
 * Fills the pieces and squares arrays with HNEF_BIT_U8_EMPTY.
 *
 * Initializes hnef_type_pieces and hnef_type_squares to invalid, but
 * initialized, values. Every bit is HNEF_BIT_U8_EMPTY.
 *
 * This function is not stable. `rules` may be in an unpredictable state
 * after you call this function if it fails. If so, you can recover by
 * calling `hnef_ruleset_clear()` on `rules` -- but you should really
 * call `hnef_game_clear()` on the game struct and initialize the game
 * again (read the ruleset file again) to be really safe.
 */
enum HNEF_FR
hnef_ruleset_init_done (
	struct hnef_ruleset	* const HNEF_RSTR rules,
	const unsigned short	bwidth,
	const unsigned short	bheight
	)
{
	const unsigned short	blen =
				(unsigned short)(bwidth * bheight);

	assert	(NULL != rules);
	assert	(NULL != rules->pieces);
	assert	(NULL != rules->squares);
	assert	(hnef_ruleset_valid_size(bwidth));
	assert	(hnef_ruleset_valid_size(bheight));

	rules->bwidth	= bwidth;
	rules->bheight	= bheight;
	rules->opt_blen	= blen;

	assert	(NULL != rules->id);
	assert	(NULL != rules->name);

	if	(NULL == rules->ui_theme)
	{
		rules->ui_theme = malloc(2 * sizeof(* rules->ui_theme));
		if	(NULL == rules->ui_theme)
		{
			return	HNEF_FR_FAIL_ALLOC;
		}
		/*
		 * This should be removed if Gleipnir is removed as a
		 * dependency of uicx.
		 */
		rules->ui_theme[0] = '\0';
		rules->ui_theme[1] = '\n';
	}

	if	(NULL != rules->opt_squares_escape)
	{
		free	(rules->opt_squares_escape);
		rules->opt_squares_escape	= NULL;
	}
	assert	(NULL == rules->opt_squares_escape);

	rules->opt_squares_escape =
		malloc(sizeof(* rules->opt_squares_escape)
				* (size_t)(rules->opt_blen + 1));
	if	(NULL == rules->opt_squares_escape)
	{
		return	HNEF_FR_FAIL_ALLOC;
	}

	hnef_ruleset_initopt(rules);
	return	HNEF_FR_SUCCESS;
}

void
hnef_ruleset_clear (
	struct hnef_ruleset	* const HNEF_RSTR rules
	)
{
	unsigned short	i;

	assert	(NULL != rules);

	if	(NULL != rules->pieces)
	{
		free	(rules->pieces);
		rules->pieces	= NULL;
	}
	if	(NULL != rules->squares)
	{
		free	(rules->squares);
		rules->squares	= NULL;
	}

	if	(NULL != rules->id)
	{
		free	(rules->id);
		rules->id	= NULL;
	}
	if	(NULL != rules->name)
	{
		free	(rules->name);
		rules->name	= NULL;
	}
	if	(NULL != rules->ui_theme)
	{
		free	(rules->ui_theme);
		rules->ui_theme	= NULL;
	}

	for	(i = (unsigned short)0;
		i < (unsigned short)(HNEF_TYPE_MAX + 1); ++i)
	{
		hnef_type_piece_init(& rules->type_pieces	[i]);
		hnef_type_square_init(& rules->type_squares	[i]);
	}

	rules->bwidth			= HNEF_BOARDPOS_NONE;
	rules->bheight			= HNEF_BOARDPOS_NONE;
	rules->forbid_repeat		= HNEF_FALSE;
	rules->opt_blen			= HNEF_BOARDPOS_NONE;
	rules->opt_escape_piecec	= 0;
	rules->opt_tp_dbl_trap		= HNEF_BIT_U8_EMPTY;
	rules->opt_tp_dbl_trap_capt	= HNEF_BIT_U8_EMPTY;
	rules->opt_tp_dbl_trap_compl	= HNEF_BIT_U8_EMPTY;
	rules->opt_tp_captloss		= HNEF_BIT_U8_EMPTY;
	rules->opt_tp_escape		= HNEF_BIT_U8_EMPTY;
	rules->opt_ts_escape		= HNEF_BIT_U8_EMPTY;
	rules->type_piecec		= 0;
	rules->type_squarec		= 0;
}

/*
 * `hnef_ruleset_clear()` is NOT called automatically by this function.
 */
struct hnef_ruleset *
hnef_alloc_ruleset (void)
{
	struct hnef_ruleset *	rules	= NULL;

	rules	= malloc(sizeof(* rules));
	if	(NULL == rules)
	{
		return	NULL;
	}

	rules->id			= NULL;
	rules->name			= NULL;
	rules->ui_theme			= NULL;
	rules->pieces			= NULL;
	rules->squares			= NULL;
	rules->opt_squares_escape	= NULL;

	return rules;
}

void
hnef_free_ruleset (
	struct hnef_ruleset	* const rules
	)
{
	assert	(NULL != rules);

	if	(NULL != rules->squares)
	{
		free	(rules->squares);
	}
	if	(NULL != rules->pieces)
	{
		free	(rules->pieces);
	}

	if	(NULL != rules->id)
	{
		free	(rules->id);
	}
	if	(NULL != rules->name)
	{
		free	(rules->name);
	}
	if	(NULL != rules->ui_theme)
	{
		free	(rules->ui_theme);
	}

	if	(NULL != rules->opt_squares_escape)
	{
		free	(rules->opt_squares_escape);
	}
	free	(rules);
}

