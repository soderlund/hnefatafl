/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_CORE_TYPE_SQUARE_T_H
#define HNEF_CORE_TYPE_SQUARE_T_H

#include "boardt.h"	/* HNEF_BIT_U8 */

/*@exposed@*/
struct hnef_type_square
{

	/*
	 * bit is the square's single bit.
	 *
	 * capture is the piece bitmask of pieces that the square is
	 * hostile to. These can be owned by any player. This settings
	 * is also overridden by the piece's captures if a piece is
	 * standing on the square.
	 *
	 * capt_sides_pieces determines for which pieces this square
	 * overrides `capt_sides`. The square `capt_sides` setting only
	 * overrides `capt_sides` for the pieces in this bitmask.
	 *
	 * For `ui_bit`, see the documentation on `type_piece.ui_bit`.
	 */
	HNEF_BIT_U8	bit,
			captures,
			capt_sides_pieces,
			ui_bit;

	/*
	 * Unless HNEF_TSQUARE_CAPT_SIDES_NONE, this square overrides
	 * type_piece.capt_sides when a piece (which is in the
	 * `capt_sides_pieces` bitmask) is standing on it. This does not
	 * affect other pieces than the ones in `capt_sides_pieces`.
	 *
	 * If applicable for the given type of piece, a square type
	 * always overrides capt_sides for a piece, unless double trap
	 * requires the piece to be fully surrounded and we're checking
	 * for a double trap.
	 *
	 * type_piece->custodial is automatically dropped if this is 4
	 * sides.
	 */
	unsigned short	capt_sides;

	/*
	 * True if type_pieces with escape can escape to this square to
	 * win the game.
	 */
	HNEF_BOOL	escape;

};

#endif

