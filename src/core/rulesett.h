/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_CORE_RULESET_T_H
#define HNEF_CORE_RULESET_T_H

#include "boardt.h"		/* HNEF_BIT_U8 */
#include "type_piecet.h"	/* hnef_type_piece */
#include "type_squaret.h"	/* hnef_type_square */
#include "types.h"		/* HNEF_TYPE_MAX */

/*
 * The length of the pieces and squares arrays is, in both cases,
 * bwidth * bheight. This is also the length of the pieces array in
 * board.
 */
/*@exposed@*/
struct hnef_ruleset
{

	/*
	 * id is the ruleset ID.
	 *
	 * name is the ruleset name (for display purposes only).
	 *
	 * ui_theme is a completely optional UI hint about which theme
	 * to use, which the UI is always free to ignore, even if the UI
	 * has different themes.
	 */
/*@null@*/
/*@owned@*/
	char	* id,
		* name,
		* ui_theme;

	/*
	 * forbid_repeat is true if repetition is forbidden.
	 */
	HNEF_BOOL	forbid_repeat;

	/*
	 * bheight is the height (y-axis length) of the board.
	 *
	 * bwidth is the width (x-axis length) of the board.
	 *
	 * Until initialized, width and height are `HNEF_BOARDPOS_NONE`,
	 * which is an invalid size.
	 *
	 * hnef_type_pieces is the amount of defined hnef_type_squares.
	 * This does not equal the array length.
	 *
	 * hnef_type_squarec is the amount of defined hnef_type_squares.
	 * This does not equal the array length.
	 *
	 * opt_blen is bwidth * bheight.
	 *
	 * opt_escape_pieces is the number of pieces that can escape.
	 */
	unsigned short	bheight,
			bwidth,
			type_piecec,
			type_squarec,
			opt_blen,
			opt_escape_piecec;

	/*
	 * Indices of escape squares. `opt_blen` is the sentinel value
	 * (the length is therefore `opt_blen + 1`, likely entailing
	 * some wasted bytes since most squares are not escape squares).
	 */
/*@notnull@*/
/*@owned@*/
	unsigned short	* opt_squares_escape;

	/*
	 * pieces is the pieces array. Length is bwidth * bheight.
	 *
	 * squares is the squares array. Length is bwidth * bheight.
	 */
/*@null@*/
/*@owned@*/
	HNEF_BIT_U8	* pieces,
			* squares;

	/*
	 * opt_tp_captloss is a bitmask of hnef_type_pieces with
	 * captloss.
	 *
	 * opt_tp_dbl_trap is a bitmask of hnef_type_pieces with
	 * double_trap == HNEF_TRUE.
	 *
	 * opt_tp_dbl_trap_capt is a bitmask of hnef_type_pieces with
	 * double_trap_capt == HNEF_TRUE.
	 *
	 * opt_tp_dbl_trap_compls is a bitmask of hnef_type_pieces with
	 * double_trap_compl == HNEF_TRUE.
	 *
	 * opt_tp_escape is a bitmask of hnef_type_pieces with escape.
	 *
	 * opt_ts_capt_sides is a bitmask of hnef_type_square for which
	 * hnef_type_square.capt_sides overrides
	 * hnef_type_piece.capt_sides. If a square type's bit is among
	 * these, then hnef_type_square.capt_sides is a valid amount of
	 * sides that can override hnef_type_piece.capt_sides.
	 *
	 * opt_ts_escape is a bitmask of hnef_type_squares with escape.
	 */
	HNEF_BIT_U8	opt_tp_captloss,
			opt_tp_dbl_trap,
			opt_tp_dbl_trap_capt,
			opt_tp_dbl_trap_compl,
			opt_tp_escape,
			opt_ts_capt_sides,
			opt_ts_escape;

	/*
	 * Defined piece types.
	 *
	 * These piece types that are always present in this array
	 * regardless if they have been defined by a ruleset file:
	 *
	 * INDEX	PIECE BIT
	 * 0		1
	 * 1		2
	 * 2		4
	 * 3		8
	 * 4		16
	 * 5		32
	 * 6		64
	 * 7		128
	 * 8		Always HNEF_BIT_U8_EMPTY (invalid)
	 *
	 * Note that type_pieces[8] is a hnef_type_piece that is
	 * returned as an invalid value. You should not use it (for
	 * anything else than checking if it's bit is
	 * HNEF_BIT_U8_EMPTY), nor change its statistics.
	 *
	 * If a hnef_type_piece in this array has bit ==
	 * HNEF_BIT_U8_EMPTY, then that hnef_type_piece's bit (which you
	 * gave to type_piece_get) has not been defined by the ruleset,
	 * or it's invalid.
	 *
	 * The hnef_type_piece->bit is only set to non-0 for pieces that
	 * are defined in the ruleset file. All other hnef_type_pieces
	 * have bit == HNEF_BIT_U8_EMPTY, to show that they are not
	 * used.
	 *
	 * The pieces have to be defined from 1 to 128 in order.
	 * Defining piece 1 and then 4 is an error. Therefore
	 * hnef_type_piecec will always be the amount of defined pieces
	 * and you can always iterate over hnef_type_pieces from 0 to
	 * hnef_type_piecec.
	 */
/*@notnull@*/
	struct hnef_type_piece	type_pieces[(unsigned short)
						(HNEF_TYPE_MAX + 1)];

	/*
	 * Defined square types.
	 *
	 * Works exactly like hnef_type_piece but for hnef_type_square
	 * structs.
	 */
/*@notnull@*/
	struct hnef_type_square	type_squares[(unsigned short)
						(HNEF_TYPE_MAX + 1)];

};

#endif

