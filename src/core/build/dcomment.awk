#!/usr/bin/awk -f

BEGIN {
	comment		= 0;
	comment_end	= 0;
	emptyc		= 0;
	include		= 0;
}

/^#include "/ {
	include	= 1;
}

/^#include </ {
	include	= 0;
}

/^\t*\/\*$/ {
	comment	= 1;
}

/^\t*\/\*[^@]/ {
	comment	= 1;
}

/.*\*\// {
	comment_end	= 1;
}

/^$/ {
	emptyc	+= 1;
}

/..*/ {
	if	(!comment && !include)
	{
		emptyc	= 0;
	}
}

{
	if	(emptyc < 2 && !comment && !include)
	{
		print $0
	}
	if	(comment_end)
	{
		comment		= 0;
		comment_end	= 0;
	}
	include		= 0;
}

