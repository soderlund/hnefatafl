/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_CORE_GAME_T_H
#define HNEF_CORE_GAME_T_H

#include "listmht.h"	/* hnef_listmh */
#include "rulesett.h"	/* hnef_ruleset */

/*
 * Information about a game.
 *
 * -	Ruleset.
 *	-	Initial piece layout array	(rules->pieces).
 *	-	Squares array			(rules->squares).
 *	-	Piece types.
 *	-	Square types.
 * -	Players.
 *	-	AI (maybe)			(players[i]->ai->...).
 * -	Board (the ongoing game).
 *	-	Player turn			(b->turn).
 *	-	Pieces array			(b->pieces).
 */
struct hnef_game
{

/*@notnull@*/
/*@owned@*/
	struct hnef_ruleset	* rules;

	/*
	 * The ongoing game.
	 *
	 * This is NULL until `game_init_done()` has been called.
	 */
/*@null@*/
/*@owned@*/
	struct hnef_board	* board;

/*@notnull@*/
/*@owned@*/
	/*
	 * Array of playerc players.
	 *
	 * This is a pointer to an array, rather than an array, because
	 * of alloc_player. We can't just malloc a bunch of players in
	 * alloc_game, since that would not fully allocate the player
	 * struct.
	 */
	struct hnef_player	* * players;

	/*
	 * This is always equal to HNEF_PLAYERS_MAX.
	 */
	unsigned short		playerc;

	/*
	 * A move history list.
	 *
	 * NOTE:	This list (and the corresponding list in the
	 *		aiminimax struct: aiminimax->opt_movehist) can
	 *		grow indefinitely. This can consume infinite
	 *		memory, but will of course only consume 8 bytes
	 *		per move (or however large a moveh is).
	 *
	 * `opt_replayhist` is for undoing moves and loading saved
	 * games. It's just there so we don't have to allocate it all
	 * the time.
	 */
/*@notnull@*/
/*@owned@*/
	struct hnef_listmh	* movehist,
				* opt_replayhist;

	/*
	 * This is set to true when the ruleset is validated. If this is
	 * true, `hnef_game_valid()` will not check every thing in the
	 * game struct again, but just return `opt_valid_code`.
	 */
	HNEF_BOOL		opt_valid_known;

	/*
	 * If `opt_valid_known`, this code will be returned by
	 * `hnef_game_valid()` instead of computing it again (because
	 * it's a pretty big check and it may be done lots of times).
	 *
	 * This is reset when:
	 *
	 * -	`hnef_game_clear()`
	 * -	All `hnef_game_read_*()` functions in `rread.c`.
	 *
	 * NOTE:	If you modify the ruleset anywhere else, you
	 *		have to set `opt_valid_known` to false.
	 *		Otherwise it may falsely report that the ruluset
	 *		is valid; then the program will assume that
	 *		certain struct are `!NULL` and try to
	 *		dereference `NULL` pointers, et c..
	 */
	enum HNEF_RVALID	opt_valid_code;

};

#endif

