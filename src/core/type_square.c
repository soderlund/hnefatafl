/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */
#include <stdlib.h>	/* NULL */

#include "num.h"	/* hnef_single_bit */
#include "types.h"	/* HNEF_TYPEC_* */
#include "type_square.h"

unsigned short HNEF_TSQUARE_CAPT_SIDES_NONE	= (unsigned short)0;

HNEF_BOOL
hnef_type_square_can_capture (
	const struct hnef_type_square	* const HNEF_RSTR ts,
	const HNEF_BIT_U8		pbit
	)
{
	return	((unsigned int)pbit & (unsigned int)ts->captures)
		== (unsigned int)pbit;
}

/*
 * Initializes to invalid / default values.
 *
 * bit is invalid.
 */
void
hnef_type_square_init (
	struct hnef_type_square	* const HNEF_RSTR ts
	)
{
	ts->bit			= HNEF_BIT_U8_EMPTY;
	ts->captures		= HNEF_BIT_U8_EMPTY;
	ts->capt_sides		= HNEF_TSQUARE_CAPT_SIDES_NONE;
	ts->capt_sides_pieces	= HNEF_BIT_U8_EMPTY;
	ts->escape		= HNEF_FALSE;
	ts->ui_bit		= HNEF_BIT_U8_EMPTY;
}

/*
 * Retrieves hnef_type_square with matching bit, or an invalid square
 * type if none is found.
 */
struct hnef_type_square *
hnef_type_square_get (
	struct hnef_ruleset	* const HNEF_RSTR rules,
	const HNEF_BIT_U8	bit
	)
{
	return	& rules->type_squares[hnef_type_index_get(bit)];
}

/*
 * Sets the indicated hnef_type_square's bit to the parameter,
 * indicating that it may be used.
 *
 * *	If the bit is not a valid single bit, then it returns
 *	HNEF_FR_FAIL_ILL_ARG.
 *
 * *	If any previous square in r->hnef_type_squares is not defined
 *	(its bit is HNEF_BIT_U8_EMPTY, indicating that it has not been
 *	set using this function), it returns HNEF_FR_FAIL_NULLPTR.
 *
 * *	If the parameter bit has already been defined previously, then
 *	it returns HNEF_FR_FAIL_ILL_STATE.
 *
 * If the bit was successfully set (and r->hnef_type_squarec increases),
 * it returns HNEF_FR_SUCCESS.
 */
enum HNEF_FR
hnef_type_square_set (
	struct hnef_ruleset	* const HNEF_RSTR rules,
	const HNEF_BIT_U8	bit
	)
{
	unsigned short		index,
				i;
	struct hnef_type_square	* HNEF_RSTR ts	= NULL;

	assert	(hnef_single_bit((unsigned int)bit)
	||	bit < (HNEF_BIT_U8)128);

	index	= hnef_type_index_get(bit);
	if	(HNEF_TYPE_INDEX_INVALID == index)
	{
		return	HNEF_FR_FAIL_ILL_ARG;
	}
/*@i1@*/\
	else if	(index > rules->type_squarec)
	{
		return	HNEF_FR_FAIL_NULLPTR;
	}

	for	(i = (unsigned short)0; i < index; ++i)
	{
		if (HNEF_BIT_U8_EMPTY == rules->type_squares[i].bit)
		{
			/*
			 * Wrong order: some previous piece not defined.
			 */
			return	HNEF_FR_FAIL_NULLPTR;
		}
	}

	ts	= & rules->type_squares[index];
	if	(HNEF_BIT_U8_EMPTY != ts->bit)
	{
		/*
		 * This piece type has already been defined.
		 */
		return	HNEF_FR_FAIL_ILL_STATE;
	}

	ts->bit	= bit;
	++rules->type_squarec;
	return	HNEF_FR_SUCCESS;
}

