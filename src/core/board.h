/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_CORE_BOARD_H
#define HNEF_CORE_BOARD_H

#include "boardt.h"	/* hnef_board */
#include "config.h"	/* HNEF_RSTR */
#include "funct.h"	/* HNEF_FR */
#include "gamet.h"	/* hnef_game */
#include "listmt.h"	/* hnef_listm */
#include "player.h"	/* HNEF_PLAYERS_MAX */

#ifdef HNEFATAFL_ZHASH
#include "zhasht.h"	/* hnef_zhashtable */
#endif

/*@-protoparamname@*/
/*@unused@*/
extern
void
hnef_board_copy (
/*@in@*/
/*@notnull@*/
	const struct hnef_board	* const HNEF_RSTR src,
/*@in@*/
/*@notnull@*/
	struct hnef_board	* const HNEF_RSTR dest,
	const unsigned short	blen
	)
/*@modifies * dest@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
/*@unused@*/
extern
enum HNEF_FR
hnef_board_move_unsafe (
/*@in@*/
/*@notnull@*/
	const struct hnef_game		* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	struct hnef_board		* const board,
/*@in@*/
/*@notnull@*/
	struct hnef_listmh		* const movehist,
	const unsigned short		pos,
	const unsigned short		dest
#ifdef HNEFATAFL_ZHASH
	,
/*@in@*/
/*@null@*/
	const struct hnef_zhashtable	* const HNEF_RSTR hashtable,
/*@in@*/
/*@null@*/
	unsigned int			* const hashkey,
/*@in@*/
/*@null@*/
	unsigned int			* const hashlock
#endif
	)
/*@modifies * board, * movehist@*/
#ifdef HNEFATAFL_ZHASH
/*@modifies * hashkey, * hashlock@*/
#endif
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
enum HNEF_FR
hnef_game_move (
/*@in@*/
/*@notnull@*/
	struct hnef_game	* const HNEF_RSTR game,
	const unsigned short	pos,
	const unsigned short	dest,
/*@out@*/
/*@notnull@*/
	HNEF_BOOL		* const HNEF_RSTR legal
	)
/*@modifies * game, * legal@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
/*@unused@*/
extern
enum HNEF_FR
hnef_board_moves_get_pos (
/*@in@*/
/*@notnull@*/
	const struct hnef_game		* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	const struct hnef_board		* const HNEF_RSTR board,
/*@in@*/
/*@notnull@*/
	const struct hnef_listmh	* const HNEF_RSTR movehist,
/*@in@*/
/*@notnull@*/
	struct hnef_listm		* const list,
	const unsigned short		pos_piece
	)
/*@modifies * list@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
/*@unused@*/
extern
enum HNEF_FR
hnef_board_moves_get (
/*@in@*/
/*@notnull@*/
	const struct hnef_game		* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	const struct hnef_board		* const HNEF_RSTR board,
/*@in@*/
/*@notnull@*/
	const struct hnef_listmh	* const HNEF_RSTR movehist,
/*@in@*/
/*@notnull@*/
	struct hnef_listm		* const list
	)
/*@modifies * list@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
/*@unused@*/
extern
enum HNEF_FR
hnef_game_moves_get_pos (
/*@in@*/
/*@notnull@*/
	const struct hnef_game		* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	struct hnef_listm		* const list,
	const unsigned short		pos_piece
	)
/*@modifies * list@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
/*@unused@*/
extern
int
hnef_board_movec_get (
/*@in@*/
/*@notnull@*/
	const struct hnef_game		* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	struct hnef_board		* const HNEF_RSTR board,
/*@in@*/
/*@notnull@*/
	const struct hnef_listmh	* const HNEF_RSTR movehist,
	const HNEF_BIT_U8		piecemask,
	const HNEF_BOOL			fast
	)
/*@modifies * board@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
/*@unused@*/
HNEF_BOOL
hnef_board_game_over (
/*@in@*/
/*@notnull@*/
	const struct hnef_game		* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	struct hnef_board		* const HNEF_RSTR board,
/*@in@*/
/*@notnull@*/
	const struct hnef_listmh	* const HNEF_RSTR movehist,
/*@in@*/
/*@notnull@*/
	unsigned short			* const winner
	)
/*@modifies * board, * winner@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
/*@unused@*/
HNEF_BOOL
hnef_game_over (
/*@in@*/
/*@notnull@*/
	struct hnef_game	* const HNEF_RSTR game,
/*@in@*/
/*@notnull@*/
	unsigned short		* const winner
	)
/*@modifies * game, * winner@*/
;
/*@=protoparamname@*/

/*@-protoparamname@*/
extern
void
hnef_board_init (
	struct hnef_board	* const board,
	const unsigned short	blen
	)
/*@modifies board->turn, board->pieces@*/
/*@modifies board->opt_captloss_piece_lastseen@*/
;
/*@=protoparamname@*/

/*@null@*/
/*@only@*/
/*@partial@*/
extern
struct hnef_board *
hnef_alloc_board (
	const unsigned short
	)
/*@modifies nothing@*/
;

/*@-protoparamname@*/
extern
void
hnef_free_board (
/*@notnull@*/
/*@owned@*/
/*@special@*/
	struct hnef_board	* const HNEF_RSTR board
	)
/*@modifies board->pieces, board@*/
/*@releases board->pieces, board@*/
;
/*@=protoparamname@*/

#endif

