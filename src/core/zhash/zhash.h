/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifdef HNEFATAFL_ZHASH

#ifndef HNEF_CORE_AIMHASH_H
#define HNEF_CORE_AIMHASH_H

#include "zhasht.h"	/* hnef_zhashtable */
#include "config.h"	/* HNEF_RSTR */
#include "funct.h"	/* HNEF_FR */
#include "gamet.h"	/* hnef_game */

/*@unused@*/
extern
HNEF_BOOL
hnef_zhash_mem_tab_valid (
	const size_t
	)
/*@modifies nothing@*/
;

/*@unused@*/
extern
HNEF_BOOL
hnef_zhash_mem_col_valid (
	const size_t
	)
/*@modifies nothing@*/
;

/*@unused@*/
extern
size_t
hnef_zhash_mem_tab_def (void)
/*@modifies nothing@*/
;

/*@unused@*/
extern
size_t
hnef_zhash_mem_col_def (void)
/*@modifies nothing@*/
;

/*@unused@*/
extern
unsigned int
hnef_zhashkey (
/*@in@*/
/*@notnull@*/
	const struct hnef_zhashtable	* const HNEF_RSTR,
/*@in@*/
/*@notnull@*/
	const struct hnef_board		* const HNEF_RSTR
	)
/*@modifies nothing@*/
;

/*@unused@*/
extern
unsigned int
hnef_zhashlock (
/*@in@*/
/*@notnull@*/
	const struct hnef_zhashtable	* const,
/*@in@*/
/*@notnull@*/
	const struct hnef_board		* const
	)
/*@modifies nothing@*/
;

/*@-protoparamname@*/
/*@unused@*/
extern
void
hnef_zhashtable_put (
/*@in@*/
/*@notnull@*/
	struct hnef_zhashtable		* const HNEF_RSTR ht,
/*@in@*/
/*@notnull@*/
	const struct hnef_board		* const HNEF_RSTR b,
	const int			value,
	const enum HNEF_ZVALUE_TYPE	value_type,
	unsigned short			depthleft
	)
/*@modifies * ht@*/
;
/*@=protoparamname@*/

/*@dependent@*/
/*@in@*/
/*@null@*/
/*@unused@*/
extern
struct hnef_zhashnode *
hnef_zhashtable_get (
/*@in@*/
/*@notnull@*/
/*@returned@*/
	const struct hnef_zhashtable	* const HNEF_RSTR,
	const unsigned int,
	const unsigned int,
	const unsigned short
	)
/*@modifies nothing@*/
;

/*@-protoparamname@*/
/*@unused@*/
extern
void
hnef_zhashtable_clearunused (
/*@in@*/
/*@notnull@*/
	struct hnef_zhashtable	* const ht
	)
/*@modifies * ht@*/
;
/*@=protoparamname@*/

/*@null@*/
/*@only@*/
/*@unused@*/
extern
struct hnef_zhashtable *
hnef_alloc_zhashtable (
/*@in@*/
/*@notnull@*/
	const struct hnef_game	* const HNEF_RSTR,
	const size_t,
	const size_t
	)
/*@globals errno, internalState@*/
/*@modifies errno, internalState@*/
;

/*@-protoparamname@*/
/*@unused@*/
extern
void
hnef_free_zhashtable (
/*@notnull@*/
/*@owned@*/
/*@special@*/
	struct hnef_zhashtable	* const ht
	)
/*@modifies ht@*/
/*@releases ht@*/
;
/*@=protoparamname@*/

#endif

#endif

