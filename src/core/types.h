/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#ifndef HNEF_CORE_TYPES_H
#define HNEF_CORE_TYPES_H

#include "boardt.h"	/* HNEF_BIT_U8 */

/*
 * Using define because it must be available at compile time.
 *
 * Maximum amount of pieces and squares that can be defined.
 *
 * This is the maximum value of ruleset->type_piecec,
 * ruleset->type_piececap, ruleset->type_squarec and
 * ruleset->type_squarecap.
 *
 * Note that the length fo the type_pieces and type_squares arrays is
 * 1 longer than this. The last index (HNEF_TYPE_INDEX_INVALID) contains
 * the invalid piece/square type.
 */
#define HNEF_TYPE_MAX (unsigned short)8

/*
 * Define since used in switch.
 *
 * Index of the invalid piece in ruleset->type_pieces and
 * ruleset->type_squares.
 */
#define HNEF_TYPE_INDEX_INVALID (unsigned short)8

/*@unchecked@*/
extern
const unsigned short	HNEF_TPIECE_CAPT_SIDES_MAX,
			HNEF_TPIECE_CAPT_SIDES_MIN,
			HNEF_TPIECE_CAPT_SIDES_DEF;

extern
unsigned short
hnef_type_index_get (
	const HNEF_BIT_U8
	)
/*@modifies nothing@*/
;

#endif

