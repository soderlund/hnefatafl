/*
 * Copyright © 2013-2014 Alexander Söderlund, Sweden
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL
 * WARRANTIES WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
 * AUTHOR BE LIABLE FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL
 * DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR
 * PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER
 * TORTIOUS ACTION, ARISING OUT OF OR IN CONNECTION WITH THE USE OR
 * PERFORMANCE OF THIS SOFTWARE.
 */
#include <assert.h>	/* assert */
#include <stdlib.h>	/* malloc, free */
#include <string.h>	/* memcpy */

#include "game.h"
#include "board.h"	/* hnef_board_* */
#include "func.h"	/* hnef_fr_good */
#include "listmh.h"	/* hnef_listmh_* */
#include "numt.h"	/* SIZET_MAX */
#include "player.h"	/* hnef_player_* */
#include "rvalid.h"	/* hnef_game_valid */
#include "ruleset.h"	/* hnef_ruleset_* */

/*
 * NOTE:	How to allocate and initialize a game:
 *
 *	game = hnef_alloc_game();	// NULL indicates failure
 *	if (!hnef_fr_good(hnef_game_read_*(game, ...))	// rread.c
 *						// Also check HNEF_RREAD
 *	|| !hnef_rvalid_good(hnef_game_valid(game))	// rvalid.c
 *	|| !hnef_fr_good(hnef_game_init_done(game)))
 *	{
 *		hnef_game_clear(game);
 *		// We can try `hnef_game_read_*()` again.
 *	}
 *
 * If `hnef_game_read_*()` or `hnef_game_init_done()` returns failure,
 * then the game must be cleared since it may be in an inconsistent
 * state.
 *
 * `hnef_rvalid_good()` can be called any time and a failure just means
 * that the game or ruleset is not valid. The game does not have to be
 * cleared just because it's invalid (a `HNEF_RVALID` failure doesn't
 * indicate any serious error). However, if it is invalid after reading
 * a ruleset file, then you have to clear it if you want to read another
 * ruleset file (unless you intend to make it valid in some other way).
 *
 * To start a new game, after one has been played to conclusion, call
 * `hnef_game_reset()`. This is automatically called by
 * `hnef_game_init_done()`.
 */

void
hnef_game_reset_board (
	struct hnef_game	* const HNEF_RSTR game
	)
{
	const struct hnef_ruleset	* HNEF_RSTR rules = NULL;

	assert	(NULL != game);
	assert	(NULL != game->board);
	assert	(NULL != game->rules);
	assert	(NULL != game->rules->pieces);

	rules	= game->rules;
	memcpy	(game->board->pieces, rules->pieces,
			(size_t)rules->opt_blen);
	game->board->turn	= (unsigned short)0;
}

static
void
hnef_game_reset_movehist (
/*@in@*/
/*@notnull@*/
	struct hnef_game	* const HNEF_RSTR game
	)
/*@modifies game->movehist@*/
{
	assert	(NULL != game);
	assert	(NULL != game->movehist);

	/*
	 * Remove all history:
	 */
	game->movehist->elemc	= 0;
}

/*
 * Resets the board struct hnef_(g->b) based on the ruleset.
 *
 * This is equivalent to "starting a new game".
 *
 * Calling this function is the same as calling:
 *
 * 1.	`hnef_game_reset_board(g)`
 * 2.	`hnef_game_reset_movehist(g)`
 *
 * The game must be valid.
 */
void
hnef_game_reset (
	struct hnef_game	* const HNEF_RSTR game
	)
{
	assert	(NULL != game);

	hnef_game_reset_board	(game);
	hnef_game_reset_movehist(game);
}

/*
 * Initializes optimization for the game.
 */
static
void
hnef_game_initopt (
/*@in@*/
/*@notnull@*/
	struct hnef_game	* const HNEF_RSTR game
	)
/*@modifies * game@*/
{
	unsigned short i;
	for	(i = 0; i < game->playerc; ++i)
	{
		hnef_player_initopt	(game, game->players[i]);
	}
}

/*
 * Call after initializing the game in `rread.c`, iff the game is valid
 * according to `rvalid.c`.
 *
 * See the comments in `hnef_ruleset_init_done()`. If this function
 * returns failure, you can recover by calling `hnef_game_clear()` on
 * `game` and initialize the game again (read the ruleset again).
 *
 * This function automatically calls `game_reset()`.
 */
enum HNEF_FR
hnef_game_init_done (
	struct hnef_game	* const HNEF_RSTR game
	)
{
	struct hnef_ruleset	* HNEF_RSTR rules = NULL;
	enum HNEF_FR		fr		= HNEF_FR_SUCCESS;

	assert	(NULL != game);
/*@i2@*/\
	assert	(hnef_rvalid_good(hnef_game_valid(game)));

	rules	= game->rules;

	/*
	 * Makes `opt_blen` available.
	 */
	fr	= hnef_ruleset_init_done(rules,
			rules->bwidth, rules->bheight);
	if	(!hnef_fr_good(fr))
	{
		return	fr;
	}

	hnef_game_initopt(game);

	if	(NULL != game->board)
	{
		hnef_free_board	(game->board);
		game->board	= NULL;
	}

	assert	(NULL == game->board);
	game->board = hnef_alloc_board(game->rules->opt_blen);
	if	(NULL == game->board)
	{
		return	HNEF_FR_FAIL_ALLOC;
	}

	hnef_board_init	(game->board, game->rules->opt_blen);
	hnef_game_reset	(game);

/*@i2@*/\
	assert	(hnef_rvalid_good(hnef_game_valid(game)));

	return	HNEF_FR_SUCCESS;
}

/*
 * These variables are NULL after this call:
 *
 * -	game->board
 * -	game->rules->pieces
 * -	game->rules->squares
 * -	game->rules->id
 * -	game->rules->name
 * -	game->rules->path
 *
 * Neither game, game->players nor game->rules are initialized. See
 * `rread.c` for game initialization functions. A game is valid if
 * `hnef_game_valid()` (see `rvalid.c`) returns success.
 *
 * This function clears game->players and the game->rules.
 */
void
hnef_game_clear (
	struct hnef_game	* const HNEF_RSTR game
	)
{
	unsigned short	i;

	assert	(NULL != game);

	game->opt_valid_known	= HNEF_FALSE;
	game->opt_valid_code	= HNEF_RVALID_SUCCESS;

	if	(NULL != game->board)
	{
		hnef_free_board	(game->board);
		game->board	= NULL;
	}

	hnef_ruleset_clear	(game->rules);

	for	(i = 0; i < game->playerc; ++i)
	{
		hnef_player_clear(game->players[i]);
	}
}

static
HNEF_BOOL
hnef_game_type_piece_equal (
/*@in@*/
/*@notnull@*/
	const struct hnef_type_piece	* const HNEF_RSTR tp1,
/*@in@*/
/*@notnull@*/
	const struct hnef_type_piece	* const HNEF_RSTR tp2
	)
/*@modifies nothing@*/
{
	assert	(NULL != tp1);
	assert	(NULL != tp2);

	return	tp1->bit		== tp2->bit
	&&	tp1->captures		== tp2->captures
	&&	tp1->dbl_trap_squares	== tp2->dbl_trap_squares
	&&	tp1->noreturn		== tp2->noreturn
	&&	tp1->occupies		== tp2->occupies
	&&	tp1->ui_bit		== tp2->ui_bit
	&&	tp1->owner		== tp2->owner
	&&	tp1->capt_sides		== tp2->capt_sides
	&&	tp1->capt_edge		== tp2->capt_edge
	&&	tp1->capt_loss		== tp2->capt_loss
	&&	tp1->custodial		== tp2->custodial
	&&	tp1->dbl_trap		== tp2->dbl_trap
	&&	tp1->dbl_trap_capt	== tp2->dbl_trap_capt
	&&	tp1->dbl_trap_compl	== tp2->dbl_trap_compl
	&&	tp1->dbl_trap_edge	== tp2->dbl_trap_edge
	&&	tp1->dbl_trap_encl	== tp2->dbl_trap_encl
	&&	tp1->escape		== tp2->escape;
}

static
HNEF_BOOL
hnef_game_type_square_equal (
/*@in@*/
/*@notnull@*/
	const struct hnef_type_square	* const HNEF_RSTR ts1,
/*@in@*/
/*@notnull@*/
	const struct hnef_type_square	* const HNEF_RSTR ts2
	)
/*@modifies nothing@*/
{
	assert	(NULL != ts1);
	assert	(NULL != ts2);

	return	ts1->bit		== ts2->bit
	&&	ts1->captures		== ts2->captures
	&&	ts1->capt_sides_pieces	== ts2->capt_sides_pieces
	&&	ts1->ui_bit		== ts2->ui_bit
	&&	ts1->capt_sides		== ts2->capt_sides
	&&	ts1->escape		== ts2->escape;
}

/*
 * Compares two rulesets (mostly `game->rules` but, possibly in the
 * future, also `game->players`) for equality. This function only
 * compares the relevant struct fields to see if two rulesets (which
 * have been read from ruleset files) are equal; it does not compare the
 * ongoing game (`game->board`, `game->movehist`) or optimization
 * variables.
 *
 * -	`game->rules` is compared.
 * -	`game->rules->type_pieces/squares` are compared.
 * -	`game->players` may be compared in the future, but are not
 *	compard now (however your code should assume that they are
 *	compared now).
 *
 * If either or both games are invalid, this function always returns
 * false.
 *
 * `hnef_game_init_done()` must *not* have been called on either
 * ruleset. They can be compared without it.
 *
 * -	`rules->id`, `rules->name` and `rules->ui_theme` must all be
 *	equal, even though `name` and `ui_theme` don't really matter.
 */
HNEF_BOOL
hnef_game_rules_equal (
	struct hnef_game	* const HNEF_RSTR g1,
	struct hnef_game	* const HNEF_RSTR g2
	)
{
	const struct hnef_ruleset	* HNEF_RSTR r1	= NULL,
					* HNEF_RSTR r2	= NULL;
	unsigned short			blen1,
					i;

	assert	(NULL != g1);
	assert	(NULL != g2);

	r1	= g1->rules;
	r2	= g2->rules;
	blen1	= (unsigned short)(r1->bwidth * r1->bheight);

	if	(!hnef_rvalid_good(hnef_game_valid(g1))
	||	!hnef_rvalid_good(hnef_game_valid(g2)))
	{
		return	HNEF_FALSE;
	}

	assert	(NULL != r1->id);
	assert	(NULL != r2->id);
	assert	(NULL != r1->name);
	assert	(NULL != r2->name);
	assert	(NULL != r1->ui_theme);
	assert	(NULL != r2->ui_theme);
	assert	(NULL != r1->pieces);
	assert	(NULL != r2->pieces);
	assert	(NULL != r1->squares);
	assert	(NULL != r2->squares);

	/*
	 * NOTE:	`game->players` are not compared because there
	 *		are no player fields to compare. However we may
	 *		compare them in the future.
	 */

	if	(0 != strcmp	(r1->id,	r2->id)
	||	0 != strcmp	(r1->name,	r2->name)
	||	0 != strcmp	(r1->ui_theme,	r2->ui_theme)
	||	g1->playerc		!= g2->playerc /* Unnecessary */
	||	r1->forbid_repeat	!= r2->forbid_repeat
	||	r1->bwidth		!= r2->bwidth
	||	r1->bheight		!= r2->bheight
	||	r1->type_piecec		!= r2->type_piecec
	||	r1->type_squarec	!= r2->type_squarec)
	{
		return	HNEF_FALSE;
	}

	/*
	 * Implied by width / height
	 */
	assert	(blen1 == (unsigned short)(r2->bwidth * r2->bheight));

	for	(i = 0; i < blen1; ++i)
	{
		if	(r1->pieces[i] != r2->pieces[i]
		||	r1->squares[i] != r2->squares[i])
		{
			return	HNEF_FALSE;
		}
	}

	assert	(r1->type_piecec == r2->type_piecec);
	for	(i = 0; i < r1->type_piecec; ++i)
	{
		if (!hnef_game_type_piece_equal(& r1->type_pieces[i],
						& r2->type_pieces[i]))
		{
			return	HNEF_FALSE;
		}
	}

	assert	(r1->type_squarec == r2->type_squarec);
	for	(i = 0; i < r1->type_squarec; ++i)
	{
		if (!hnef_game_type_square_equal(& r1->type_squares[i],
						& r2->type_squares[i]))
		{
			return	HNEF_FALSE;
		}
	}

	return	HNEF_TRUE;
}

/*
 * Returns NULL if failing to allocate.
 *
 * If successful, then game->playerc is set to HNEF_PLAYERS_MAX. The
 * players are allocated and initialized according to player_init.
 *
 * You must still initialize the rest of the game after calling this
 * function.
 *
 * `hnef_game_clear()` is automatically called on the game object before
 * returning it.
 */
struct hnef_game *
hnef_alloc_game (void)
{
	unsigned short		i;
	struct hnef_game	* const HNEF_RSTR game =
				malloc(sizeof(* game));
	if	(NULL == game)
	{
		return	NULL;
	}

	game->rules	= hnef_alloc_ruleset();
	if	(NULL == game->rules)
	{
		free	(game);
		return	NULL;
	}

	game->movehist	= hnef_alloc_listmh(HNEF_LISTMH_CAP_DEF);
	if	(NULL == game->movehist)
	{
		hnef_free_ruleset	(game->rules);
		free			(game);
		return			NULL;
	}

	game->opt_replayhist = hnef_alloc_listmh(HNEF_LISTMH_CAP_DEF);
	if	(NULL == game->opt_replayhist)
	{
		hnef_free_listmh	(game->movehist);
		hnef_free_ruleset	(game->rules);
		free			(game);
		return			NULL;
	}

	game->playerc	= HNEF_PLAYERS_MAX;
	game->players	= malloc(sizeof(* game->players)
			* (size_t)game->playerc);
	if	(NULL == game->players)
	{
		hnef_free_listmh	(game->opt_replayhist);
		hnef_free_listmh	(game->movehist);
		hnef_free_ruleset	(game->rules);
		free			(game);
		return			NULL;
	}
	for	(i = 0; i < game->playerc; ++i)
	{
		game->players[i]	= hnef_alloc_player();
		if	(NULL == game->players[i]
		||	!hnef_player_init(game->players[i], i))
		{
			unsigned short j;
			for	(j = (unsigned short)0; j <= i
/* Check added for GCC's loop optimizations: */
				&& j <= HNEF_PLAYERS_MAX
				; ++j)
			{
				if	(NULL != game->players[j])
				{
					hnef_free_player
						(game->players[j]);
				}
			}
			free		(game->players);
			hnef_free_listmh(game->opt_replayhist);
			hnef_free_listmh(game->movehist);
			hnef_free_ruleset(game->rules);
			free		(game);
			return		NULL;
		}
	}

	game->board	= NULL;

	/*
	 * Resets `opt_valid_known`.
	 */
	hnef_game_clear(game);

	return	game;
}

void
hnef_free_game (
	struct hnef_game	* const HNEF_RSTR game
	)
{
	if	(NULL != game->board)
	{
		hnef_free_board		(game->board);
	}

	if	(NULL != game->rules)
	{
		hnef_free_ruleset	(game->rules);
	}

	if	(NULL != game->movehist)
	{
		hnef_free_listmh	(game->movehist);
	}

	if	(NULL != game->opt_replayhist)
	{
		hnef_free_listmh	(game->opt_replayhist);
	}

	if	(NULL != game->players)
	{
		unsigned short i;
		for	(i = (unsigned short)0; i < game->playerc; ++i)
		{
			if	(NULL != game->players[i])
			{
				hnef_free_player(game->players[i]);
			}
		}
		free	(game->players);
/*@i2@*/\
	}

	free	(game);
}

