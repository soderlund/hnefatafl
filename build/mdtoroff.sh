#!/bin/sh

# Version 140729

#
# Converts a Markdown formatted file to roff.
#
# -c	Manual page category.
# -p	Command name.
# -v	Version. Must be in the format "YYMMDD".
#
# The first line in `stdin` is the title. The second line is ignored.
#

set -e

MARKDOWN=${MARKDOWN:-"pandoc -s -t man"}

c_awk=awk

a_mancat=c
a_prgnam=p
a_version=v

print_help ()
{
	printf '%s [-%s MANCAT] [-%s PRGNAM] [-%s VERSION]\n'\
		"`basename $0`" "$a_mancat" "$a_prgnam" "$a_version"
	printf '\n'
	printf 'Generates a manual page with: %s\n' "$MARKDOWN"
	printf '\n'
	printf 'The following conversion is made post-generation:\n'
	printf '\n'
	printf '\t.TH PRGNAM "" "VERSION"\n'
	printf '\t->\n'
	printf '\t.TH PRGNAM MANCAT DATE VERSION "MANTITLE"\n'
	printf '\n'
	printf 'DATE (YYYY-MM-DD) is extracted from VERSION (YYMMDD).\n'
	printf '\n'
	printf 'MANTITLE is the first line in stdin.'
	printf ' The second line is ignored.\n'
}

while	getopts $a_mancat:$a_prgnam:$a_version: arg
do
	case $arg in
		$a_mancat)
			mancat="$OPTARG"
			;;
		$a_prgnam)
			prgnam="$OPTARG"
			;;
		$a_version)
			version="$OPTARG"
			;;
		\?)
			print_help
			exit 1
			;;
	esac
done
if	[ "$OPTIND" -gt 1 ]
then
	shift `expr $OPTIND - 1`
fi

if	[ -z "$mancat" ]
then
	printf 'Missing argument: %s\n' $a_mancat
	printf '\n'
	print_help
	exit 1
elif	[ -z "$prgnam" ]
then
	printf 'Missing argument: %s\n' $a_prgnam
	printf '\n'
	print_help
	exit 1
elif	[ -z "$version" ]
then
	printf 'Missing argument: %s\n' $a_version
	printf '\n'
	print_help
	exit 1
fi

read mantitle
read tmp
unset tmp
if	[ "" = "$mantitle" ]
then
	printf 'Manual title (first line) is empty.\n'
	printf '\n'
	print_help
	exit 1
fi

$c_awk '
BEGIN {
	prev	= ""
	cur	= ""

	print "% "'\"$prgnam\"'
	print "%"
	print "% "'\"$version\"'
}

{
	prev	= cur
	cur	= $0
	gsub(//, "", cur)
}

/^---*/ {
	prev = toupper(prev)
	gsub(/-/, "=", cur)
}

/^###### / {
	gsub(/######/, "#####", cur)
}

/^##### / {
	gsub(/#####/, "####", cur)
}

/^#### / {
	gsub(/####/, "###", cur)
}

/^### / {
	gsub(/###/, "##", cur)
}

{
	if	(NR > 1)
	{
		print prev
	}
}

END {
	print cur
}
'\
| $MARKDOWN\
| $c_awk '
function expand_date (d)
{
	return	"20"substr(d, 1, 2)"-"substr(d, 3, 2)"-"substr(d, 5, 2)
}

/^\.TH / {
	sub(/'$prgnam'/,	toupper('\"$prgnam\"'),	$0)
	sub(/""/,		'\"$mancat\"',		$0)
	sub(/"'$version'"/,
	expand_date('\"$version\"')" '$version' \"'"$mantitle"'\"",
		$0)
}

{
	gsub(/\\f\[C\]/, "\\f[CI]", $0)
	print $0
}
'

